#ifndef _CONSOLE_COLOUR_H_
#define _CONSOLE_COLOUR_H_

namespace ui
{

class ConsoleColour
{
public:
    enum ColourPair
    {
        DEFAULT = 0,
        SELFPLAYER = 16,
        PLAYER1,
        PLAYER2,
        PLAYER3,
        PLAYER4,
        PLAYER5,
        PLAYER6,
        PLAYER7,
        PLAYER8,
        FOOD,
        WALL,
        SLIP,
        TUNNEL,
    };

private:
    bool colour_enabled;

    int curr_colour_pair;

public:
    constexpr ConsoleColour();

public:
    void Setup();

    void Begin(int colour_pair);
    void End();

    int ColourPairFromPlayerIndex(int index);
};

extern ConsoleColour colour;

}   // namespace ui

#endif
