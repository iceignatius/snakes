#include "MessageForm.h"
#include "YesNoForm.h"
#include "TextInputForm.h"
#include "FormUtils.h"

using namespace std;
using namespace ui;

void ui::util::RegisterForms(FormManager *mgr)
{
    mgr->RegisterFormKlass(MessageForm::KlassName(), MessageForm::CreateInstance);
    mgr->RegisterFormKlass(YesNoForm::KlassName(), YesNoForm::CreateInstance);
    mgr->RegisterFormKlass(TextInputForm::KlassName(), TextInputForm::CreateInstance);
}

void ui::util::ShowMessage(
    FormManager *mgr,
    const string &msg,
    unsigned timeout,
    Form *blockform)
{
    FormParams params;
    params.InsertStringParam("text", msg);
    params.InsertUintParam("timeout", timeout);

    mgr->PushNewForm(MessageForm::KlassName(), params);

    if( blockform )
        mgr->RunModal(blockform);
}

int ui::util::AskYesNo(
    FormManager *mgr,
    Form *currform,
    const string &title,
    const string &prompt,
    int defaultselect)
{
    FormParams params;
    params.InsertStringParam("title", title);
    params.InsertStringParam("prompt", prompt);
    params.InsertIntParam("select", defaultselect);

    mgr->PushNewForm(YesNoForm::KlassName(), params);

    if( mgr->RunModal(currform, &params) )
        return AR_NO;

    return params.GetIntParam("select", defaultselect);
}

bool ui::util::InputString(
    FormManager *mgr,
    Form *currform,
    const string &title,
    const string &prompt,
    string &result)
{
    FormParams params;
    params.InsertStringParam("title", title);
    params.InsertStringParam("prompt", prompt);

    mgr->PushNewForm(TextInputForm::KlassName(), params);

    if( mgr->RunModal(currform, &params) )
        return false;

    result = params.GetStringParam("result", "");
    return true;
}
