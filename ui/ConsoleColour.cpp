#include <stdexcept>
#include <ncurses.h>
#include "ConsoleColour.h"

using namespace std;
using namespace ui;

ConsoleColour ui::colour;

constexpr ConsoleColour::ConsoleColour() :
    colour_enabled(false), curr_colour_pair(-1)
{
}

void ConsoleColour::Setup()
{
    if( !has_colors() || !can_change_color() )
        return;

    try
    {
        if( start_color() )
            throw runtime_error("Start colour system failed!");

        colour_enabled = true;

        enum
        {
            COLOUR_GREEN = 16,
            COLOUR_RED,
            COLOUR_BLUE,
            COLOUR_YELLOW,
            COLOUR_PURPLE,
            COLOUR_ORANGE,
            COLOUR_BROWN,
            COLOUR_DARK_RED,
            COLOUR_DARK_GREEN,
            COLOUR_CYAN,
            COLOUR_LIGHT_GREY,
        };

        if( init_color(COLOUR_GREEN, 0, 1000, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_RED, 1000, 0, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_BLUE, 0, 0, 1000) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_YELLOW, 1000, 1000, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_PURPLE, 1000, 0, 1000) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_ORANGE, 1000, 500, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_BROWN, 500, 250, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_DARK_RED, 664, 0, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_DARK_GREEN, 0, 664, 0) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_CYAN, 0, 1000, 1000) )
            throw runtime_error("Initialise colour failed!");
        if( init_color(COLOUR_LIGHT_GREY, 664, 664, 664) )
            throw runtime_error("Initialise colour failed!");

        if( init_pair(PLAYER1, COLOUR_RED, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER2, COLOUR_BLUE, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER3, COLOUR_YELLOW, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER4, COLOUR_PURPLE, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER5, COLOUR_ORANGE, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER6, COLOUR_BROWN, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER7, COLOUR_DARK_RED, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(PLAYER8, COLOUR_DARK_GREEN, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(SELFPLAYER, COLOUR_GREEN, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(FOOD, COLOUR_CYAN, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(WALL, COLOR_WHITE, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(SLIP, COLOUR_LIGHT_GREY, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
        if( init_pair(TUNNEL, COLOUR_CYAN, COLOR_BLACK) )
            throw runtime_error("Initialise colour pair failed!");
    }
    catch(exception &e)
    {
        colour_enabled = false;
    }
}

void ConsoleColour::Begin(int colour_pair)
{
    if( !colour_enabled )
        return;

    if( curr_colour_pair >= 0 )
        throw runtime_error("Duplicated begin colour!");

    if( colour_pair < 0 || colour_pair >= COLOR_PAIRS )
        throw invalid_argument("Invalid colour pair value!");

    if( attron(COLOR_PAIR(colour_pair)) )
        throw runtime_error("Change colour failed!");

    curr_colour_pair = colour_pair;
}

void ConsoleColour::End()
{
    if( !colour_enabled ) return;

    if( curr_colour_pair < 0 )
        throw runtime_error("Duplicated end colour!");

    attroff(COLOR_PAIR(curr_colour_pair));
    curr_colour_pair = -1;
}

int ConsoleColour::ColourPairFromPlayerIndex(int index)
{
    switch(index)
    {
    case 1: return PLAYER1;
    case 2: return PLAYER2;
    case 3: return PLAYER3;
    case 4: return PLAYER4;
    case 5: return PLAYER5;
    case 6: return PLAYER6;
    case 7: return PLAYER7;
    case 8: return PLAYER8;

    default:
        return DEFAULT;
    }
}
