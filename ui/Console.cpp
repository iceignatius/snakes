#include <assert.h>
#include <string.h>
#include <stdexcept>
#include "Console.h"

using namespace std;
using namespace ui;

#define MIN_SCREEN_WIDTH_REQ    80
#define MIN_SCREEN_HEIGHT_REQ   24

Console::Console() :
    started(false)
{
}

Console::~Console()
{
    Shutdown();
}

void Console::Setup()
{
    if( started ) return;

    bool lib_inited = false;
    try
    {
        if( !initscr() )
            throw runtime_error("Initialise curses library failed");
        lib_inited = true;

        // Check resolution.
        if( getmaxx(stdscr) < MIN_SCREEN_WIDTH_REQ ||
            getmaxy(stdscr) < MIN_SCREEN_HEIGHT_REQ )
        {
            throw runtime_error("Console resolution too small!");
        }

        // Catch most of keys input.
        if( cbreak() )
            throw runtime_error("Set property failed!");

        // No new-line.
        if( nonl() )
            throw runtime_error("Set property failed!");

        // Not show the input characters.
        if( noecho() )
            throw runtime_error("Set property failed!");

        // Catch special keys.
        if( keypad(stdscr, true) )
            throw runtime_error("Set property failed!");

        // Not wait key input if the input buffer is empty.
        if( notimeout(stdscr, true) )
            throw runtime_error("Set property failed!");
        if( nodelay(stdscr, true) )
            throw runtime_error("Set property failed!");

        // Hide cursor.
        curs_set(0);

        started = true;
    }
    catch(exception &e)
    {
        if( lib_inited )
            endwin();
        throw;
    }
}

void Console::Shutdown()
{
    if( !started ) return;

    endwin();
    started = false;
}

void Console::BeginRender()
{
    if( clear() )
        throw runtime_error("Clear screen failed!");
}

void Console::EndRender()
{
    if( refresh() )
        throw runtime_error("Update screen failed!");
}

void Console::RenderHLine(int x, int y, int len, char body, char edge)
{
    for(int i = 1; i < len - 1; ++i)
        mvaddch(y, x + i, body);

    mvaddch(y, x, edge);
    mvaddch(y, x + len - 1, edge);
}

void Console::RenderVLine(int x, int y, int len, char body, char edge)
{
    for(int i = 1; i < len - 1; ++i)
        mvaddch(y + i, x, body);

    mvaddch(y, x, edge);
    mvaddch(y + len - 1, x, edge);
}

void Console::RenderBox(
    int x,
    int y,
    int w,
    int h,
    char hline,
    char vline,
    char edge,
    char filled)
{
    assert( w >= 2 && h >= 2 );
    int xl = x, xr = x + w - 1;
    int yt = y, yb = y + h - 1;

    for(int col = 1; col < w - 1; ++col)
        mvaddch(yt, xl + col, hline);
    mvaddch(yt, xl, edge);
    mvaddch(yt, xr, edge);

    for(int row = 1; row < h - 1; ++row)
    {
        if( filled )
        {
            for(int col = 1; col < w - 1; ++col)
                mvaddch(yt + row, xl + col, filled);
        }

        mvaddch(yt + row, xl, vline);
        mvaddch(yt + row, xr, vline);
    }

    for(int col = 1; col < w - 1; ++col)
        mvaddch(yb, xl + col, hline);
    mvaddch(yb, xl, edge);
    mvaddch(yb, xr, edge);
}

void Console::RenderMessageBox(const string &text, int x, int y, int w, int h)
{
    int len = text.length();
    int box_cols = len + 4;
    int box_rows = 3;

    int panel_cols = w ? w : getmaxx(stdscr);
    int panel_rows = h ? h : getmaxy(stdscr);
    assert( panel_cols >= box_cols );
    assert( panel_rows >= box_rows );

    int box_x = x + ( panel_cols - box_cols ) / 2;
    int box_y = y + ( panel_rows - box_rows ) / 2;

    RenderHLine(box_x, box_y, box_cols, '-', '+');
    RenderHLine(box_x, box_y + 3 - 1, box_cols, '-', '+');

    move(box_y + 1, box_x);
    addstr("| ");
    printw(text.c_str());
    addstr(" |");
}

const bool* Console::GetKeyboardState(bool state[KEY_MAX])
{
    static bool internal_buffer[KEY_MAX];
    bool *state_array = state ? state : internal_buffer;

    memset(state_array, 0, sizeof(state_array[0])*KEY_MAX);
    for(int key; ( key = getch() ) >= 0; )
    {
        assert( key < KEY_MAX );
        state_array[key] = true;
    }

    return state_array;
}
