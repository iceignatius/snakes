#ifndef _UI_TEXT_INPUT_MODULE_H_
#define _UI_TEXT_INPUT_MODULE_H_

#include <string>

namespace ui
{

enum class TextInputState
{
    DISABLED = -1,
    ABORTED = -1,
    TYPING = 0,
    COMPLETED = 1,
};

class TextInputModule
{
private:
    std::string text;
    TextInputState state;

public:
    TextInputModule() : state(TextInputState::TYPING) {}

private:
    int PushCharacter(int ch);

public:
    TextInputState State() const { return state; }

    const std::string& BufferedText() { return text; }
    std::string PopCompletedText(const std::string &failval = "");
    int PushInput(const bool keystate[]);

    void Disable();
    void Abort();
    void Restart();
};

}   // namespace ui

#endif
