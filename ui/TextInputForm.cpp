#include "common/ascii.h"
#include "TextInputForm.h"

using namespace std;
using namespace ui;

TextInputForm::TextInputForm(FormManager *mgr) :
    FormBase(mgr), dialog_width(4), dialog_height(6)
{
}

const char* TextInputForm::KlassName()
{
    return "text-input-form";
}

Form* TextInputForm::CreateInstance(FormManager *mgr)
{
    return new TextInputForm(mgr);
}

string TextInputForm::ReplaceControlCodes(
    const string &text,
    char replacement)
{
    string str = text;
    for(size_t i = 0; i < str.length(); ++i)
    {
        if( iscntrl(str[i]) )
            str[i] = replacement;
    }

    return str;
}

list<string> TextInputForm::ParseTextLines(
    const string &text,
    char ctrl_replacement)
{
    list<string> lines;

    string str;
    for(size_t i = 0; i < text.length(); ++i)
    {
        char ch = text[i];
        if( ch == '\n' )
        {
            lines.push_back(ReplaceControlCodes(str, ctrl_replacement));
            str.clear();
        }
        else
        {
            str.push_back(ch);
        }
    }

    if( text.back() != '\n' )
        lines.push_back(str);

    return lines;
}

void TextInputForm::OnPush(const FormParams &params)
{
    title = params.GetStringParam("title", "");
    title = ReplaceControlCodes(title, '*');

    string prompt = params.GetStringParam("prompt", "");
    prompt_lines = ParseTextLines(prompt, '*');

    unsigned max_text_width = title.length() ? title.length() + 2 : 0;
    for(string &str : prompt_lines)
    {
        if( max_text_width < str.length() )
            max_text_width = str.length();
    }

    dialog_width = max(dialog_width, (int)( 2 + max_text_width + 2 ));
    dialog_height = max(dialog_height, (int)( 2 + prompt_lines.size() + 2 + 2 ));
}

void TextInputForm::OnPop(FormParams &params)
{
    if( ExitCode() == 0 )
        params.InsertStringParam("result", input.PopCompletedText(""));
}

void TextInputForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    input.PushInput(keystate);

    if( input.State() == TextInputState::COMPLETED )
        Close();
    if( input.State() == TextInputState::ABORTED )
        Close(1);
}

void TextInputForm::Render()
{
    int screen_cols = getmaxx(stdscr);
    int screen_rows = getmaxy(stdscr);

    int dialog_posx =
        screen_cols >= dialog_width ?
        ( screen_cols - dialog_width ) / 2 :
        0;
    int dialog_posy =
        screen_rows >= dialog_height ?
        ( screen_rows - dialog_height ) / 2 :
        0;

    Console *console = Manager()->GetConsole();

    int separator_posy = dialog_posy + prompt_lines.size() + 4 - 1;
    console->RenderBox(dialog_posx, dialog_posy, dialog_width, dialog_height);
    console->RenderHLine(dialog_posx, separator_posy, dialog_width);

    if( title.length() )
    {
        int xoff = ( dialog_width - title.length() - 2 ) / 2;
        mvprintw(dialog_posy, dialog_posx + xoff, " %s ", title.c_str());
    }

    if( prompt_lines.size() )
    {
        int yoff = 2;
        for(string &str : prompt_lines)
        {
            int xoff = ( dialog_width - str.length() ) / 2;
            mvprintw(dialog_posy + yoff, dialog_posx + xoff, "%s", str.c_str());
            ++yoff;
        }
    }

    string text = input.BufferedText();
    if( text.length() )
    {
        int xoff = ( dialog_width - text.length() ) / 2;
        mvprintw(dialog_posy + dialog_height - 2, dialog_posx + xoff, "%s", text.c_str());
    }
}
