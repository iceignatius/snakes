#ifndef _UI_YESNO_FORM_H_
#define _UI_YESNO_FORM_H_

#include <list>
#include "ui/FormBase.h"

namespace ui
{

class YesNoForm : public ui::FormBase
{
private:
    std::string title;
    std::list<std::string> prompt_lines;
    int selectcode;

    enum SelectCode
    {
        SELECT_YES = 0,
        SELECT_NO = 1,
    };

    int dialog_width;
    int dialog_height;

private:
    YesNoForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    static std::string ReplaceControlCodes(
        const std::string &text,
        char replacement);

    static std::list<std::string> ParseTextLines(
        const std::string &text,
        char ctrl_replacement);

private:
    void OnPush(const ui::FormParams &params) override;
    void OnPop(ui::FormParams &params) override;

public:
    void Update(unsigned steptime) override;
    void Render() override;
};

}   // namespace ui

#endif
