#ifndef _UI_MESSAGE_FORM_H_
#define _UI_MESSAGE_FORM_H_

#include "ui/FormBase.h"

namespace ui
{

class MessageForm : public ui::FormBase
{
private:
    std::string text;

    bool timer_enabled;
    unsigned timer_remain;

private:
    MessageForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    void OnPush(const ui::FormParams &params) override;

public:
    void Update(unsigned steptime) override;
    void Render() override;
};

}   // namespace ui

#endif
