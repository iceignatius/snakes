#include <string.h>
#include "FormParam.h"

using namespace std;
using namespace ui;

VariantParam VariantParam::NullParam;

VariantParam::VariantParam() : type(Type::Null)
{
    this->value.ptrval = nullptr;
}

VariantParam::VariantParam(Type type) : type(type)
{
    this->value.ptrval = nullptr;
}

VariantParam::VariantParam(const VariantParam &src) :
    VariantParam(src.type)
{
    if( type == Type::String )
    {
        if( !( value.ptrval = strdup((const char*)src.value.ptrval) ) )
            throw bad_alloc();
    }
    else
    {
        value = src.value;
    }
}

VariantParam::VariantParam(VariantParam &&src) :
    VariantParam(src.type)
{
    value = src.value;
    src.value.ptrval = nullptr;
}

VariantParam::~VariantParam()
{
    if( type == Type::String && value.ptrval )
        free(value.ptrval);
}

VariantParam& VariantParam::operator=(const VariantParam &src)
{
    if( type == Type::String && value.ptrval )
        free(value.ptrval);

    type = src.type;

    if( type == Type::String )
    {
        if( !( value.ptrval = strdup((const char*)src.value.ptrval) ) )
            throw bad_alloc();
    }
    else
    {
        value = src.value;
    }

    return *this;
}

VariantParam& VariantParam::operator=(VariantParam &&src)
{
    type = src.type;
    value = src.value;
    src.value.ptrval = nullptr;

    return *this;
}

VariantParam VariantParam::MakeBool(bool value)
{
    VariantParam param(Type::Bool);
    param.value.boolval = value;
    return param;
}

VariantParam VariantParam::MakeInt(int value)
{
    VariantParam param(Type::Int);
    param.value.intval = value;
    return param;
}

VariantParam VariantParam::MakeUint(unsigned value)
{
    VariantParam param(Type::Uint);
    param.value.uintval = value;
    return param;
}

VariantParam VariantParam::MakePointer(void *value)
{
    VariantParam param(Type::Pointer);
    param.value.ptrval = value;
    return param;
}

VariantParam VariantParam::MakeString(const char *value)
{
    VariantParam param(Type::String);

    if( !( param.value.ptrval = strdup(value) ) )
        throw bad_alloc();

    return param;
}

VariantParam VariantParam::MakeNetAddr(const sockaddr_t *value)
{
    VariantParam param(Type::NetAddr);
    param.value.addrval = *value;
    return param;
}

bool VariantParam::GetBool(bool failval) const
{
    return type == Type::Bool ? value.boolval : failval;
}

int VariantParam::GetInt(int failval) const
{
    return type == Type::Int ? value.intval : failval;
}

unsigned VariantParam::GetUint(unsigned failval) const
{
    return type == Type::Uint ? value.uintval : failval;
}

void* VariantParam::GetPointer(void *failval) const
{
    return type == Type::Pointer ? value.ptrval : failval;
}

const char* VariantParam::GetString(const char *failval) const
{
    return type == Type::String ? (const char*) value.ptrval : failval;
}

const sockaddr_t* VariantParam::GetNetAddr(const sockaddr_t *failval) const
{
    return type == Type::NetAddr ? &value.addrval : failval;
}



void FormParams::Clear()
{
    paramlist.clear();
}

bool FormParams::IsParamExisted(const string &name) const
{
    auto iter = paramlist.find(name);
    return iter != paramlist.end();
}

VariantParam::Type FormParams::GetType(const string &name) const
{
    auto iter = paramlist.find(name);
    return iter != paramlist.end() ?
        iter->second.GetType() :
        VariantParam::Type::Null;
}

const VariantParam& FormParams::GetParam(const string &name) const
{
    auto iter = paramlist.find(name);
    return iter != paramlist.end() ?
        iter->second :
        VariantParam::NullParam;
}

void FormParams::InsertParam(const string &name, const VariantParam &value)
{
    paramlist[name] = value;
}

void FormParams::EraseParam(const string &name)
{
    paramlist.erase(name);
}

void FormParams::InsertBoolParam(const string &name, bool value)
{
    paramlist[name] = VariantParam::MakeBool(value);
}

void FormParams::InsertIntParam(const string &name, int value)
{
    paramlist[name] = VariantParam::MakeInt(value);
}

void FormParams::InsertUintParam(const string &name, unsigned value)
{
    paramlist[name] = VariantParam::MakeUint(value);
}

void FormParams::InsertPointerParam(const string &name, void *value)
{
    paramlist[name] = VariantParam::MakePointer(value);
}

void FormParams::InsertStringParam(const string &name, const string &value)
{
    paramlist[name] = VariantParam::MakeString(value.c_str());
}

void FormParams::InsertNetAddrParam(const string &name, const SocketAddr &value)
{
    paramlist[name] = VariantParam::MakeNetAddr((const sockaddr_t*)&value);
}

bool FormParams::GetBoolParam(const string &name, bool failval) const
{
    return GetParam(name).GetBool(failval);
}

int FormParams::GetIntParam(const string &name, int failval) const
{
    return GetParam(name).GetInt(failval);
}

unsigned FormParams::GetUintParam(const string &name, unsigned failval) const
{
    return GetParam(name).GetUint(failval);
}

void* FormParams::GetPointerParam(const string &name, void *failval) const
{
    return GetParam(name).GetPointer(failval);
}

string FormParams::GetStringParam(const string &name, const string &failval) const
{
    return GetParam(name).GetString(failval.c_str());
}

SocketAddr FormParams::GetNetAddrParam(const string &name, const SocketAddr &failval) const
{
    const sockaddr_t *addr = GetParam(name).GetNetAddr((const sockaddr_t*)&failval);
    return SocketAddr(*addr);
}
