#ifndef _FORM_UTILS_H_
#define _FORM_UTILS_H_

#include "FormInterface.h"

namespace ui
{
namespace util
{

void RegisterForms(FormManager *mgr);

enum AskResult
{
    AR_YES = 0,
    AR_NO = 1,
};

void ShowMessage(
    FormManager *mgr,
    const std::string &msg,
    unsigned timeout = 0,
    Form *blockform = nullptr);
int AskYesNo(
    FormManager *mgr,
    Form *currform,
    const std::string &title,
    const std::string &prompt,
    int defaultselect = AR_YES);
bool InputString(
    FormManager *mgr,
    Form *currform,
    const std::string &title,
    const std::string &prompt,
    std::string &result);

}   // namespace util
}   // namespace ui

#endif
