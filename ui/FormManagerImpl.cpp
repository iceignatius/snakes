#include "common/tmutl.h"
#include "FormManagerImpl.h"

using namespace std;
using namespace ui;

#define TARGET_STEPTIME 50

FormManagerImpl::FormManagerImpl() :
    terminating(false), exitcode(0)
{
}

FormManagerImpl::~FormManagerImpl()
{
    Shutdown();
}

void FormManagerImpl::Setup()
{
    console.Setup();
}

void FormManagerImpl::Shutdown()
{
    form_stack.clear();
    console.Shutdown();
}

Console* FormManagerImpl::GetConsole()
{
    return &console;
}

void FormManagerImpl::RegisterFormKlass(
    const string &klass,
    Form*(*creator)(FormManager *mgr))
{
    form_factory[klass] = creator;
}

Form* FormManagerImpl::FocusForm()
{
    return form_stack.size() ? form_stack.back().get() : nullptr;
}

void FormManagerImpl::PushNewForm(const string &klass, const FormParams &params)
{
    auto iter = form_factory.find(klass);
    if( iter == form_factory.end() )
        throw invalid_argument("Unsupported form klass: " + klass);

    shared_ptr<Form> form(iter->second(this));

    form_stack.push_back(form);
    form->OnPush(params);
    form->OnFocus(FormParams());
}

void FormManagerImpl::Terminate(int exitcode)
{
    this->exitcode = exitcode;
    terminating = true;
}

int FormManagerImpl::RemoveClosedFocusForms(FormParams &params)
{
    params.Clear();

    Form *form = FocusForm();
    if( !form || form->State() != FormState::Closed )
        return 0;

    int exitcode = form->ExitCode();
    form->OnPop(params);
    form_stack.pop_back();

    while( ( form = FocusForm() ) && form->State() == FormState::Closed )
    {
        FormParams temp;
        form->OnPop(temp);
        form_stack.pop_back();
    }

    if( form )
    {
        form->Show();
        form->OnFocus(params);
    }

    return exitcode;
}

void FormManagerImpl::UpdateForms(unsigned steptime)
{
    for(auto form : form_stack)
    {
        if( form->State() <= FormState::Suspended )
            continue;

        form->Update(steptime);
    }
}

void FormManagerImpl::RenderForms()
{
    console.BeginRender();

    for(auto form : form_stack)
    {
        if( form->State() <= FormState::Hidden )
            continue;

        form->Render();
    }

    console.EndRender();
}

int FormManagerImpl::RunModal(Form *stopform, FormParams *params)
{
    FormParams temp_params;
    if( !params ) params = &temp_params;

    exitcode = 0;

    unsigned lasttime = tmutl_get_mstime();
    while( FocusForm() != stopform )
    {
        unsigned currtime = tmutl_get_mstime();
        unsigned steptime = currtime - lasttime;
        lasttime = currtime;

        exitcode = RemoveClosedFocusForms(*params);
        UpdateForms(steptime);
        RenderForms();

        unsigned proctime = tmutl_get_mstime() - currtime;
        if( proctime < TARGET_STEPTIME )
            tmutl_sleep( TARGET_STEPTIME - proctime );
    }

    return exitcode;
}
