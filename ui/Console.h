#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <string>
#include <ncurses.h>

namespace ui
{

class Console
{
private:
    bool started;

public:
    Console();
    ~Console();

public:
    void Setup();
    void Shutdown();

    void BeginRender();
    void EndRender();

    void RenderHLine(int x, int y, int len, char body = '-', char edge = '+');
    void RenderVLine(int x, int y, int len, char body = '|', char edge = '+');
    void RenderBox(
        int x,
        int y,
        int w,
        int h,
        char hline = '-',
        char vline = '|',
        char edge = '+',
        char filled = ' ' );
    void RenderMessageBox(
        const std::string &text, int x = 0, int y = 0, int w = 0, int h = 0);

    const bool* GetKeyboardState(bool state[KEY_MAX]);
    static constexpr int CtrlKeyWith(int ch) { return ~0x60 & ch; }
};

}   // namespace ui

#endif
