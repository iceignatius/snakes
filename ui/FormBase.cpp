#include <string.h>
#include "FormBase.h"

using namespace std;
using namespace ui;

FormBase::FormBase(FormManager *mgr) :
    mgr(mgr), state(FormState::Shown), exitcode(0)
{
}

FormState FormBase::State() const
{
    return
        state == FormState::Closed ? FormState::Closed :
        this == mgr->FocusForm() ? FormState::Focus :
        state;
}

int FormBase::ExitCode() const
{
    return exitcode;
}

void FormBase::Close(int exitcode)
{
    state = FormState::Closed;
    this->exitcode = exitcode;
}

void FormBase::Suspend()
{
    if( state != FormState::Closed )
        state = FormState::Suspended;
}

void FormBase::Hide()
{
    if( state != FormState::Closed )
        state = FormState::Hidden;
}

void FormBase::Show()
{
    if( state != FormState::Closed )
        state = FormState::Shown;
}

FormManager* FormBase::Manager()
{
    return mgr;
}

const bool* FormBase::GetKeyboardState(bool state[KEY_MAX])
{
    state = state ? state : kbdstate;

    if( this == mgr->FocusForm() )
    {
        return mgr->GetConsole()->GetKeyboardState(state);
    }
    else
    {
        memset(state, 0, KEY_MAX * sizeof(state[0]));
        return state;
    }
}
