#include <ncurses.h>
#include "common/ascii.h"
#include "Console.h"
#include "TextInputModule.h"

using namespace std;
using namespace ui;

string TextInputModule::PopCompletedText(const string &failval)
{
    if( state != TextInputState::COMPLETED ) return failval;

    string res = text;
    Restart();

    return res;
}

int TextInputModule::PushCharacter(int ch)
{
    if( isprint(ch) )
    {
        text.push_back(ch);
        return 1;
    }
    else if( ch == ASCII_BS && !text.empty() )
    {
        text.pop_back();
        return 1;
    }
    else if( ch == ASCII_DEL )
    {
        text.clear();
        return 1;
    }
    else
    {
        return 0;
    }
}

int TextInputModule::PushInput(const bool keystate[])
{
    if( state != TextInputState::TYPING )
        return 0;

    int count = 0;

    // Process work state keys.

    if( keystate[ASCII_ESC] || keystate[Console::CtrlKeyWith('E')] )
    {
        if( text.empty() )
            Abort();
        else
            Restart();
        ++count;
    }

    if( keystate[ASCII_CR] || keystate[KEY_ENTER] )
    {
        state = TextInputState::COMPLETED;
        ++count;
    }

    // Process for ASCII codes.

    for(int ch = 0; ch < 0x80; ++ch)
    {
        if( keystate[ch] )
            count += PushCharacter(ch);
    }

    // Process input modify keys.

    if( keystate[KEY_BACKSPACE] )
        count += PushCharacter(ASCII_BS);

    if( keystate[KEY_DC] )
        count += PushCharacter(ASCII_DEL);

    return count;
}

void TextInputModule::Disable()
{
    text.clear();
    state = TextInputState::DISABLED;
}

void TextInputModule::Abort()
{
    text.clear();
    state = TextInputState::ABORTED;
}

void TextInputModule::Restart()
{
    text.clear();
    state = TextInputState::TYPING;
}
