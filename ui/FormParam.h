#ifndef _UI_FORM_PARAM_H_
#define _UI_FORM_PARAM_H_

#include <string>
#include <map>
#include <netsock/sockbase.h>

namespace ui
{

class VariantParam
{
public:
    enum class Type
    {
        Null,
        Bool,
        Int,
        Uint,
        Pointer,
        String,
        NetAddr,
    };

private:
    union GeneralValue
    {
        bool boolval;
        int intval;
        unsigned uintval;
        void *ptrval;
        sockaddr_t addrval;
    };

    Type type;
    GeneralValue value;

public:
    VariantParam();
    VariantParam(Type type);
    VariantParam(const VariantParam &src);
    VariantParam(VariantParam &&src);
    ~VariantParam();

    VariantParam& operator=(const VariantParam &src);
    VariantParam& operator=(VariantParam &&src);

public:
    static VariantParam MakeBool(bool value);
    static VariantParam MakeInt(int value);
    static VariantParam MakeUint(unsigned value);
    static VariantParam MakePointer(void *value);
    static VariantParam MakeString(const char *value);
    static VariantParam MakeNetAddr(const sockaddr_t *value);

public:
    static VariantParam NullParam;

public:
    Type GetType() const { return type; }
    bool IsNull() const { return type == Type::Null; }
    bool IsBool() const { return type == Type::Bool; }
    bool IsInt() const { return type == Type::Int; }
    bool IsUint() const { return type == Type::Uint; }
    bool IsPointer() const { return type == Type::Pointer; }
    bool IsString() const { return type == Type::String; }
    bool IsNetAddr() const { return type == Type::NetAddr; }

    bool GetBool(bool failval) const;
    int GetInt(int failval) const;
    unsigned GetUint(unsigned failval) const;
    void* GetPointer(void *failval) const;
    const char* GetString(const char *failval) const;
    const sockaddr_t* GetNetAddr(const sockaddr_t *failval) const;
};

class FormParams
{
private:
    std::map<std::string, VariantParam> paramlist;

public:
    void Clear();

    bool IsParamExisted(const std::string &name) const;
    VariantParam::Type GetType(const std::string &name) const;
    const VariantParam& GetParam(const std::string &name) const;
    void InsertParam(const std::string &name, const VariantParam &value);
    void EraseParam(const std::string &name);

    void InsertBoolParam(const std::string &name, bool value);
    void InsertIntParam(const std::string &name, int value);
    void InsertUintParam(const std::string &name, unsigned value);
    void InsertPointerParam(const std::string &name, void *value);
    void InsertStringParam(const std::string &name, const std::string &value);
    void InsertNetAddrParam(const std::string &name, const SocketAddr &value);

    bool GetBoolParam(const std::string &name, bool failval) const;
    int GetIntParam(const std::string &name, int failval) const;
    unsigned GetUintParam(const std::string &name, unsigned failval) const;
    void* GetPointerParam(const std::string &name, void *failval) const;
    std::string GetStringParam(const std::string &name, const std::string &failval) const;
    SocketAddr GetNetAddrParam(const std::string &name, const SocketAddr &failval) const;
};

}   // namespace ui

#endif
