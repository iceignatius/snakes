#include "common/ascii.h"
#include "YesNoForm.h"

using namespace std;
using namespace ui;

YesNoForm::YesNoForm(FormManager *mgr) :
    FormBase(mgr), selectcode(0), dialog_width(12), dialog_height(6)
{
}

const char* YesNoForm::KlassName()
{
    return "yesnoform";
}

Form* YesNoForm::CreateInstance(FormManager *mgr)
{
    return new YesNoForm(mgr);
}

string YesNoForm::ReplaceControlCodes(
    const string &text,
    char replacement)
{
    string str = text;
    for(size_t i = 0; i < str.length(); ++i)
    {
        if( iscntrl(str[i]) )
            str[i] = replacement;
    }

    return str;
}

list<string> YesNoForm::ParseTextLines(
    const string &text,
    char ctrl_replacement)
{
    list<string> lines;

    string str;
    for(size_t i = 0; i < text.length(); ++i)
    {
        char ch = text[i];
        if( ch == '\n' )
        {
            lines.push_back(ReplaceControlCodes(str, ctrl_replacement));
            str.clear();
        }
        else
        {
            str.push_back(ch);
        }
    }

    if( text.back() != '\n' )
        lines.push_back(str);

    return lines;
}

void YesNoForm::OnPush(const FormParams &params)
{
    title = params.GetStringParam("title", "");
    title = ReplaceControlCodes(title, '*');

    string prompt = params.GetStringParam("prompt", "");
    prompt_lines = ParseTextLines(prompt, '*');

    unsigned max_text_width = title.length() ? title.length() + 2 : 0;
    for(string &str : prompt_lines)
    {
        if( max_text_width < str.length() )
            max_text_width = str.length();
    }

    dialog_width = max(dialog_width, (int)( 2 + max_text_width + 2 ));
    dialog_height = max(dialog_height, (int)( 2 + prompt_lines.size() + 2 + 2 ));

    selectcode = params.GetIntParam("select", selectcode);
}

void YesNoForm::OnPop(FormParams &params)
{
    params.InsertIntParam("select", selectcode);
}

void YesNoForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    if( keystate[ASCII_CR] )
        Close();

    if( keystate[KEY_LEFT] )
        selectcode = SELECT_YES;

    if( keystate[KEY_RIGHT] )
        selectcode = SELECT_NO;
}

void YesNoForm::Render()
{
    int screen_cols = getmaxx(stdscr);
    int screen_rows = getmaxy(stdscr);

    int dialog_posx =
        screen_cols >= dialog_width ?
        ( screen_cols - dialog_width ) / 2 :
        0;
    int dialog_posy =
        screen_rows >= dialog_height ?
        ( screen_rows - dialog_height ) / 2 :
        0;

    Console *console = Manager()->GetConsole();

    int separator_posy = dialog_posy + prompt_lines.size() + 4 - 1;
    console->RenderBox(dialog_posx, dialog_posy, dialog_width, dialog_height);
    console->RenderHLine(dialog_posx, separator_posy, dialog_width);

    if( title.length() )
    {
        int xoff = ( dialog_width - title.length() - 2 ) / 2;
        mvprintw(dialog_posy, dialog_posx + xoff, " %s ", title.c_str());
    }

    if( prompt_lines.size() )
    {
        int yoff = 2;
        for(string &str : prompt_lines)
        {
            int xoff = ( dialog_width - str.length() ) / 2;
            mvprintw(dialog_posy + yoff, dialog_posx + xoff, "%s", str.c_str());
            ++yoff;
        }
    }

    if( selectcode == SELECT_YES )
        attron(A_REVERSE);
    mvaddstr(separator_posy + 1, dialog_posx + 1, " YES ");
    if( selectcode == SELECT_YES )
        attroff(A_REVERSE);

    if( selectcode == SELECT_NO )
        attron(A_REVERSE);
    mvaddstr(separator_posy + 1, dialog_posx + dialog_width - 1 - 5, "  NO ");
    if( selectcode == SELECT_NO )
        attroff(A_REVERSE);
}
