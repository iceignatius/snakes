#include <limits.h>
#include "net/netdef.h"
#include "ServerNetworking.h"

using namespace std;
using namespace net;

#define LOGLABEL "SnakesServer.Networking"

ServerNetworking::ServerNetworking():
    event_handler(nullptr)
{
}

ServerNetworking::~ServerNetworking()
{
    Close();
}

void ServerNetworking::Open(
    ServerNetEvents *event_handler,
    unsigned port,
    const string &name,
    const string &password)
{
    try
    {
        this->event_handler = event_handler;

        exgr.AddPeer(new AnonPeer(
            exgr.GetSocket(),
            name,
            password,
            this,
            EventOnQuery,
            EventOnJoin));

        exgr.Open(port);
    }
    catch(exception &e)
    {
        Close();
        throw;
    }
}

void ServerNetworking::Close()
{
    exgr.Close();

    userlist.clear();
    exgr.RemoveAllPeers();

    event_handler = nullptr;
}

void ServerNetworking::RunStep(unsigned steptime)
{
    while( exgr.ReceiveDispatch() )
    {}

    exgr.RunStep(steptime);

    for(auto iter = userlist.begin(); iter != userlist.end();)
    {
        int userid = iter->first;
        UserInfo &info = iter->second;
        ++iter;

        if( !info.go_remove ) continue;

        info.remove_timer -= steptime;
        if( info.remove_timer < 0 )
        {
            userlist.erase(userid);
            exgr.RemovePeer(userid);
            printf("%s: An user removed, id=%d\n", LOGLABEL, userid);
        }
    }
}

int ServerNetworking::GenerateUserId() const
{
    auto iter = userlist.crbegin();
    if( iter == userlist.crend() )
        return 1;

    if( iter->first < INT_MAX - 1 )
        return iter->first + 1;

    for(int id = 1; id < INT_MAX; ++id)
    {
        if( userlist.find(id) == userlist.end() )
            return id;
    }

    return -1;
}

void ServerNetworking::AddUser(
    int userid,
    const SocketAddr &remote,
    const vector<uint8_t> &skey)
{
    PlayerPeer *peer = new PlayerPeer(
        userid,
        exgr.GetSocket(),
        remote,
        skey,
        this,
        EventOnError,
        EventOnQuit,
        EventOnTalk,
        EventOnSetState,
        EventOnSetDir);

    userlist[userid].peer = peer;
    exgr.AddPeer(peer);

    printf("%s: An user added, id=%d, remote=%s\n",
        LOGLABEL,
        userid,
        IPv4ToStr(remote.GetIP()).c_str());
}

void ServerNetworking::RemoveUserLater(int userid)
{
    auto iter = userlist.find(userid);
    if( iter == userlist.end() ) return;

    iter->second.go_remove = true;
    iter->second.remove_timer = NETDEF_QUIT_REMOVE_TIMEOUT;

    printf("%s: An user is going to be removed, id=%d\n", LOGLABEL, userid);
}

QueryInfo ServerNetworking::EventOnQuery(void *self)
{
    return ((ServerNetworking*)self)->OnQuery();
}

int ServerNetworking::EventOnJoin(
    void *self,
    const SocketAddr &remote,
    const vector<uint8_t> &skey,
    const string &username,
    int &userid)
{
    return ((ServerNetworking*)self)->OnJoin(remote, skey, username, userid);
}

void ServerNetworking::EventOnError(void *self, int userid, int errcode)
{
    ((ServerNetworking*)self)->OnError(userid, errcode);
}

void ServerNetworking::EventOnQuit(void *self, int userid)
{
    ((ServerNetworking*)self)->OnQuit(userid);
}

void ServerNetworking::EventOnTalk(void *self, int userid, const string &text)
{
    ((ServerNetworking*)self)->OnTalk(userid, text);
}

void ServerNetworking::EventOnSetState(void *self, int userid, int state)
{
    ((ServerNetworking*)self)->OnSetState(userid, state);
}

void ServerNetworking::EventOnSetDir(void *self, int userid, char dir)
{
    ((ServerNetworking*)self)->OnSetDir(userid, dir);
}

QueryInfo ServerNetworking::OnQuery()
{
    return event_handler ? event_handler->OnQuery() : QueryInfo();
}

int ServerNetworking::OnJoin(
    const SocketAddr &remote,
    const vector<uint8_t> &skey,
    const string &username,
    int &userid)
{
    if( !event_handler ) return CMDRC_ERR_JOIN_FULL;

    userid = GenerateUserId();
    int err = event_handler->OnJoin(userid, username);
    if( err ) return err;

    AddUser(userid, remote, skey);
    return 0;
}

void ServerNetworking::OnError(int userid, int errcode)
{
    if( !event_handler ) return;

    auto iter = userlist.find(userid);
    if( iter == userlist.end() )
        return;

    UserInfo &info = iter->second;
    if( info.go_remove )
        return;

    event_handler->OnConnError(userid, errcode);
    RemoveUserLater(userid);
}

void ServerNetworking::OnQuit(int userid)
{
    if( !event_handler ) return;

    auto iter = userlist.find(userid);
    if( iter == userlist.end() )
        return;

    UserInfo &info = iter->second;
    if( info.go_remove )
        return;

    event_handler->OnQuit(userid);
    RemoveUserLater(userid);
}

void ServerNetworking::OnTalk(int userid, const string &text)
{
    if( !event_handler ) return;

    auto iter = userlist.find(userid);
    if( iter == userlist.end() )
        return;

    UserInfo &info = iter->second;
    if( info.go_remove )
        return;

    event_handler->OnTalk(userid, text);
}

void ServerNetworking::OnSetState(int userid, int state)
{
    if( !event_handler ) return;

    auto iter = userlist.find(userid);
    if( iter == userlist.end() )
        return;

    UserInfo &info = iter->second;
    if( info.go_remove )
        return;

    event_handler->OnSetState(userid, (GameState) state);
}

void ServerNetworking::OnSetDir(int userid, char dir)
{
    if( !event_handler ) return;

    auto iter = userlist.find(userid);
    if( iter == userlist.end() )
        return;

    UserInfo &info = iter->second;
    if( info.go_remove )
        return;

    event_handler->OnSetDir(userid, dir);
}

void ServerNetworking::SendTalkToAllUsers(
    int sender,
    const string &name,
    const string &text)
{
    for(auto &iter : userlist)
        iter.second.peer->SendTalk(sender, name, text);
}

bool ServerNetworking::PushObject(const void *data, size_t size)
{
    bool have_err = false;
    for(auto &iter : userlist)
    {
        if( !iter.second.peer->PushObject(data, size) )
            have_err = true;
    }

    return !have_err;
}

bool ServerNetworking::SendThenClearObjects()
{
    bool have_err = false;
    for(auto &iter : userlist)
    {
        if( !iter.second.peer->SendObjects() )
            have_err = true;

        // Clear the quered objects no matter if send failed or not!
        iter.second.peer->ClearObjects();
    }

    return !have_err;
}
