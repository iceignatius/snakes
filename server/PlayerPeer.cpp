#include "net/AliveProcessor.h"
#include "QuitProcessor.h"
#include "SetStateProcessor.h"
#include "SetDirProcessor.h"
#include "PlayerPeer.h"

using namespace std;
using namespace net;

PlayerPeer::PlayerPeer(
    int userid,
    SocketUdp *sock,
    const SocketAddr &remote,
    const vector<uint8_t> &skey,
    void *eventhost,
    void(*OnError)(void*, int, int),
    void(*OnQuit)(void*, int),
    void(*OnTalk)(void*, int, const string&),
    void(*OnSetState)(void*, int, int),
    void(*OnSetDir)(void*, int, char)) :
        CmdPeerBase(userid, sock, skey)
{
    this->eventhost = eventhost;
    this->OnError = OnError;
    this->OnQuit = OnQuit;
    this->OnTalk = OnTalk;
    this->OnSetState = OnSetState;
    this->OnSetDir = OnSetDir;

    Active(remote);

    AliveProcessor *alive_proc = new AliveProcessor(this);
    QuitProcessor *quit_proc = new QuitProcessor(this, this, EventOnQuit);
    talk_proc = new TalkProcessor(this, this, EventOnTalk);
    update_proc = new UpdateProcessor(this);
    SetStateProcessor *state_proc = new SetStateProcessor(this, this, EventOnSetState);
    SetDirProcessor *dir_proc = new SetDirProcessor(this, this, EventOnSetDir);

    AddProcessor(alive_proc);
    AddProcessor(quit_proc);
    AddProcessor(talk_proc);
    AddProcessor(update_proc);
    AddProcessor(state_proc);
    AddProcessor(dir_proc);
}

void PlayerPeer::OnConnectionError(int errcode)
{
    OnError(eventhost, UserId(), errcode);
    CmdPeerBase::OnConnectionError(errcode);
}

void PlayerPeer::EventOnQuit(void *self)
{
    PlayerPeer *inst = (PlayerPeer*)self;
    inst->OnQuit(inst->eventhost, inst->UserId());
}

void PlayerPeer::EventOnTalk(void *self, const string &text)
{
    PlayerPeer *inst = (PlayerPeer*)self;
    inst->OnTalk(inst->eventhost, inst->UserId(), text);
}

void PlayerPeer::EventOnSetState(void *self, int state)
{
    PlayerPeer *inst = (PlayerPeer*)self;
    inst->OnSetState(inst->eventhost, inst->UserId(), state);
}

void PlayerPeer::EventOnSetDir(void *self, char dir)
{
    PlayerPeer *inst = (PlayerPeer*)self;
    inst->OnSetDir(inst->eventhost, inst->UserId(), dir);
}

bool PlayerPeer::SendTalk(int sender, const string &name, const string &text)
{
    return talk_proc->SendCommand(sender, name, text);
}

bool PlayerPeer::PushObject(const void *data, size_t size)
{
    return update_proc->Push(data, size);
}

void PlayerPeer::ClearObjects()
{
    update_proc->Clear();
}

bool PlayerPeer::SendObjects()
{
    return update_proc->Send();
}
