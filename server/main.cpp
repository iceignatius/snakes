#include <signal.h>
#include <time.h>
#include <iostream>
#include <getopt.h>
#include "common/tmutl.h"
#include "net/netdef.h"
#include "net/cmdcrypt.h"
#include "game/GameDef.h"
#include "SnakesServer.h"

using namespace std;

struct ServerOpts
{
    bool needhelp;
    uint16_t port;
    string name;
    string password;
    bool cipher_enabled;
    unsigned idle_timeout;
    string scenefile;
};

static bool terminating = false;

static
void ProcessSignalHandler(int sig)
{
    terminating = true;
}

static
void PrintHelp(const string &apname)
{
    cout << "snakes-server" << endl;
    cout << "Usage: " << apname << " [options]" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << "  -h, --help                    Show this message." << endl;
    cout << "  -p, --port=port               Port of the server" << endl;
    cout << "  -m, --name=name               Name of the chat room." << endl;
    cout << "  -w, --password=password       The password to allow clients to join." << endl;
    cout << "      --disable-cipher          Disable the cipher behaviour." << endl;
    cout << "      --idle-timeout=seconds    Set a timeout to auto terminate" << endl;
    cout << "                                when no player connected." << endl;
    cout << "                                Not set this argument or set to a zero value" << endl;
    cout << "                                will disable this behaviour." << endl;
    cout << "      --scene=file              Load and using a scene file." << endl;
}

static
ServerOpts ParseOpts(int argc, char *argv[])
{
    ServerOpts info;
    info.needhelp = false;
    info.port = NETDEF_DEFAULT_SERVER_PORT;
    info.cipher_enabled = true;
    info.idle_timeout = 0;

    static const struct option longopts[] =
    {
        { "help",           no_argument,        nullptr, 'h' },
        { "port",           required_argument,  nullptr, 'p' },
        { "name",           required_argument,  nullptr, 'm' },
        { "password",       required_argument,  nullptr, 'w' },
        { "disable-cipher", required_argument,  nullptr, 'D' },
        { "idle-timeout",   required_argument,  nullptr, 'T' },
        { "scene",          required_argument,  nullptr, 'S' },
        { nullptr, 0, nullptr, 0 }
    };

    static const char shortopts[] = "hp:m:w:";

    int opt, index;
    while( ( opt = getopt_long(argc, argv, shortopts, longopts, &index) ) >= 0 )
    {
        switch( opt )
        {
        case 'h':
            info.needhelp = true;
            break;

        case 'p':
            if( 0 == ( info.port = atoi(optarg) ) )
            {
                cerr << "ERROR: Invalid port format!" << endl;
                info.needhelp = true;
            }
            break;

        case 'm':
            info.name = optarg;
            break;

        case 'w':
            info.password = optarg;
            break;

        case 'D':
            info.cipher_enabled = false;
            break;

        case 'T':
            if( 0 == ( info.idle_timeout = 1000 * atoi(optarg) ) )
            {
                cerr << "ERROR: Invalid idle timeout!" << endl;
                info.needhelp = true;
            }
            break;

        case 'S':
            info.scenefile = optarg;
            break;

        case '?':
            cerr << "ERROR: Unsupported option: \"" << optopt << "\"" << endl;
            info.needhelp = true;
            break;
        }
    }

    return info;
}

int main(int argc, char *argv[])
{
    ServerOpts opts = ParseOpts(argc, argv);
    if( opts.needhelp )
    {
        PrintHelp(argv[0]);
        return 0;
    }

    int exitcode = 1;
    try
    {
        signal(SIGINT, ProcessSignalHandler);
#ifdef _WIN32
        signal(SIGBREAK, ProcessSignalHandler);
#else
        signal(SIGQUIT, ProcessSignalHandler);
#endif

        srand(time(nullptr));

        if( !CmdCrypt::GlobalInit() )
            throw runtime_error("Initialise cipher module failed!");

        SnakesServer game;
        game.Open(opts.port, opts.name, opts.password, opts.scenefile);

        tmutl::Timer idle_timer(opts.idle_timeout);

        unsigned lasttime = tmutl_get_mstime();
        while( !terminating )
        {
            unsigned currtime = tmutl_get_mstime();
            unsigned steptime = currtime - lasttime;
            lasttime = currtime;

            game.RunStep(steptime);

            if( opts.idle_timeout )
            {
                if( game.QueryStatus().curr_joined_num )
                    idle_timer.Reset();
                else if( idle_timer.Expired() )
                    terminating = true;
            }

            unsigned proctime = tmutl_get_mstime() - currtime;
            if( proctime < game::def::STEPTIME )
                tmutl_sleep( game::def::STEPTIME - proctime );
        }

        exitcode = 0;
    }
    catch(exception &e)
    {
        cerr << "ERROR: " << e.what() << endl;
    }
    catch(...)
    {
        cerr << "ERROR: Unknown error occurred!" << endl;
    }

    return exitcode;
}
