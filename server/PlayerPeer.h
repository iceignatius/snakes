#ifndef _PLAYER_PEER_H_
#define _PLAYER_PEER_H_

#include "net/CmdPeerBase.h"
#include "TalkProcessor.h"
#include "UpdateProcessor.h"

class PlayerPeer : public net::CmdPeerBase
{
private:
    void *eventhost;
    void(*OnError)(void *host, int userid, int errcode);
    void(*OnQuit)(void *host, int userid);
    void(*OnTalk)(void *host, int userid, const std::string &text);
    void(*OnSetState)(void *host, int userid, int state);
    void(*OnSetDir)(void *host, int userid, char dir);

    TalkProcessor *talk_proc;
    UpdateProcessor *update_proc;

public:
    PlayerPeer(
        int userid,
        SocketUdp *sock,
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        void *eventhost,
        void(*OnError)(void*, int, int),
        void(*OnQuit)(void*, int),
        void(*OnTalk)(void*, int, const std::string&),
        void(*OnSetState)(void*, int, int),
        void(*OnSetDir)(void*, int, char));

private:
    void OnConnectionError(int errcode) override;

    static void EventOnQuit(void *self);
    static void EventOnTalk(void *self, const std::string &text);
    static void EventOnSetState(void *self, int state);
    static void EventOnSetDir(void *self, char dir);

public:
    bool SendTalk(int sender, const std::string &name, const std::string &text);

    bool PushObject(const void *data, size_t size);
    void ClearObjects();
    bool SendObjects();
};

#endif
