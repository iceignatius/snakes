#ifndef _SERVER_NETWORKING_H_
#define _SERVER_NETWORKING_H_

#include "net/CmdExchangerImpl.h"
#include "AnonPeer.h"
#include "PlayerPeer.h"

class ServerNetworking : public ServerNetApi
{
private:
    net::CmdExchangerImpl exgr;

    struct UserInfo
    {
        PlayerPeer *peer;
        bool go_remove;
        int remove_timer;

        UserInfo() : peer(nullptr), go_remove(false), remove_timer(0) {}
    };

    std::map<int, UserInfo> userlist;

    ServerNetEvents *event_handler;

public:
    ServerNetworking();
    ~ServerNetworking();

public:
    void Open(
        ServerNetEvents *event_handler,
        unsigned port,
        const std::string &name,
        const std::string &password);
    void Close();

    void RunStep(unsigned steptime);

private:
    int GenerateUserId() const;

    void AddUser(
        int userid,
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey);
    void RemoveUserLater(int userid);

private:
    static QueryInfo EventOnQuery(void *self);
    static int EventOnJoin(
        void *self,
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        const std::string &username,
        int &userid);
    static void EventOnError(void *self, int userid, int errcode);
    static void EventOnQuit(void *self, int userid);
    static void EventOnTalk(void *self, int userid, const std::string &text);
    static void EventOnSetState(void *self, int userid, int state);
    static void EventOnSetDir(void *self, int userid, char dir);

    QueryInfo OnQuery();
    int OnJoin(
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        const std::string &username,
        int &userid);
    void OnError(int userid, int errcode);
    void OnQuit(int userid);
    void OnTalk(int userid, const std::string &text);
    void OnSetState(int userid, int state);
    void OnSetDir(int userid, char dir);

public:
    void SendTalkToAllUsers(
        int sender,
        const std::string &name,
        const std::string &text) override;

    bool PushObject(const void *data, size_t size) override;
    bool SendThenClearObjects() override;
};

#endif
