#include "SnakesServer.h"

using namespace std;

SnakesServer::~SnakesServer()
{
    Close();
}

void SnakesServer::Open(
    unsigned port,
    const string &name,
    const string &password,
    const string &scenefile)
{
    try
    {
        game.Open(&network, scenefile);
        network.Open(&game, port, name, password);
    }
    catch(exception &e)
    {
        Close();
        throw;
    }
}

void SnakesServer::Close()
{
    network.Close();
    game.Close();
}

void SnakesServer::RunStep(unsigned steptime)
{
    network.RunStep(steptime);
    game.RunStep(steptime);
}

QueryInfo SnakesServer::QueryStatus()
{
    return game.QueryStatus();
}
