#ifndef _SET_DIR_PROCESSOR_H_
#define _SET_DIR_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class SetDirProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnSetDir)(void *host, char dir);

public:
    SetDirProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnSetDir)(void*, char));

private:
    void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;
};

#endif
