#ifndef _UPDATE_PROCESSOR_H_
#define _UPDATE_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class UpdateProcessor : public net::CmdProcessorBase
{
private:
    std::vector<uint8_t> payload_buf;
    size_t payload_size;

public:
    UpdateProcessor(net::CmdPeer *peer);

public:
    bool Push(const void *data, size_t size);
    void Clear();

    bool Send();
};

#endif
