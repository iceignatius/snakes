#include "game/GameNetInterface.h"
#include "SetStateProcessor.h"

using namespace std;
using namespace net;

SetStateProcessor::SetStateProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnSetState)(void*, int)) :
        CmdProcessorBase(
            CMDID_SETSTATE,
            CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
            peer)
{
    this->eventhost = eventhost;
    this->OnSetState = OnSetState;
}

void SetStateProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    int state = msg.GetIntegerArg("playstate", (int) GameState::PREPARING);
    OnSetState(eventhost, state);

    CmdMsg respmsg(CmdId());
    respmsg.AddIntegerArg("playstate", state);
    respmsg.AddErrorCode();

    SendResponseMessage(respmsg, msg.sn, crc);
}
