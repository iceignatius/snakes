#ifndef _SET_STATE_PROCESSOR_H_
#define _SET_STATE_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class SetStateProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnSetState)(void *host, int state);

public:
    SetStateProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnSetState)(void*, int));

private:
    void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;
};

#endif
