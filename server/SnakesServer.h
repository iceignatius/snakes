#ifndef _SNAKES_SERVER_H_
#define _SNAKES_SERVER_H_

#include "game/SnakesGameServer.h"
#include "ServerNetworking.h"

class SnakesServer
{
private:
    SnakesGameServer game;
    ServerNetworking network;

public:
    ~SnakesServer();

public:
    void Open(
        unsigned port,
        const std::string &name,
        const std::string &password,
        const std::string &scenefile);
    void Close();

    void RunStep(unsigned steptime);

public:
    QueryInfo QueryStatus();
};

#endif
