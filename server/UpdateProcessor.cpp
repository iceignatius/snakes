#include "net/netdef.h"
#include "UpdateProcessor.h"

using namespace std;
using namespace net;

UpdateProcessor::UpdateProcessor(net::CmdPeer *peer) :
    CmdProcessorBase(CMDID_UPDATE, ACCUMULATE_SN, peer)
{
    payload_buf.resize(NETDEF_MAX_UPDATE_SIZE);
    payload_size = 0;
}

bool UpdateProcessor::Push(const void *data, size_t size)
{
    size_t encsize = ndobj_unit_encode(
        payload_buf.data() + payload_size,
        payload_buf.size() - payload_size,
        "object",
        NDOBJ_TYPE_BINARY,
        data,
        size);

    payload_size += encsize;

    return !!encsize;
}

void UpdateProcessor::Clear()
{
    payload_size = 0;
}

bool UpdateProcessor::Send()
{
    CmdMsg msg(CmdId());
    msg.flags = CMDFLAG_NO_RESP;

    msg.AddBinaryArg("object-list", payload_buf.data(), payload_size);

    return SendRequestMessage(msg);
}
