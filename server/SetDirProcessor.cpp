#include "SetDirProcessor.h"

using namespace std;
using namespace net;

SetDirProcessor::SetDirProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnSetDir)(void*, char)) :
        CmdProcessorBase(
            CMDID_SETDIR,
            CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
            peer)
{
    this->eventhost = eventhost;
    this->OnSetDir = OnSetDir;
}

void SetDirProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    OnSetDir(eventhost, msg.GetIntegerArg("dir", 0));

    CmdMsg respmsg(CmdId());
    respmsg.AddErrorCode();

    SendResponseMessage(respmsg, msg.sn, crc);
}
