#ifndef _FORM_KLASS_NAME_H_
#define _FORM_KLASS_NAME_H_

#define FORMKLASS_PLAY          "play-form"
#define FORMKLASS_PATIO         "patio-form"
#define FORMKLASS_SERVER_SETUP  "server-setup-form"
#define FORMKLASS_ABOUT         "about-form"
#define FORMKLASS_ENTRY         "entry-form"

#endif
