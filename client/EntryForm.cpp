#include "version.h"
#include "common/ascii.h"
#include "ui/FormUtils.h"
#include "FormKlassName.h"
#include "PatioForm.h"
#include "ServerSetupForm.h"
#include "AboutForm.h"
#include "EntryForm.h"

using namespace std;
using namespace ui;

EntryForm::EntryForm(FormManager *mgr) :
    FormBase(mgr), menu_sel(0)
{
    MenuItem menu;

    menu.title = "Join Game";
    menu.hotkey = 'J';
    menu.host = this;
    menu.on_menu = EventOnJoinGameMenu;
    menu_list.push_back(menu);

    menu.title = "Create Game";
    menu.hotkey = 'C';
    menu.host = this;
    menu.on_menu = EventOnCreateGameMenu;
    menu_list.push_back(menu);

    menu.title = "About";
    menu.hotkey = 'A';
    menu.host = this;
    menu.on_menu = EventOnAboutMenu;
    menu_list.push_back(menu);

    menu.title = "Exit";
    menu.hotkey = 'E';
    menu.host = this;
    menu.on_menu = EventOnQuitMenu;
    menu_list.push_back(menu);
}

const char* EntryForm::KlassName()
{
    return FORMKLASS_ENTRY;
}

Form* EntryForm::CreateInstance(FormManager *mgr)
{
    return new EntryForm(mgr);
}

void EntryForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    if( keystate[ASCII_ESC] )
        Close();

    if( keystate[KEY_UP] && menu_sel >= 1 )
        --menu_sel;

    if( keystate[KEY_DOWN] && menu_sel + 1 < menu_list.size() )
        ++menu_sel;

    if( keystate[ASCII_CR] && menu_sel < menu_list.size() )
    {
        MenuItem &menu = menu_list[menu_sel];
        menu.on_menu(menu.host);
    }

    for(MenuItem &menu : menu_list)
    {
        if( keystate[tolower(menu.hotkey)] ||
            keystate[toupper(menu.hotkey)] ||
            keystate[Console::CtrlKeyWith(toupper(menu.hotkey))] )
        {
            menu.on_menu(menu.host);
        }
    }
}

void EntryForm::Render()
{
    int scrwid = getmaxx(stdscr);
    int scrhei = getmaxy(stdscr);

    wprintw(stdscr, "\n");
    wprintw(stdscr, "\n");
    wprintw(stdscr, "        ________  _____  ___        __       __   ___  _______   ________ \n");
    wprintw(stdscr, "       /\"       )(\\\"   \\|\"  \\      /\"\"\\     |/\"| /  \")/\"     \"| /\"       )\n");
    wprintw(stdscr, "      (:   \\___/ |.\\\\   \\    |    /    \\    (: |/   /(: ______)(:   \\___/ \n");
    wprintw(stdscr, "       \\___  \\   |: \\.   \\\\  |   /\' /\\  \\   |    __/  \\/    |   \\___  \\   \n");
    wprintw(stdscr, "        __/  \\\\  |.  \\    \\. |  //  __\'  \\  (// _  \\  // ___)_   __/  \\\\  \n");
    wprintw(stdscr, "       /\" \\   :) |    \\    \\ | /   /  \\\\  \\ |: | \\  \\(:      \"| /\" \\   :) \n");
    wprintw(stdscr, "      (_______/   \\___|\\____\\)(___/    \\___)(__|  \\__)\\_______)(_______/  \n");
    wprintw(stdscr, "\n");
    wprintw(stdscr, "                   The classic snake game with multi players\n");

    int menu_row = scrhei - menu_list.size() - 4;
    for(size_t i = 0; i < menu_list.size(); ++i)
    {
        const MenuItem &menu = menu_list[i];

        if( i == menu_sel )
            attron(A_REVERSE);

        mvwprintw(
            stdscr,
            menu_row++,
            32,
            "(%c) %-12s",
            menu.hotkey,
            menu.title.c_str());

        if( i == menu_sel )
            attroff(A_REVERSE);
    }

    {
        string ap_name_and_ver =
            "snakes-v" +
            to_string(SNAKES_VER_MAJOR) + "." + to_string(SNAKES_VER_MINOR);
        int x = scrwid - ap_name_and_ver.length();
        mvwprintw(stdscr, scrhei - 2, x, ap_name_and_ver.c_str());
    }

    attron(A_REVERSE);
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 1, x, ' ');
    mvwprintw(stdscr, scrhei - 1, 0, " Use ARROW and ENTER keys to select and enter menu");
    attroff(A_REVERSE);
}

void EntryForm::OnPush(const FormParams &params)
{
    username = params.GetStringParam("username", "");
}

void EntryForm::OnJoinGameMenu()
{
    FormParams params;
    params.InsertStringParam("username", username);

    Manager()->PushNewForm(PatioForm::KlassName(), params);
    Suspend();
}

void EntryForm::OnCreateGameMenu()
{
    FormParams params;
    params.InsertStringParam("username", username);

    Manager()->PushNewForm(ServerSetupForm::KlassName(), params);
    Suspend();
}

void EntryForm::OnAboutMenu()
{
    Manager()->PushNewForm(AboutForm::KlassName(), FormParams());
    Suspend();
}

void EntryForm::OnQuitMenu()
{
    Close();
}
