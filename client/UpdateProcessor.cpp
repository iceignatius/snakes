#include <assert.h>
#include <string.h>
#include "UpdateProcessor.h"

using namespace std;
using namespace net;

UpdateProcessor::UpdateProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnUpdate)(void*, const void*, size_t)) :
        CmdProcessorBase(CMDID_UPDATE, ACCUMULATE_SN, peer)
{
    this->eventhost = eventhost;
    this->OnUpdate = OnUpdate;
}

void UpdateProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    vector<uint8_t> buf = msg.GetBinaryArg("object-list");

    const uint8_t *bufpos = buf.data();
    size_t bufsize = buf.size();
    while( bufsize )
    {
        const char *name;
        size_t namelen;
        int type;
        const void *body;
        size_t bodysize;

        size_t rsz = ndobj_unit_decode(
            bufpos,
            bufsize,
            &name,
            &namelen,
            &type,
            &body,
            &bodysize);
        if( !rsz )
            break;

        assert( rsz <= bufsize );
        bufpos += rsz;
        bufsize -= rsz;

        static const char target_name[] = "object";
        if( namelen != sizeof(target_name) - 1 )
            continue;
        if( memcmp(name, target_name, namelen) )
            continue;
        if( type != NDOBJ_TYPE_BINARY )
            continue;

        OnUpdate(eventhost, body, bodysize);
    }
}
