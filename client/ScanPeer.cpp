#include "net/netdef.h"
#include "ScanPeer.h"

using namespace std;

ScanPeer::ScanPeer(
    SocketUdp *sock,
    void *eventhost,
    void(*OnServerInfo)(void*, const ServerInfo&)) :
        CmdPeerBase(CMDSENDER_ANON, sock), sock(sock)
{
    query_proc = new QueryProcessor(this, eventhost, OnServerInfo);
    AddProcessor(query_proc);
}

void ScanPeer::SendQuery(const SocketAddr &addr)
{
    for(int i = 0; i < NETDEF_RESEND_TRYCOUNT; ++i)
    {
        if( !query_proc->SendCommand(addr) )
            throw runtime_error("Send query command failed!");
    }
}

void ScanPeer::SendQuery()
{
    SocketAddr addr(sock->GetBroadcastIP(), NETDEF_DEFAULT_SERVER_PORT);
    SendQuery(addr);
}
