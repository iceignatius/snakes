#ifndef _PLAY_FORM_H_
#define _PLAY_FORM_H_

#include "SnakesClient.h"
#include "ui/FormBase.h"
#include "ui/TextInputModule.h"

class PlayForm : public ui::FormBase
{
private:
    SnakesClient client;

    struct TalkInfo
    {
        int sender;
        std::string name;
        std::string text;
    };

    std::list<TalkInfo> talklist;
    ui::TextInputModule talkinput;

private:
    PlayForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    void OnPush(const ui::FormParams &params) override;
    void OnPop(ui::FormParams &params) override;

private:
    void ProcFormKeys(const bool keystate[]);
    void ProcTalkList();
    void ProcTalkInput(const bool keystate[]);
    void ProcGameInput(const bool keystate[]);
    void CloseIfErrorState();

public:
    void Update(unsigned steptime) override;

private:
    int SelectEntityColour(const game::Object *obj) const;
    void RenderPlayAreaBorder();
    void RenderGameObjects();
    void RenderGameState();
    void RenderRankList();
    void RenderTalkList();
    void RenderTalkInput();
    void RenderHelperRow();
    void RenderClientState();

public:
    void Render() override;
};

#endif
