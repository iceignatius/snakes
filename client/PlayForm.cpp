#include <string.h>
#include "game/GameState.h"
#include "game/GamePlayer.h"
#include "game/GameEntity.h"
#include "ui/ConsoleColour.h"
#include "ui/FormUtils.h"
#include "FormKlassName.h"
#include "PlayForm.h"

using namespace std;
using namespace ui;

PlayForm::PlayForm(FormManager *mgr) :
    FormBase(mgr)
{
    talkinput.Disable();
}

const char* PlayForm::KlassName()
{
    return FORMKLASS_PLAY;
}

Form* PlayForm::CreateInstance(FormManager *mgr)
{
    return new PlayForm(mgr);
}

void PlayForm::OnPush(const FormParams &params)
{
    SocketAddr addr = params.GetNetAddrParam("addr", SocketAddr());
    string username = params.GetStringParam("username", "");
    string password = params.GetStringParam("password", "");

    if( addr.IsAvailable() )
        client.Connect(addr, username, password);
}

void PlayForm::OnPop(FormParams &params)
{
    client.Disconnect();
}

void PlayForm::ProcFormKeys(const bool keystate[])
{
    if( keystate[Console::CtrlKeyWith('E')] &&
        talkinput.State() != TextInputState::TYPING )
    {
        if( util::AskYesNo(Manager(), this, "Confirm", "Quit game?", util::AR_YES)
            == util::AR_YES )
        {
            Close();
        }
    }
}

void PlayForm::ProcTalkList()
{
    TalkInfo talk;
    if( !client.ReceiveText(talk.sender, talk.name, talk.text) ) return;

    talklist.push_back(talk);

    static const int ranklines = 10;
    int scrhei = getmaxy(stdscr);
    int talklines = scrhei - ranklines - 1 - 1;

    while( (int) talklist.size() > talklines )
        talklist.pop_front();
}

void PlayForm::ProcTalkInput(const bool keystate[])
{
    if( talkinput.State() == TextInputState::DISABLED )
    {
        if( keystate[Console::CtrlKeyWith('T')] )
            talkinput.Restart();
        else
            return;
    }

    talkinput.PushInput(keystate);

    if( talkinput.State() == TextInputState::COMPLETED )
    {
        string text = talkinput.PopCompletedText();
        if( !text.empty() )
            client.SendText(text);

        talkinput.Disable();
    }
}

void PlayForm::ProcGameInput(const bool keystate[])
{
    if( keystate[Console::CtrlKeyWith('G')] )
        client.SetPlayerReady();

    if( keystate[KEY_UP] )
        client.SetPlayerDir('N');
    if( keystate[KEY_DOWN] )
        client.SetPlayerDir('S');
    if( keystate[KEY_LEFT] )
        client.SetPlayerDir('W');
    if( keystate[KEY_RIGHT] )
        client.SetPlayerDir('E');
}

void PlayForm::CloseIfErrorState()
{
    string errmsg;
    switch( client.GetState(&errmsg) )
    {
    case ClientState::Disconnected:
    case ClientState::ConnectFailed:
    case ClientState::ConnectionError:
        util::ShowMessage(Manager(), errmsg);
        Close();
        break;

    default:
        break;
    }
}

void PlayForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    ProcFormKeys(keystate);
    ProcTalkList();
    ProcTalkInput(keystate);
    ProcGameInput(keystate);
    CloseIfErrorState();

    client.RunStep(steptime);
}

void PlayForm::RenderPlayAreaBorder()
{
    int scrhei = getmaxy(stdscr);
    int panel_hei = scrhei - 1;
    int map_y_diff = panel_hei - game::def::MAP_HEIGHT;
    int map_y_offset = map_y_diff >> 1;

    if( map_y_offset > 0 )
    {
        int y = map_y_offset - 1;
        for(int x = 0; x < game::def::MAP_WIDTH; ++x)
            mvwaddch(stdscr, y, x, '-');
    }

    if( map_y_diff >= 2 )
    {
        int y = map_y_offset + game::def::MAP_HEIGHT;
        for(int x = 0; x < game::def::MAP_WIDTH; ++x)
            mvwaddch(stdscr, y, x, '-');
    }

    for(int y = 0; y < panel_hei; ++y)
        mvwaddch(stdscr, y, game::def::MAP_WIDTH, '|');
}

int PlayForm::SelectEntityColour(const game::Object *obj) const
{
    const game::Entity *entity = dynamic_cast<const game::Entity*>(obj);
    if( !entity ) return colour.DEFAULT;

    int colour_pair = entity->Colour();
    if( colour_pair >= 0 ) return colour_pair;

    const game::Player *player =
        dynamic_cast<const game::Player*>(client.Pool().GetAlive(entity->Owner()));
    if( !player )
        return colour.DEFAULT;

    return player->Id() == client.SelfPlayerId() ?
        colour.SELFPLAYER :
        colour.ColourPairFromPlayerIndex(player->PlayerIndex());
}

void PlayForm::RenderGameObjects()
{
    int scrhei = getmaxy(stdscr);
    int panel_hei = scrhei - 1;
    int map_y_diff = panel_hei - game::def::MAP_HEIGHT;
    int map_y_offset = map_y_diff >> 1;

    for(const game::Object &obj : client.Pool())
    {
        if( !obj.IsAlive() ) continue;

        const game::Entity *entity = dynamic_cast<const game::Entity*>(&obj);
        if( !entity ) continue;

        char graph = entity->Graph();
        if( !graph ) continue;

        int x = entity->PosX();
        int y = entity->PosY();
        if( x < 0 || game::def::MAP_WIDTH <= x ) continue;
        if( y < 0 || game::def::MAP_HEIGHT <= y ) continue;
        y = game::def::MAP_HEIGHT - 1 - y + map_y_offset;

        int colour_pair = SelectEntityColour(entity);

        colour.Begin(colour_pair);
        mvwaddch(stdscr, y, x, graph);
        colour.End();
    }
}

void PlayForm::RenderGameState()
{
    string temp;
    if( client.GetState(&temp) != ClientState::Connected )
        return;

    const game::PlayState *playstate =
        dynamic_cast<const game::PlayState*>(client.Pool().GetAlive(game::def::PLAYSTATE_ID));

    GameState state = playstate ? playstate->GetState() : GameState::PREPARING;
    unsigned remain = playstate ? playstate->GetStartRemainTime() : 0;

    const PlayerInfo *player = client.FindPlayerInfo(client.SelfPlayerId());

    char text[64] = {0};
    switch(state)
    {
    case GameState::PREPARING:
        if( client.IsPlayerReady() )
            snprintf(text, sizeof(text), "WAIT for others");
        else
            snprintf(text, sizeof(text), "Press Ctrl+G to ready");
        break;

    case GameState::STARTING:
        snprintf(text, sizeof(text), "START in: %u", ( remain + 1000 - 1 )/1000);
        break;

    case GameState::PAUSED:
        snprintf(text, sizeof(text), "PAUSED");
        break;

    case GameState::PLAYING:
        break;

    case GameState::FINISHED:
        snprintf(
            text,
            sizeof(text),
            player && player->is_ready ? "WAIT for others" :
            player && player->alive ? "WINNER (Ctrl+G play again)" :
            "GAME OVER (Ctrl+G play again)");
        break;
    }

    if( strlen(text) )
    {
        int scrhei = getmaxy(stdscr);
        int panel_hei = scrhei - 1;
        int panel_wid = game::def::MAP_WIDTH;
        Manager()->GetConsole()->RenderMessageBox(text, 0, 0, panel_wid, panel_hei);
    }
}

void PlayForm::RenderRankList()
{
    multimap<int, PlayerInfo> ranklist;
    for(auto iter : client.PlayersInfo())
    {
        const PlayerInfo &player = iter.second;
        ranklist.insert(pair<int, PlayerInfo>(player.rank, player));
    }

    static const int ranklines = 10;
    int posx = game::def::MAP_WIDTH + 1;
    int posy = 0;
    mvwprintw(stdscr, posy++, posx+13, "RANK");

    for(auto iter : ranklist)
    {
        PlayerInfo &player = iter.second;

        int colour_pair =
            client.SelfPlayerId() == player.id ?
            colour.SELFPLAYER :
            colour.ColourPairFromPlayerIndex(player.index);

        char name[13+1] = {0};
        strncpy(name, player.name.c_str(), sizeof(name)-1);

        mvwprintw(stdscr, posy, posx+0, "%2d ", player.rank);
        colour.Begin(colour_pair);
        mvwprintw(stdscr, posy, posx+3, "%s", name);
        colour.End();
        mvwprintw(stdscr, posy, posx+17, "len=%3u kill=%1u ", player.len, player.kills);

        if( ++posy >= ranklines )
            break;
    }

    int scrwid = getmaxx(stdscr);
    for(int x = posx; x < scrwid; ++x)
        mvwaddch(stdscr, ranklines - 1, x, '-');
}

void PlayForm::RenderTalkList()
{
    static const int ranklines = 10;
    int posx = game::def::MAP_WIDTH + 1;
    int posy = ranklines;

    for(const TalkInfo &talk : talklist)
    {
        move(posy++, posx);

        const PlayerInfo *player = client.FindPlayerInfo(talk.sender);
        if( player )
        {
            int colour_pair =
                client.SelfPlayerId() == talk.sender ?
                colour.SELFPLAYER :
                colour.ColourPairFromPlayerIndex(player->index);

            colour.Begin(colour_pair);
            wprintw(stdscr, "%s", player->name.c_str());
            colour.End();
        }
        else
        {
            wprintw(stdscr, "%s", talk.name.c_str());
        }

        wprintw(stdscr, ": %s", talk.text.c_str());
    }
}

void PlayForm::RenderTalkInput()
{
    if( talkinput.State() != TextInputState::TYPING )
        return;

    int scrwid = getmaxx(stdscr);
    int scrhei = getmaxy(stdscr);
    int posx = game::def::MAP_WIDTH + 1;
    int posy = scrhei - 2;

    attron(A_REVERSE);
    for(int x = posx; x < scrwid; ++x)
        mvwaddch(stdscr, posy, x, ' ');
    mvwprintw(stdscr, posy, posx, "%s", talkinput.BufferedText().c_str());
    attroff(A_REVERSE);
}

void PlayForm::RenderHelperRow()
{
    int scrwid = getmaxx(stdscr);
    int scrhei = getmaxy(stdscr);

    attron(A_REVERSE);
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 1, x, ' ');
    mvwprintw(stdscr, scrhei - 1, 0, " Ctrl+T: talk | Ctrl+E: exit");
    attroff(A_REVERSE);
}

void PlayForm::RenderClientState()
{
    string msg;
    switch( client.GetState(&msg) )
    {
    case ClientState::Connected:
    case ClientState::Disconnected:
        break;

    case ClientState::ConnectFailed:
    case ClientState::ConnectionError:
    case ClientState::Querying:
    case ClientState::Joining:
        Manager()->GetConsole()->RenderMessageBox(msg);
        break;
    }
}

void PlayForm::Render()
{
    RenderPlayAreaBorder();
    RenderGameObjects();
    RenderGameState();
    RenderRankList();
    RenderTalkList();
    RenderTalkInput();
    RenderHelperRow();
    RenderClientState();
}
