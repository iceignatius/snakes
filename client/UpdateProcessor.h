#ifndef _UPDATE_PROCESSOR_H_
#define _UPDATE_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class UpdateProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnUpdate)(void *host, const void *data, size_t size);

public:
    UpdateProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnUpdate)(void*, const void*, size_t));

private:
    void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;
};

#endif
