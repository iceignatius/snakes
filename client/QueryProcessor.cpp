#include "QueryProcessor.h"

using namespace net;

QueryProcessor::QueryProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnQuery)(void*, const ServerInfo&)) :
        CmdProcessorBase(CMDID_QUERY, CmdProcessorBase::ALLOW_UNMANAGED_RESP, peer)
{
    this->eventhost = eventhost;
    this->OnQuery = OnQuery;
}

void QueryProcessor::OnResponseMessage(
    const CmdMsg &msg,
    const SocketAddr &srcaddr)
{
    struct ServerInfo info =
    {
        .addr = srcaddr,
        .name = msg.GetStringArg("name"),
        .playstate = (GameState) msg.GetUnsignedArg("play-state", -1),
        .curr_players = msg.GetUnsignedArg("curr-joined-num", 0),
        .max_players = msg.GetUnsignedArg("max-joined-num", 0),
        .need_password = msg.GetBooleanArg("need-password", false),
        .pubkey = msg.GetBinaryArg("public-key"),
    };

    OnQuery(eventhost, info);
}

bool QueryProcessor::SendCommand()
{
    CmdMsg msg(CmdId());
    return SendRequestMessage(msg);
}

bool QueryProcessor::SendCommand(const SocketAddr &addr)
{
    CmdMsg msg(CmdId());
    return SendUnmanagedMessage(msg, addr);
}
