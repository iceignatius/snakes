#include <netsock/urladdr.h>
#include "common/ascii.h"
#include "net/netdef.h"
#include "ui/FormUtils.h"
#include "FormKlassName.h"
#include "PatioForm.h"

using namespace std;
using namespace ui;

static
const char* ServerStateToStr(GameState state)
{
    switch(state)
    {
    case GameState::PREPARING:
        return "joinable";

    case GameState::STARTING:
    case GameState::PAUSED:
    case GameState::PLAYING:
        return "playing";

    case GameState::FINISHED:
        return "finished";
    }

    return "unknown";
}

PatioForm::PatioForm(FormManager *mgr) :
    FormBase(mgr), curr_sel(0)
{
    exgr.Open(0, true);

    scan_peer = new ScanPeer(exgr.GetSocket(), this, EventOnServerInfo);
    exgr.AddPeer(scan_peer);
}

const char* PatioForm::KlassName()
{
    return FORMKLASS_PATIO;
}

Form* PatioForm::CreateInstance(FormManager *mgr)
{
    return new PatioForm(mgr);
}

void PatioForm::Rescan()
{
    curr_sel = 0;
    infolist.clear();

    scan_peer->SendQuery();

    for(const SocketAddr &addr : addit_remote_list)
        scan_peer->SendQuery(addr);
}

void PatioForm::AddAdditionalAddress()
{
    string url;
    if( !util::InputString(
        Manager(),
        this,
        "Add Remote",
        "Input additional remote",
        url) )
    {
        return;
    }

    SocketAddr addr = UrlAddr::GetAddrFromUrl(url, NETDEF_DEFAULT_SERVER_PORT);
    if( !addr.IsAvailable() )
    {
        util::ShowMessage(Manager(), "Unresolved remote URL!", 5000);
        return;
    }

    addit_remote_list.insert(addr);
    Rescan();
}

bool PatioForm::PushPlayForm(const ServerInfo &remote)
{
    FormParams params;
    params.InsertNetAddrParam("addr", remote.addr);
    params.InsertStringParam("username", username);

    if( remote.need_password )
    {
        string password;

        if( !util::InputString(
            Manager(),
            this,
            "Password required",
            "Please input\nthe login password",
            password) )
        {
            return false;
        }

        if( password.empty() )
            return false;

        params.InsertStringParam("password", password);
    }

    Manager()->PushNewForm(FORMKLASS_PLAY, params);
    return true;
}

void PatioForm::Update(unsigned steptime)
{
    exgr.RunStep(steptime);

    const bool *keystate = GetKeyboardState(nullptr);

    if( keystate[Console::CtrlKeyWith('E')] )
        Close();

    if( keystate[KEY_UP] && curr_sel >= 1 )
        --curr_sel;

    if( keystate[KEY_DOWN] && curr_sel + 1 < infolist.size() )
        ++curr_sel;

    if( keystate[ASCII_CR] && curr_sel < infolist.size() )
    {
        if( PushPlayForm(infolist[curr_sel]) )
            Suspend();
    }

    if( keystate[Console::CtrlKeyWith('R')] )
        Rescan();

    if( keystate[Console::CtrlKeyWith('A')] )
        AddAdditionalAddress();
}

void PatioForm::Render()
{
    int scrwid = getmaxx(stdscr);
    int scrhei = getmaxy(stdscr);

    mvwprintw(stdscr, 0, 0, "Servers: %zu", infolist.size());

    mvwprintw(
        stdscr,
        1,
        0,
        "%-4s%-44s%-16s%-8s%-8s",
        "",
        "Name",
        "Password",
        "Players",
        "Status");

    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, 2, x, '-');
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 2, x, '-');

    if( infolist.size() )
    {
        unsigned list_rows = scrhei - 3 - 2;
        unsigned begin_index =
            curr_sel > list_rows / 2 ?
            curr_sel - list_rows / 2 :
            0;
        unsigned end_index =
            begin_index + list_rows < infolist.size() ?
            begin_index + list_rows :
            infolist.size();

        int item_row = 3;
        for(unsigned i = begin_index; i != end_index; ++i)
        {
            const ServerInfo &info = infolist[i];

            if( i == curr_sel )
                attron(A_REVERSE);

            string name =
                info.name.length() ?
                info.name :
                IPv4ToStr(info.addr.GetIP());

            mvwprintw(
                stdscr,
                item_row++,
                0,
                "%-4u%-44s%-16s%2u/%-2u%3s%s",
                i,
                name.c_str(),
                info.need_password ? "required" : "",
                info.curr_players,
                info.max_players,
                "",
                ServerStateToStr(info.playstate));

            if( i == curr_sel )
                attroff(A_REVERSE);
        }
    }

    attron(A_REVERSE);
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 1, x, ' ');
    mvwprintw(
        stdscr,
        scrhei - 1,
        0,
        " ENTER: join | Ctrl+R: rescan | Ctrl+A: manual add | Ctrl+E: exit");
    attroff(A_REVERSE);
}

void PatioForm::OnPush(const FormParams &params)
{
    username = params.GetStringParam("username", "");
}

void PatioForm::OnFocus(const FormParams &params)
{
    Rescan();
}

void PatioForm::EventOnServerInfo(void *self, const ServerInfo &info)
{
    ((PatioForm*)self)->OnServerInfo(info);
}

void PatioForm::OnServerInfo(const ServerInfo &info)
{
    for(auto item : infolist)
    {
        if( item.addr.GetIP().val == info.addr.GetIP().val &&
            item.addr.GetPort() == info.addr.GetPort() )
        {
            return;
        }
    }

    infolist.push_back(info);
}
