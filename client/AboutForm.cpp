#include "version.h"
#include "common/ascii.h"
#include "FormKlassName.h"
#include "AboutForm.h"

using namespace std;
using namespace ui;

AboutForm::AboutForm(FormManager *mgr) :
    FormBase(mgr)
{
}

const char* AboutForm::KlassName()
{
    return FORMKLASS_ABOUT;
}

Form* AboutForm::CreateInstance(FormManager *mgr)
{
    return new AboutForm(mgr);
}

void AboutForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    if( keystate[Console::CtrlKeyWith('E')] )
        Close();

    if( keystate[ASCII_CR] )
        Close();
}

void AboutForm::Render()
{
    int scrwid = getmaxx(stdscr);
    int scrhei = getmaxy(stdscr);

    wprintw(stdscr, "  ____              _             \n");
    wprintw(stdscr, " / ___| _ __   __ _| | _____  ___ \n");
    wprintw(stdscr, " \\___ \\| '_ \\ / _` | |/ / _ \\/ __|\n");
    wprintw(stdscr, "  ___) | | | | (_| |   <  __/\\__ \\\n");
    wprintw(stdscr, " |____/|_| |_|\\__,_|_|\\_\\___||___/\n");
    wprintw(stdscr, "\n");
    wprintw(stdscr, "Snakes\n");
    wprintw(stdscr, "version %d.%d\n", SNAKES_VER_MAJOR, SNAKES_VER_MINOR);
    wprintw(stdscr, "\n");
    wprintw(stdscr, "The classic snake game with multi players\n");
    wprintw(stdscr, "\n");
    wprintw(stdscr, "Copyright (c) 2021 Ignacio,\n");
    wprintw(stdscr, "see (https://gitlab.com/iceignatius/snakes) for more information.\n");
    wprintw(stdscr, "\n");
    wprintw(stdscr, "Author: Ignacio\n");
    wprintw(stdscr, "\n");
    wprintw(stdscr, "Thanks for:\n");
    wprintw(stdscr, "* The \"NCurses\" (https://invisible-island.net/ncurses/announce.html)\n");
    wprintw(stdscr, "* The \"PSCurses\" (https://www.projectpluto.com/win32a.htm)\n");
    wprintw(stdscr, "* The \"LibTomCrypt\" (https://www.libtom.net/LibTomCrypt/)\n");
    wprintw(stdscr, "* The \"Text to ASCII Art Generator\" (http://patorjk.com/software/taag/)\n");
    wprintw(stdscr, "* The \"Text to ASCII Art Generator\" (http://patorjk.com/software/taag/)\n");

    attron(A_REVERSE);
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 1, x, ' ');
    mvwprintw(
        stdscr,
        scrhei - 1,
        0,
        " ENTER or Ctrl+E: exit");
    attroff(A_REVERSE);
}
