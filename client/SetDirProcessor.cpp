#include "SetDirProcessor.h"

using namespace std;
using namespace net;

SetDirProcessor::SetDirProcessor(CmdPeer *peer) :
    CmdProcessorBase(
        CMDID_SETDIR,
        CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
        peer)
{
}

bool SetDirProcessor::SendPlayerDirCommand(char dir)
{
    CmdMsg msg(CmdId());

    msg.AddIntegerArg("dir", dir);

    return SendRequestMessage(msg);
}
