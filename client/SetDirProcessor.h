#ifndef _SET_DIR_PROCESSOR_H_
#define _SET_DIR_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class SetDirProcessor : public net::CmdProcessorBase
{
public:
    SetDirProcessor(net::CmdPeer *peer);

public:
    bool SendPlayerDirCommand(char dir);
};

#endif
