#include "QuitProcessor.h"

using namespace net;

QuitProcessor::QuitProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnQuit)(void*)) :
        CmdProcessorBase(CMDID_QUIT, CmdProcessorBase::ACCUMULATE_SN, peer)
{
    this->eventhost = eventhost;
    this->OnQuit = OnQuit;
}

void QuitProcessor::OnResponseMessage(
    const CmdMsg &msg,
    const SocketAddr &srcaddr)
{
    OnQuit(eventhost);
}

bool QuitProcessor::SendCommand()
{
    CmdMsg msg(CmdId());
    return SendRequestMessage(msg);
}
