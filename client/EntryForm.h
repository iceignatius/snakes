#ifndef _ENTRY_FORM_H_
#define _ENTRY_FORM_H_

#include <vector>
#include "ui/FormBase.h"

class EntryForm : public ui::FormBase
{
private:
    struct MenuItem
    {
        std::string title;
        char hotkey;
        EntryForm *host;
        void(*on_menu)(EntryForm*);
    };

    std::vector<MenuItem> menu_list;
    unsigned menu_sel;

    std::string username;

private:
    EntryForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

public:
    virtual void Update(unsigned steptime) override;
    virtual void Render() override;

private:
    virtual void OnPush(const ui::FormParams &params) override;

private:
    static void EventOnJoinGameMenu(EntryForm *self) { self->OnJoinGameMenu(); }
    static void EventOnCreateGameMenu(EntryForm *self) { self->OnCreateGameMenu(); }
    static void EventOnAboutMenu(EntryForm *self) { self->OnAboutMenu(); }
    static void EventOnQuitMenu(EntryForm *self) { self->OnQuitMenu(); }

    void OnJoinGameMenu();
    void OnCreateGameMenu();
    void OnAboutMenu();
    void OnQuitMenu();
};

#endif
