#ifndef _PATIO_FORM_H_
#define _PATIO_FORM_H_

#include <set>
#include "ui/FormBase.h"
#include "net/CmdExchangerImpl.h"
#include "ScanPeer.h"

class PatioForm : public ui::FormBase
{
private:
    std::string username;

    std::set<SocketAddr> addit_remote_list;
    std::vector<ServerInfo> infolist;
    unsigned curr_sel;

    net::CmdExchangerImpl exgr;
    ScanPeer *scan_peer;

private:
    PatioForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    void Rescan();
    void AddAdditionalAddress();
    bool PushPlayForm(const ServerInfo &remote);

public:
    virtual void Update(unsigned steptime) override;
    virtual void Render() override;

private:
    virtual void OnPush(const ui::FormParams &params) override;
    virtual void OnFocus(const ui::FormParams &params) override;

private:
    static void EventOnServerInfo(void *self, const ServerInfo &info);

    void OnServerInfo(const ServerInfo &info);
};

#endif
