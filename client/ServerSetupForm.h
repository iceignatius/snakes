#ifndef _SERVER_SETUP_FORM_H_
#define _SERVER_SETUP_FORM_H_

#include <vector>

#ifdef _WIN32
#   include <windows.h>
#else
#   include <unistd.h>
#endif

#include "ui/FormBase.h"

class ServerSetupForm : public ui::FormBase
{
private:
    struct SceneInfo
    {
        std::string filename;
        std::string scenename;
        unsigned players;
    };

private:
    std::string username;

    unsigned port;
    std::string gamename;
    std::string password;

    std::vector<SceneInfo> scenelist;
    unsigned curr_sel;

#ifdef _WIN32
    PROCESS_INFORMATION *server_procinfo;
#else
    pid_t server_pid;
#endif

private:
    ServerSetupForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

public:
    virtual void Update(unsigned steptime) override;
    virtual void Render() override;

private:
    virtual void OnPush(const ui::FormParams &params) override;
    virtual void OnPop(ui::FormParams &params) override;
    virtual void OnFocus(const ui::FormParams &params) override;

private:
    void RescanScenes();
    void PushPlayForm();
    bool ExecuteServerProcess();
    void TerminateServerProcess();
};

#endif
