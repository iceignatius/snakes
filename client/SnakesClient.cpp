#include "net/netdef.h"
#include "net/cmdcrypt.h"
#include "game/GamePlayer.h"
#include "SnakesClient.h"

using namespace std;

SnakesClient::~SnakesClient()
{
    Disconnect();
}

ClientState SnakesClient::GetState(string *msg) const
{
    return network.GetState(msg);
}

void SnakesClient::Connect(
    const SocketAddr &addr,
    const string &username,
    const string &password)
{
    try
    {
        game.Open(&network);
        network.Connect(&game, addr, username, password);
    }
    catch(exception &e)
    {
        Disconnect();
        throw;
    }
}

void SnakesClient::Disconnect()
{
    network.DisconnectAndClear();
    game.Close();
}

void SnakesClient::UpdatePlayerList()
{
    playerlist.clear();

    for(const game::Object &obj : game.Pool())
    {
        const game::Player *player = dynamic_cast<const game::Player*>(&obj);
        if( !player || !player->IsAlive() ) continue;

        PlayerInfo info;
        info.id = player->Id();
        info.name = player->Name();
        info.index = player->PlayerIndex();
        info.rank = player->Rank();
        info.len = player->Length();
        info.kills = player->KillCount();
        info.alive = player->IsSnakeAlive();
        info.is_ready = player->IsReady();

        playerlist[info.id] = info;
    }
}

void SnakesClient::RunStep(unsigned steptime)
{
    network.RunStep(steptime);
    game.RunStep(steptime);

    UpdatePlayerList();
}

void SnakesClient::SendText(const string &text)
{
    network.SendText(text);
}

bool SnakesClient::ReceiveText(int &sender, string &name, string &text)
{
    return network.ReceiveText(sender, name, text);
}

bool SnakesClient::IsPlayerReady() const
{
    return network.IsPlayerReady();
}

bool SnakesClient::SetPlayerReady()
{
    return network.SendPlayerReady();
}

bool SnakesClient::SetPlayerDir(char dir)
{
    return network.SendPlayerDir(dir);
}

const game::ObjectPool& SnakesClient::Pool() const
{
    return game.Pool();
}

int SnakesClient::SelfPlayerId() const
{
    return network.UserId();
}

const map<int, PlayerInfo>& SnakesClient::PlayersInfo() const
{
    return playerlist;
}

const PlayerInfo* SnakesClient::FindPlayerInfo(int id) const
{
    auto iter = playerlist.find(id);
    return iter != playerlist.end() ? &iter->second : nullptr;
}
