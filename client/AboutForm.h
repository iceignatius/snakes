#ifndef _ABOUT_FORM_H_
#define _ABOUT_FORM_H_

#include "ui/FormBase.h"

class AboutForm : public ui::FormBase
{
private:
    AboutForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

public:
    virtual void Update(unsigned steptime) override;
    virtual void Render() override;
};

#endif
