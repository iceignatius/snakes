#ifndef _SET_STATE_PROCESSOR_H_
#define _SET_STATE_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class SetStateProcessor : public net::CmdProcessorBase
{
private:
    bool player_ready;

public:
    SetStateProcessor(net::CmdPeer *peer);

private:
    void OnResponseMessage(
        const net::CmdMsg &msg,
        const SocketAddr &srcaddr) override;

public:
    bool IsPlayerReady() const;
    bool SendPlayerReadyCommand();
};

#endif
