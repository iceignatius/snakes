#ifndef _CLIENT_NETWORKING_H_
#define _CLIENT_NETWORKING_H_

#include "net/CmdExchangerImpl.h"
#include "game/GameNetInterface.h"
#include "ConnectPeer.h"
#include "PlayerPeer.h"

class ClientNetworking : public ClientNetApi
{
private:
    ClientState state;
    std::string state_msg;

    net::CmdExchangerImpl exgr;
    ConnectPeer *connect_peer;
    PlayerPeer *player_peer;

    struct ConnectInfo
    {
        std::string username;
        std::string password;
        SocketAddr remote;
        int timer;
    } connect_info;

    int userid;
    std::vector<uint8_t> skey;

    struct TalkInfo
    {
        int sender;
        std::string name;
        std::string text;
    };

    std::list<TalkInfo> talklist;

    ClientNetEvents *event_handler;

public:
    ClientNetworking();
    ~ClientNetworking();

public:
    ClientState GetState(std::string *msg = nullptr) const;

    void Connect(
        ClientNetEvents *event_handler,
        const SocketAddr &addr,
        const std::string &username,
        const std::string &password);
    void Disconnect(bool keep_err_state);
    inline void DisconnectKeepErrorState() { Disconnect(true); }
    inline void DisconnectAndClear() { Disconnect(false); }

    void RunStep(unsigned steptime);

private:
    static void EventOnQueryResp(void *self, const ServerInfo &info);
    static void EventOnJoinResp(void *self, int errcode, int userid);
    static void EventOnQuitResp(void *self);
    static void EventOnConnectionError(void *self, int errcode);
    static void EventOnTalkReq(
        void *self,
        int sender,
        const std::string &name,
        const std::string &text);
    static void EventOnUpdateReq(void *self, const void *data, size_t size);

    void OnQueryResp(const ServerInfo &info);
    void OnJoinResp(int errcode, int userid);
    void OnQuitResp();
    void OnConnectionError(int errcode);
    void OnTalkReq(int sender, const std::string &name, const std::string &text);
    void OnUpdateReq(const void *data, size_t size);

public:
    int UserId() const;

    void SendText(const std::string &text);
    bool ReceiveText(int &sender, std::string &name, std::string &text);

    bool IsPlayerReady() const;
    bool SendPlayerReady();
    bool SendPlayerDir(char dir);
};

#endif
