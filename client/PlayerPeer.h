#ifndef _PLAYER_PEER_H_
#define _PLAYER_PEER_H_

#include "net/CmdPeerBase.h"
#include "QuitProcessor.h"
#include "TalkProcessor.h"
#include "SetStateProcessor.h"
#include "SetDirProcessor.h"

class PlayerPeer : public net::CmdPeerBase
{
private:
    void *eventhost;
    void(*OnError)(void *host, int errcode);

    QuitProcessor *quit_proc;
    TalkProcessor *talk_proc;
    SetStateProcessor *state_proc;
    SetDirProcessor *dir_proc;

public:
    PlayerPeer(
        int userid,
        SocketUdp *sock,
        const std::vector<uint8_t> &skey,
        void *eventhost,
        void(*OnError)(void*, int),
        void(*OnQuit)(void*),
        void(*OnTalk)(void*, int, const std::string&, const std::string&),
        void(*OnUpdate)(void*, const void *, size_t));

private:
    void OnConnectionError(int errcode) override;

public:
    bool SendQuitCommand();
    bool SendTalkCommand(const std::string &text);

    bool IsPlayerReady() const;
    bool SendPlayerReadyCommand();
    bool SendPlayerDirCommand(char dir);
};

#endif
