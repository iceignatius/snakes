#ifndef _TALK_PROCESSOR_H_
#define _TALK_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class TalkProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnTalk)(void *host, int sender, const std::string &name, const std::string &text);

public:
    TalkProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnTalk)(void*, int, const std::string&, const std::string&));

private:
    void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;

public:
    bool SendCommand(const std::string &text);
};

#endif
