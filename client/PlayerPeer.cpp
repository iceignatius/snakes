#include "net/AliveProcessor.h"
#include "UpdateProcessor.h"
#include "PlayerPeer.h"

using namespace std;
using namespace net;

PlayerPeer::PlayerPeer(
    int userid,
    SocketUdp *sock,
    const vector<uint8_t> &skey,
    void *eventhost,
    void(*OnError)(void*, int),
    void(*OnQuit)(void*),
    void(*OnTalk)(void*, int, const string&, const string&),
    void(*OnUpdate)(void*, const void *, size_t)) :
        CmdPeerBase(userid, sock, skey)
{
    this->eventhost = eventhost;
    this->OnError = OnError;

    AliveProcessor *alive_proc = new AliveProcessor(this);
    quit_proc = new QuitProcessor(this, eventhost, OnQuit);
    talk_proc = new TalkProcessor(this, eventhost, OnTalk);
    UpdateProcessor *update_proc = new UpdateProcessor(this, eventhost, OnUpdate);
    state_proc = new SetStateProcessor(this);
    dir_proc = new SetDirProcessor(this);

    AddProcessor(alive_proc);
    AddProcessor(quit_proc);
    AddProcessor(talk_proc);
    AddProcessor(update_proc);
    AddProcessor(state_proc);
    AddProcessor(dir_proc);
}

void PlayerPeer::OnConnectionError(int errcode)
{
    OnError(eventhost, errcode);
    CmdPeerBase::OnConnectionError(errcode);
}

bool PlayerPeer::SendQuitCommand()
{
    return quit_proc->SendCommand();
}

bool PlayerPeer::SendTalkCommand(const string &text)
{
    return talk_proc->SendCommand(text);
}

bool PlayerPeer::IsPlayerReady() const
{
    return state_proc->IsPlayerReady();
}

bool PlayerPeer::SendPlayerReadyCommand()
{
    return state_proc->SendPlayerReadyCommand();
}

bool PlayerPeer::SendPlayerDirCommand(char dir)
{
    return dir_proc->SendPlayerDirCommand(dir);
}
