#ifndef _SNAKES_CLIENT_H_
#define _SNAKES_CLIENT_H_

#include "game/SnakesGameClient.h"
#include "ClientNetworking.h"

struct PlayerInfo
{
    int id;
    std::string name;
    int index;
    int rank;
    int len;
    int kills;
    bool alive;
    bool is_ready;
};

class SnakesClient
{
private:
    SnakesGameClient game;
    ClientNetworking network;

    std::map<int, PlayerInfo> playerlist;

public:
    ~SnakesClient();

public:
    ClientState GetState(std::string *msg = nullptr) const;

    void Connect(
        const SocketAddr &addr,
        const std::string &username,
        const std::string &password);
    void Disconnect();

private:
    void UpdatePlayerList();

public:
    void RunStep(unsigned steptime);

public:
    void SendText(const std::string &text);
    bool ReceiveText(int &sender, std::string &name, std::string &text);

    bool IsPlayerReady() const;
    bool SetPlayerReady();
    bool SetPlayerDir(char dir);

    const game::ObjectPool& Pool() const;

    int SelfPlayerId() const;
    const std::map<int, PlayerInfo>& PlayersInfo() const;
    const PlayerInfo* FindPlayerInfo(int id) const;
};

#endif
