#include <assert.h>

#ifndef _WIN32
#   include <sys/wait.h>
#endif

#include "common/ascii.h"
#include "common/pathutl.h"
#include "net/netdef.h"
#include "ui/FormUtils.h"
#include "game/GameScene.h"
#include "FormKlassName.h"
#include "ServerSetupForm.h"

using namespace std;
using namespace ui;

ServerSetupForm::ServerSetupForm(FormManager *mgr) :
    FormBase(mgr),
    port(0),
    curr_sel(0),
#ifdef _WIN32
    server_procinfo(nullptr)
#else
    server_pid(-1)
#endif
{
}

const char* ServerSetupForm::KlassName()
{
    return FORMKLASS_SERVER_SETUP;
}

Form* ServerSetupForm::CreateInstance(FormManager *mgr)
{
    return new ServerSetupForm(mgr);
}

void ServerSetupForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    if( keystate[Console::CtrlKeyWith('E')] )
        Close();

    if( keystate[Console::CtrlKeyWith('N')] )
    {
        util::InputString(
            Manager(),
            this,
            "Set game name",
            "Please input\nthe game name",
            gamename);
    }

    if( keystate[Console::CtrlKeyWith('D')] )
    {
        util::InputString(
            Manager(),
            this,
            "Set game password",
            "Please input\nthe game password",
            password);
    }

    if( keystate[Console::CtrlKeyWith('P')] )
    {
        string portstr;
        util::InputString(
            Manager(),
            this,
            "Set server port",
            "Please input\nthe server port number",
            portstr);
        port = stoul(portstr);
    }

    if( keystate[KEY_UP] && curr_sel >= 1 )
        --curr_sel;

    if( keystate[KEY_DOWN] && curr_sel + 1 < scenelist.size() )
        ++curr_sel;

    if( keystate[ASCII_CR] )
    {
        if( ExecuteServerProcess() )
        {
            PushPlayForm();
            Suspend();
        }
    }
}

void ServerSetupForm::Render()
{
    int scrwid = getmaxx(stdscr);
    int scrhei = getmaxy(stdscr);

    mvwprintw(stdscr, 0, 0, "Server port: ");
    attron(A_REVERSE);
    wprintw(stdscr, "%u", port ? port : NETDEF_DEFAULT_SERVER_PORT);
    attroff(A_REVERSE);

    mvwprintw(stdscr, 1, 0, "Game name: ");
    attron(A_REVERSE);
    wprintw(stdscr, "%s", gamename.c_str());
    attroff(A_REVERSE);

    mvwprintw(stdscr, 2, 0, "Password: ");
    attron(A_REVERSE);
    wprintw(stdscr, "%s", password.c_str());
    attroff(A_REVERSE);

    mvwprintw(stdscr, 4, 0, "Scenes: %zu", scenelist.size());
    mvwprintw(stdscr, 5, 0, "%-4s%-32s%-8s", "", "Scene", "Players");

    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, 6, x, '-');
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 2, x, '-');

    unsigned list_rows = scrhei - 7 - 2;
    unsigned begin_index =
        curr_sel > list_rows / 2 ?
        curr_sel - list_rows / 2 :
        0;
    unsigned end_index =
        begin_index + list_rows < scenelist.size() ?
        begin_index + list_rows :
        scenelist.size();

    int item_row = 7;
    for(unsigned i = begin_index; i != end_index; ++i)
    {
        const SceneInfo &info = scenelist[i];

        if( i == curr_sel )
            attron(A_REVERSE);

        mvwprintw(
            stdscr,
            item_row++,
            0,
            "%-4u%-32s%8u\n",
            i,
            info.scenename.c_str(),
            info.players);

        if( i == curr_sel )
            attroff(A_REVERSE);
    }

    attron(A_REVERSE);
    for(int x = 0; x < scrwid; ++x)
        mvwaddch(stdscr, scrhei - 1, x, ' ');
    mvwprintw(
        stdscr,
        scrhei - 1,
        0,
        " ENTER: start | Ctrl+N: name | Ctrl+D: password | Ctrl+P: port | Ctrl+E: exit");
    attroff(A_REVERSE);
}

void ServerSetupForm::OnPush(const FormParams &params)
{
    username = params.GetStringParam("username", "");
    RescanScenes();
}

void ServerSetupForm::OnPop(FormParams &params)
{
    TerminateServerProcess();
}

void ServerSetupForm::OnFocus(const FormParams &params)
{
    TerminateServerProcess();
}

void ServerSetupForm::RescanScenes()
{
    curr_sel = 0;
    scenelist.clear();

    // Add the default blank scene info.
    {
        game::Scene scene;
        scene.LoadDefaultBlankMap();

        SceneInfo info;
        info.scenename = scene.Name();
        info.players = scene.MaxPlayers();

        scenelist.push_back(info);
    }

    // List all files in the target directories.
    list<string> filelist;
    filelist.splice(
        filelist.end(),
        pathutl::ListDirFiles("/usr/share/snakes/scenes"));
    filelist.splice(
        filelist.end(),
        pathutl::ListDirFiles("/usr/local/share/snakes/scenes"));
    filelist.splice(
        filelist.end(),
        pathutl::ListDirFiles(pathutl::GetHomeDir() + "/.config/snakes/scenes"));
#ifdef _WIN32
    filelist.splice(
        filelist.end(),
        pathutl::ListDirFiles(pathutl::ExtractFileDir(pathutl::GetExecutableFileName())));
#endif

    // Test files, extract and add scene information.
    for(string &filename : filelist)
    {
        if( pathutl::ExtractFileExt(filename) != ".snksc" )
            continue;

        game::Scene scene;
        if( !scene.LoadFile(filename) )
            continue;

        SceneInfo info;
        info.filename = filename;
        info.scenename = scene.Name();
        info.players = scene.MaxPlayers();

        scenelist.push_back(info);
    }
}

void ServerSetupForm::PushPlayForm()
{
    FormParams params;

    SocketAddr addr(ipv4_const_loop, port ? port : NETDEF_DEFAULT_SERVER_PORT);
    params.InsertNetAddrParam("addr", addr);

    params.InsertStringParam("username", username);

    if( !password.empty() )
        params.InsertStringParam("password", password);

    Manager()->PushNewForm(FORMKLASS_PLAY, params);
}

bool ServerSetupForm::ExecuteServerProcess()
{
    TerminateServerProcess();

    try
    {
#ifdef _WIN32
        // Prepare server startup arguments.

        string command =
            pathutl::ExtractFileDir(pathutl::GetExecutableFileName()) + "/snakesd";

        if( port > 0 )
            command += " --port=" + to_string(port);

        if( !gamename.empty() )
            command += " --name=" + gamename;

        if( !password.empty() )
            command += " --password=" + password;

        string scenefile = scenelist[curr_sel].filename;
        if( !scenefile.empty() )
            command += " --scene=" + scenefile;

        // Execute server process.

        server_procinfo = new PROCESS_INFORMATION;
        server_procinfo->hProcess = INVALID_HANDLE_VALUE;
        server_procinfo->hThread = INVALID_HANDLE_VALUE;

        STARTUPINFO startinfo = { .cb = sizeof(STARTUPINFO) };
        bool succ = CreateProcess(
            nullptr,
            const_cast<char*>(command.c_str()),
            nullptr,
            nullptr,
            false,
            CREATE_NEW_PROCESS_GROUP,
            nullptr,
            nullptr,
            &startinfo,
            server_procinfo);
        if( !succ )
            throw runtime_error("Failed to execute server process!");
#else
        // Prepare server startup arguments.

        static const int argc_max = 8;
        const char *argv[argc_max];
        int argc = 0;

        string argstr_exefile =
            pathutl::ExtractFileDir(pathutl::GetExecutableFileName()) + "/snakesd";
        if( !argstr_exefile.empty() )
            argv[argc++] = argstr_exefile.c_str();

        string argstr_port =
            port > 0 ? argstr_port = "--port=" + to_string(port) : "";
        if( !argstr_port.empty() )
            argv[argc++] = argstr_port.c_str();

        string argstr_name =
            !gamename.empty() ? "--name=" + gamename : "";
        if( !argstr_name.empty() )
            argv[argc++] = argstr_name.c_str();

        string argstr_password =
            !password.empty() ? "--password=" + password : "";
        if( !argstr_password.empty() )
            argv[argc++] = argstr_password.c_str();

        string scenefile = scenelist[curr_sel].filename;
        string argstr_scene =
            !scenefile.empty() ? "--scene=" + scenefile : "";
        if( !argstr_scene.empty() )
            argv[argc++] = argstr_scene.c_str();

        argv[argc++] = nullptr;
        assert( argc <= argc_max );

        // Fork process.

        assert( server_pid < 0 );
        server_pid = fork();
        if( server_pid < 0 )
            throw runtime_error("Failed to fork process!");
        if( server_pid > 0 )
            return true;

        // Close files.

        fclose(stdout);
        fclose(stderr);

        // Execute server process.

        if( 0 > execv(argv[0], (char* const*) argv) )
            exit(1);
#endif

        return true;
    }
    catch(exception &e)
    {
        TerminateServerProcess();
        util::ShowMessage(Manager(), e.what(), 5000);
    }

    return false;
}

void ServerSetupForm::TerminateServerProcess()
{
#ifdef _WIN32
    if( !server_procinfo ) return;

    bool succ =
        server_procinfo->hProcess != INVALID_HANDLE_VALUE ?
        GenerateConsoleCtrlEvent(CTRL_BREAK_EVENT, server_procinfo->dwProcessId) :
        false;

    if( succ )
        WaitForSingleObject(server_procinfo->hProcess, INFINITE);

    if( server_procinfo->hThread != INVALID_HANDLE_VALUE )
        CloseHandle(server_procinfo->hThread);
    if( server_procinfo->hProcess != INVALID_HANDLE_VALUE )
        CloseHandle(server_procinfo->hProcess);

    delete server_procinfo;
    server_procinfo = nullptr;
#else
    if( server_pid <= 0 ) return;

    if( 0 == kill(server_pid, SIGQUIT) )
        waitpid(server_pid, nullptr, 0);

    server_pid = -1;
#endif
}
