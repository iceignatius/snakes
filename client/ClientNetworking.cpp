#include "net/netdef.h"
#include "net/cmdcrypt.h"
#include "ClientNetworking.h"

using namespace std;

ClientNetworking::ClientNetworking() :
    state(ClientState::Disconnected),
    state_msg("Disconnected"),
    connect_peer(nullptr),
    player_peer(nullptr),
    userid(CMDSENDER_ANON),
    event_handler(nullptr)
{
}

ClientNetworking::~ClientNetworking()
{
    DisconnectAndClear();
}

ClientState ClientNetworking::GetState(string *msg) const
{
    if( msg )
        *msg = state_msg;

    return state;
}

void ClientNetworking::Connect(
    ClientNetEvents *event_handler,
    const SocketAddr &addr,
    const string &username,
    const string &password)
{
    DisconnectAndClear();

    try
    {
        this->event_handler = event_handler;

        CmdCrypt::GenerateSKey(skey);

        exgr.Open(0);

        connect_peer = new ConnectPeer(
            exgr.GetSocket(),
            this,
            EventOnQueryResp,
            EventOnJoinResp);
        exgr.AddPeer(connect_peer);

        connect_peer->Active(addr);

        if( !connect_peer->SendQueryCommand() )
            throw runtime_error("Send query command failed!");

        state = ClientState::Querying;
        state_msg = "Connecting...";

        connect_info.username = username;
        connect_info.password = password;
        connect_info.timer = NETDEF_CONNECT_TIMEOUT;
    }
    catch(exception &e)
    {
        DisconnectKeepErrorState();
        throw;
    }
}

void ClientNetworking::Disconnect(bool keep_err_state)
{
    if( player_peer )
        player_peer->SendQuitCommand();

    exgr.Close();
    exgr.RemoveAllPeers();
    player_peer = nullptr;
    connect_peer = nullptr;

    userid = CMDSENDER_ANON;
    skey.clear();

    connect_info.remote = SocketAddr();
    connect_info.password = "";
    connect_info.username = "";
    connect_info.timer = 0;

    talklist.clear();

    if( !keep_err_state ||
        ( state != ClientState::ConnectFailed &&
            state != ClientState::ConnectionError ) )
    {
        state = ClientState::Disconnected;
        state_msg = "Disconnected";
    }

    event_handler = nullptr;
}

void ClientNetworking::RunStep(unsigned steptime)
{
    while( exgr.ReceiveDispatch() )
    {}

    exgr.RunStep(steptime);

    switch( state )
    {
    case ClientState::Disconnected:
    case ClientState::ConnectFailed:
    case ClientState::ConnectionError:
        break;

    case ClientState::Querying:
    case ClientState::Joining:
        connect_info.timer -= steptime;
        if( connect_info.timer <= 0 )
        {
            state = ClientState::ConnectFailed;
            state_msg = "Connection timeout!";
            DisconnectKeepErrorState();
        }
        break;

    case ClientState::Connected:
        if( connect_peer )
        {
            exgr.RemovePeer(CMDSENDER_ANON);
            connect_peer = nullptr;
        }
        break;
    }
}

void ClientNetworking::EventOnQueryResp(void *self, const ServerInfo &info)
{
    ((ClientNetworking*)self)->OnQueryResp(info);
}

void ClientNetworking::EventOnJoinResp(void *self, int errcode, int userid)
{
    ((ClientNetworking*)self)->OnJoinResp(errcode, userid);
}

void ClientNetworking::EventOnQuitResp(void *self)
{
    ((ClientNetworking*)self)->OnQuitResp();
}

void ClientNetworking::EventOnConnectionError(void *self, int errcode)
{
    ((ClientNetworking*)self)->OnConnectionError(errcode);
}

void ClientNetworking::EventOnTalkReq(
    void *self,
    int sender,
    const string &name,
    const string &text)
{
    ((ClientNetworking*)self)->OnTalkReq(sender, name, text);
}

void ClientNetworking::EventOnUpdateReq(void *self, const void *data, size_t size)
{
    ((ClientNetworking*)self)->OnUpdateReq(data, size);
}

void ClientNetworking::OnQueryResp(const ServerInfo &info)
{
    connect_info.remote = info.addr;

    if( !connect_peer->SendJoinCommand(
        connect_info.username,
        connect_info.password,
        info.pubkey,
        skey) )
    {
        throw runtime_error("Send join command failed!");
    }

    state = ClientState::Joining;
    state_msg = "Joining...";
}

void ClientNetworking::OnJoinResp(int errcode, int userid)
{
    if( errcode )
    {
        state = ClientState::ConnectFailed;
        state_msg = cmdrc_to_str(errcode);
        DisconnectKeepErrorState();
        return;
    }

    this->userid = userid;

    player_peer = new PlayerPeer(
        userid,
        exgr.GetSocket(),
        skey,
        this,
        EventOnConnectionError,
        EventOnQuitResp,
        EventOnTalkReq,
        EventOnUpdateReq);
    exgr.AddPeer(player_peer);

    player_peer->Active(connect_info.remote);

    state = ClientState::Connected;
    state_msg = "Connected";
}

void ClientNetworking::OnQuitResp()
{
    // Nothing to do.
}

void ClientNetworking::OnConnectionError(int errcode)
{
    state = ClientState::ConnectionError;
    state_msg = "Connection error!";
    DisconnectKeepErrorState();
}

void ClientNetworking::OnTalkReq(int sender, const string &name, const string &text)
{
    TalkInfo info;
    info.sender = sender;
    info.name = name;
    info.text = text;

    talklist.push_back(info);
}

void ClientNetworking::OnUpdateReq(const void *data, size_t size)
{
    if( event_handler )
        event_handler->OnUpdateObject(data, size);
}

int ClientNetworking::UserId() const
{
    return player_peer ? player_peer->UserId() : -1;
}

void ClientNetworking::SendText(const string &text)
{
    if( player_peer )
        player_peer->SendTalkCommand(text);
}

bool ClientNetworking::ReceiveText(int &sender, string &name, string &text)
{
    if( talklist.empty() ) return false;

    TalkInfo info = talklist.front();
    talklist.pop_front();

    sender = info.sender;
    name = info.name;
    text = info.text;

    return true;
}

bool ClientNetworking::IsPlayerReady() const
{
    return player_peer && player_peer->IsPlayerReady();
}

bool ClientNetworking::SendPlayerReady()
{
    return player_peer && player_peer->SendPlayerReadyCommand();
}

bool ClientNetworking::SendPlayerDir(char dir)
{
    return player_peer && player_peer->SendPlayerDirCommand(dir);
}
