#include "game/GameNetInterface.h"
#include "SetStateProcessor.h"

using namespace std;
using namespace net;

SetStateProcessor::SetStateProcessor(CmdPeer *peer) :
    CmdProcessorBase(
        CMDID_SETSTATE,
        CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
        peer),
    player_ready(false)
{
}

void SetStateProcessor::OnResponseMessage(
    const CmdMsg &msg,
    const SocketAddr &srcaddr)
{
    if( msg.GetErrorCode() )
        return;

    int state = msg.GetIntegerArg("playstate", (int) GameState::PREPARING);
    player_ready = state > (int) GameState::PREPARING;
}

bool SetStateProcessor::IsPlayerReady() const
{
    return player_ready;
}

bool SetStateProcessor::SendPlayerReadyCommand()
{
    CmdMsg msg(CmdId());

    msg.AddIntegerArg("playstate", (int) GameState::PLAYING);

    return SendRequestMessage(msg);
}
