#ifndef _ALIVE_PROCESSOR_H_
#define _ALIVE_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

namespace net
{

class AliveProcessor : public CmdProcessorBase
{
private:
    CmdPeer *peer;

public:
    AliveProcessor(CmdPeer *peer);

public:
    void RunStep(unsigned steptime) override;
};

}   // namespace net

#endif
