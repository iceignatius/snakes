#ifndef _CMDCRYPT_H_
#define _CMDCRYPT_H_

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#ifdef __cplusplus
#   include <vector>
#   include <string>
#endif

#define CMDCRYPT_BLOCK_SIZE     (128/8)
#define CMDCRYPT_SKEY_SIZE      (128/8)
#define CMDCRYPT_ECC_KEY_SIZE   (224/8)
#define CMDCRYPT_RNG_MODULE     "sprng"
#define CMDCRYPT_HASH_MODULE    "sha256"
#define CMDCRYPT_SYM_MODULE     "aes"

#ifdef __cplusplus
extern "C" {
#endif

bool cmdcrypt_global_init(void);

void cmdcrypt_get_skey(void *key);
bool cmdcrypt_gen_ecckey(
    void /*in*/ *privkey_buf,
    size_t /*in,out*/ *privkey_size,
    void /*in*/ *pubkey_buf,
    size_t /*in,out*/ *pubkey_size);

size_t cmdcrypt_ecc_encrypt(
    void *dstbuf,
    size_t dstsize,
    const void *srcbuf,
    size_t srcsize,
    const void *pubkey,
    size_t keysize);
size_t cmdcrypt_ecc_decrypt(
    void *dstbuf,
    size_t dstsize,
    const void *srcbuf,
    size_t srcsize,
    const void *privkey,
    size_t keysize);

size_t cmdcrypt_ecc_sign_join_res(
    void *dstbuf,
    size_t dstsize,
    int errcode,
    int userid,
    const void *privkey,
    size_t keysize);
bool cmdcrypt_ecc_verify_join_res(
    const void *signdata,
    size_t signsize,
    int errcode,
    int userid,
    const void *pubkey,
    size_t keysize);

size_t cmdcrypt_calc_mac(
    void *dstbuf,
    const void *srcbuf,
    size_t srcsize,
    const void *skey);
size_t cmdcrypt_calc_passphrase(
    void *dstbuf,
    const char *password,
    const void *skey);

#ifdef __cplusplus
}   // extern "C"
#endif

#ifdef __cplusplus

namespace CmdCrypt
{

static inline
bool GlobalInit()
{
    return cmdcrypt_global_init();
}

static inline
void GenerateSKey(std::vector<uint8_t> &key)
{
    key.resize(CMDCRYPT_SKEY_SIZE);
    cmdcrypt_get_skey(key.data());
}

static inline
bool GenerateEccKey(std::vector<uint8_t> &privkey, std::vector<uint8_t> &pubkey)
{
    static const size_t bufsize = CMDCRYPT_ECC_KEY_SIZE * 4;
    privkey.resize(bufsize);
    pubkey.resize(bufsize);

    size_t privkey_size = bufsize, pubkey_size = bufsize;
    if( !cmdcrypt_gen_ecckey(
        privkey.data(),
        &privkey_size,
        pubkey.data(),
        &pubkey_size) )
    {
        return false;
    }

    privkey.resize(privkey_size);
    pubkey.resize(pubkey_size);

    return true;
}

static inline
std::vector<uint8_t> EccEncrypt(
    const std::vector<uint8_t> &src,
    const std::vector<uint8_t> &pubkey)
{
    std::vector<uint8_t> dst(256);
    size_t fillsize = cmdcrypt_ecc_encrypt(
        dst.data(),
        dst.size(),
        src.data(),
        src.size(),
        pubkey.data(),
        pubkey.size());
    dst.resize(fillsize);

    return dst;
}

static inline
std::vector<uint8_t> EccDecrypt(
    const std::vector<uint8_t> &src,
    const std::vector<uint8_t> privkey)
{
    std::vector<uint8_t> dst(256);
    size_t fillsize = cmdcrypt_ecc_decrypt(
        dst.data(),
        dst.size(),
        src.data(),
        src.size(),
        privkey.data(),
        privkey.size());
    dst.resize(fillsize);

    return dst;
}

static inline
std::vector<uint8_t> EccSignJoinRes(
    int errcode,
    int userid,
    const std::vector<uint8_t> &privkey)
{
    std::vector<uint8_t> dst(256);
    size_t fillsize = cmdcrypt_ecc_sign_join_res(
        dst.data(),
        dst.size(),
        errcode,
        userid,
        privkey.data(),
        privkey.size());
    dst.resize(fillsize);

    return dst;
}

static inline
bool EccVerifyJoinRes(
    const std::vector<uint8_t> &signature,
    int errcode,
    int userid,
    const std::vector<uint8_t> &pubkey)
{
    return cmdcrypt_ecc_verify_join_res(
        signature.data(),
        signature.size(),
        errcode,
        userid,
        pubkey.data(),
        pubkey.size());
}

static inline
std::vector<uint8_t> CalculateMac(
    const std::vector<uint8_t> &src,
    const std::vector<uint8_t> &skey)
{
    std::vector<uint8_t> dst(CMDCRYPT_BLOCK_SIZE);
    size_t fillsize =
        cmdcrypt_calc_mac(dst.data(), src.data(), src.size(), skey.data());
    dst.resize(fillsize);

    return dst;
}

static inline
std::vector<uint8_t> CalculatePassphrase(
    const std::string &password,
    const std::vector<uint8_t> &skey)
{
    std::vector<uint8_t> dst(CMDCRYPT_BLOCK_SIZE);
    size_t fillsize =
        cmdcrypt_calc_passphrase(dst.data(), password.c_str(), skey.data());
    dst.resize(fillsize);

    return dst;
}

}   // namespace CmdCrypt

#endif  // ifdef __cplusplus

#endif
