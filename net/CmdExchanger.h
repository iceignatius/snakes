#ifndef _CMD_EXCHANGER_H_
#define _CMD_EXCHANGER_H_

#include <netsock/sockudp.h>
#include "CmdPeer.h"

namespace net
{

class CmdExchanger
{
public:
    virtual ~CmdExchanger() {}

public:
    virtual SocketUdp* GetSocket() = 0;
    virtual void AddPeer(CmdPeer *peer) = 0;
    virtual void RemovePeer(int userid) = 0;
    virtual void RemoveAllPeers() = 0;

    virtual unsigned ReceiveDispatch() = 0;
    virtual void RunStep(unsigned steptime) = 0;
};

}   // namespace net

#endif
