# Snakes

It is a variant of the classic snake game
with multi players running on console terminal.

## Software Dependency

* The [netsock](https://gitlab.com/iceignatius/netsock) library.
* The [ndobj](https://gitlab.com/iceignatius/ndobj) library.
* The [NCurses](https://invisible-island.net/ncurses/announce.html) library.
* The [LibTomCrypt](https://www.libtom.net/LibTomCrypt/) library.
