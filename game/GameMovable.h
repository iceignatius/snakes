#ifndef _GAME_MOVABLE_H_
#define _GAME_MOVABLE_H_

#include "GameEntity.h"
#include "GameFactory.h"
#include "GameTimer.h"

namespace game
{

class Movable : public Entity
{
private:
    typedef Entity Super;

private:
    char currdir;
    char lastdir;

    Timer movetimer;
    bool just_moved;

    int last_x;
    int last_y;

public:
    Movable(int id, int owner, unsigned move_dura);

public:
    void Update(unsigned steptime) override;

    size_t ExportRaw(void *buf, size_t size) const override;
    size_t ImportRaw(const void *raw, size_t size) override;

private:
    bool MoveOneGrid();

public:
    bool SetDir(int dir);
    void SetMoveDuration(unsigned dura);

    bool JustMoved() const { return just_moved; }
    int LastPosX() const { return last_x; }
    int LastPosY() const { return last_y; }
};

}   // namespace game

#endif
