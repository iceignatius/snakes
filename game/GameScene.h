#ifndef _GAME_SCENE_H_
#define _GAME_SCENE_H_

#include <string>
#include <vector>
#include <map>
#include "GameObjectType.h"

namespace game
{

class Scene
{
public:
    struct ObjectInfo
    {
        int x;
        int y;
        ObjectType type;
    };

private:
    struct PlayerStartInfo
    {
        int x;
        int y;
    };

    std::string scene_name;
    std::vector<PlayerStartInfo> start_list;
    std::vector<ObjectInfo> objinfo_list;

private:
    static void PushPlayerInfo(
        int x,
        int y,
        int index,
        std::map<int, PlayerStartInfo> &start_map);
    static void PushObjectInfo(
        int x,
        int y,
        int ch,
        std::vector<ObjectInfo> &objinfo_list);
    static void ParseLineSymbols(
        const std::string &line,
        int row,
        std::map<int, PlayerStartInfo> &start_map,
        std::vector<ObjectInfo> &objinfo_list);
    static void ParseFileSymbols(
        const std::string &filename,
        std::map<int, PlayerStartInfo> &start_map,
        std::vector<ObjectInfo> &objinfo_list);
    static void VerifyObjectList(const std::vector<ObjectInfo> &objinfo_list);
    static std::vector<PlayerStartInfo> ConvertPlayerStartInfoList(
        const std::map<int, PlayerStartInfo> &start_map);

public:
    void Clear();
    void LoadDefaultBlankMap();
    bool LoadFile(const std::string &filename);

    const std::string& Name() const { return scene_name; }
    int MaxPlayers() const;
    bool GetPlayerStartPos(int index, int &x, int &y);
    const std::vector<ObjectInfo>& GetObjectInfoList() const { return objinfo_list; }
};

}   // namespace game

#endif
