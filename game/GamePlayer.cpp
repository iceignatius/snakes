#include <assert.h>
#include <string.h>
#include <vector>
#include <endian.h>
#include "GameMovable.h"
#include "GamePlayer.h"

using namespace std;
using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    uint8_t player_index;
    uint8_t player_ready;
    uint16_t len;
    uint8_t killcount;
    uint8_t rank;
    int32_t snake_head;
    char name[];
};
#pragma pack(pop)

bool Player::player_index_pool[def::MAX_PLAYER_NUM+1] = {true}; // Index 0 is reserved for the natural player!

Player::Player(int id, ObjectPool *pool) :
    Super(id),
    pool(pool),
    player_index(-1),
    player_ready(false),
    len(0),
    killcount(0),
    rank(0),
    snake_head(-1)
{
}

Player::~Player()
{
    ReleasePlayerIndex();
}

Object* Player::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new Player(id, pool);
}

void Player::Update(unsigned steptime)
{
    Super::Update(steptime);

    if( snake_head >= 0 && !pool->GetAlive(snake_head) )
        snake_head = -1;
}

size_t Player::ExportRaw(void *buf, size_t size) const
{
    size_t super_encsize = Super::ExportRaw(nullptr, 0);
    size_t total_encsize = super_encsize + sizeof(RawInfo) + name.length();
    assert(super_encsize);

    if( !buf )
        return total_encsize;
    if( size < total_encsize )
        return 0;
    if( Super::ExportRaw(buf, super_encsize) != super_encsize )
        return 0;

    RawInfo *pkg = (RawInfo*)( (uint8_t*)buf + super_encsize );
    pkg->player_index = player_index;
    pkg->player_ready = player_ready;
    pkg->len = htobe16(len);
    pkg->killcount = killcount;
    pkg->rank = rank;
    pkg->snake_head = htobe32(snake_head);
    memcpy(pkg->name, name.c_str(), name.length());

    return total_encsize;
}

size_t Player::ImportRaw(const void *raw, size_t size)
{
    size_t min_super_decsize = Super::ImportRaw(nullptr, 0);
    size_t min_total_decsize = min_super_decsize + sizeof(RawInfo);
    assert(min_super_decsize);

    if( !raw )
        return min_total_decsize;
    if( size < min_total_decsize )
        return 0;

    size_t super_decsize = Super::ImportRaw(raw, size);
    if( !super_decsize )
        return 0;

    assert( super_decsize + sizeof(RawInfo) <= size );
    const RawInfo *pkg = (const RawInfo*)( (const uint8_t*)raw + super_decsize );

    player_index = (int8_t) pkg->player_index;
    player_ready = pkg->player_ready;
    len = be16toh(pkg->len);
    killcount = pkg->killcount;
    rank = pkg->rank;
    snake_head = (int32_t) be32toh(pkg->snake_head);

    size_t namelen = size - super_decsize - sizeof(RawInfo);
    vector<char> namebuf(namelen+1);
    memcpy(namebuf.data(), pkg->name, namelen);
    namebuf[namelen] = 0;

    name = namebuf.data();

    return size;
}

int Player::AcquirePlayerIndex()
{
    for(int i = 0; i <= def::MAX_PLAYER_NUM && player_index < 0; ++i)
    {
        if( player_index_pool[i] ) continue;

        player_index = i;
        player_index_pool[player_index] = true;
    }

    return player_index;
}

void Player::ReleasePlayerIndex()
{
    if( 0 <= player_index && player_index <= def::MAX_PLAYER_NUM )
        player_index_pool[player_index] = false;

    player_index = -1;
}

void Player::SetReady(bool ready)
{
    player_ready = ready;
    MarkSyncAsap();
}

void Player::SetLength(unsigned len)
{
    unsigned old_len = this->len;
    this->len = len;

    if( len != old_len )
        MarkSyncAsap();
}

void Player::IncreaseKillCount()
{
    ++killcount;
    MarkSyncAsap();
}

void Player::ClearKillCount()
{
    killcount = 0;
    MarkSyncAsap();
}

void Player::SetRank(unsigned rank)
{
    unsigned old_rank = this->rank;
    this->rank = rank;

    if( rank != old_rank )
        MarkSyncAsap();
}

void Player::AssignSnake(int head)
{
    snake_head = head;
    MarkSyncAsap();
}

bool Player::IsSnakeAlive() const
{
    if( snake_head < 0 ) return false;

    Movable *head = dynamic_cast<Movable*>(pool->GetAlive(snake_head));
    return head;
}

void Player::SetSnakeDir(char dir)
{
    Movable *head = dynamic_cast<Movable*>(pool->GetAlive(snake_head));
    if( head )
        head->SetDir(dir);
}
