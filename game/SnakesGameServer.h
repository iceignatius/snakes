#ifndef _SNAKES_GAME_SERVER_H_
#define _SNAKES_GAME_SERVER_H_

#include "common/tmutl.h"
#include "GameNetInterface.h"
#include "GameScene.h"
#include "GamePlayer.h"
#include "GameFactory.h"
#include "GameState.h"

class SnakesGameServer : public ServerNetEvents
{
private:
    ServerNetApi *network;

    game::Scene scene;
    game::ObjectPool pool;
    game::ObjectFactory factory;
    tmutl::Timer sync_timer;

    int max_players_num;
    int curr_players_num;
    game::PlayState *playstate;

private:
    void RegisterObjectTypes();

public:
    SnakesGameServer();
    ~SnakesGameServer();

public:
    void Open(ServerNetApi *network, const std::string &scenefile);
    void Close();

private:
    void UpdateObjects(unsigned steptime);
    std::vector<uint8_t> ExportObject(const game::Syncable *syncable);
    void DoSyncObjects();
    void CheckSyncObjects();
    void MarkSyncAllObjects();
    int CountReadyPlayers();
    int CountSnakeAlivePlayers();
    void UnreadyPlayers();
    void ProcStateWorks();
    void KillGameplayObjects();
    void BuildPlayerInitialSnake(game::Player *player);
    void RebuildGameObjects();
    void KillOutOfMapEntities();
    void ProcCollisionReaction();

public:
    void RunStep(unsigned steptime);

private:
    QueryInfo OnQuery() override;
    int OnJoin(int &userid, const std::string &name) override;
    void OnQuit(int userid) override;
    void OnConnError(int userid, int errcode) override;
    void OnTalk(int userid, const std::string &text) override;
    void OnSetState(int userid, GameState state) override;
    void OnSetDir(int userid, char dir) override;

public:
    inline QueryInfo QueryStatus() { return OnQuery(); }
};

#endif
