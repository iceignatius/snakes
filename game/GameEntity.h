#ifndef _GAME_ENTITY_H_
#define _GAME_ENTITY_H_

#include "GameSyncable.h"

namespace game
{

class Entity : public Syncable
{
private:
    typedef Syncable Super;

private:
    int owner;
    int x;
    int y;

public:
    Entity(int id, int owner);

public:
    size_t ExportRaw(void *buf, size_t size) const;
    size_t ImportRaw(const void *raw, size_t size);

public:
    int Owner() const { return owner; }

    virtual void SetPos(int x, int y);
    int PosX() const { return x; }
    int PosY() const { return y; }

    virtual char Graph() const = 0;
    virtual int Colour() const = 0; // Return -1 to show the owner's colour set.

    virtual void OnCollision(Entity *another) {}
};

}   // namespace game

#endif
