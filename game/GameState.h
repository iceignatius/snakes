#ifndef _GAME_STATE_H_
#define _GAME_STATE_H_

#include "GameNetInterface.h"
#include "GameSyncable.h"
#include "GameFactory.h"

namespace game
{

class PlayState : public Syncable
{
private:
    typedef Syncable Super;

private:
    ObjectPool *pool;
    bool server_mode;

    GameState state;
    unsigned starting_remain;
    tmutl::Timer starting_timer;

public:
    PlayState(int id, ObjectPool *pool, bool server_mode);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::PLAYSTATE; }

    void Update(unsigned steptime) final;

    size_t ExportRaw(void *buf, size_t size) const final;
    size_t ImportRaw(const void *raw, size_t size) final;

public:
    bool IsOnServer() const;
    void SetState(GameState state);
    GameState GetState() const;
    unsigned GetStartRemainTime() const;
};

}   // namespace game

#endif
