#ifndef _GAME_PLAYER_H_
#define _GAME_PLAYER_H_

#include <string>
#include "GameDef.h"
#include "GameSyncable.h"
#include "GameFactory.h"

namespace game
{

class Player : public Syncable
{
private:
    typedef Syncable Super;

private:
    ObjectPool *pool;

    static bool player_index_pool[def::MAX_PLAYER_NUM+1];
    int player_index;

    std::string name;
    bool player_ready;
    unsigned len;
    unsigned killcount;
    unsigned rank;

    int snake_head;

public:
    Player(int id, ObjectPool *pool);
    ~Player();

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::PLAYER; }

    void Update(unsigned steptime) final;

    size_t ExportRaw(void *buf, size_t size) const final;
    size_t ImportRaw(const void *raw, size_t size) final;

public:
    const std::string& Name() const { return name; }
    void SetName(const std::string &name) { this->name = name; }

    int PlayerIndex() const { return player_index; }
    int AcquirePlayerIndex();
    void ReleasePlayerIndex();

    bool IsReady() const { return player_ready; }
    void SetReady(bool ready);

    unsigned Length() const { return len; }
    void SetLength(unsigned len);
    unsigned KillCount() const { return killcount; }
    void IncreaseKillCount();
    void ClearKillCount();
    unsigned Rank() const { return rank; }
    void SetRank(unsigned rank);

    void AssignSnake(int head);
    bool IsSnakeAlive() const;
    void SetSnakeDir(char dir);
};

}   // namespace game;

#endif
