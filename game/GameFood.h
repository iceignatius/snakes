#ifndef _GAME_FOOD_H_
#define _GAME_FOOD_H_

#include "GameEntity.h"
#include "GameFactory.h"

namespace game
{

class Food : public Entity
{
private:
    typedef Entity Super;

public:
    Food(int id, int owner);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::FOOD; }

    char Graph() const final;
    int Colour() const final;

    void OnCollision(Entity *another) final;
};

}   // namespace game

#endif
