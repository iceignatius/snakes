#ifndef _SNAKES_GAME_CLIENT_H_
#define _SNAKES_GAME_CLIENT_H_

#include "GameNetInterface.h"
#include "GameFactory.h"

class SnakesGameClient : public ClientNetEvents
{
public:
    ClientNetApi *network;

    game::ObjectPool pool;
    game::ObjectFactory factory;

private:
    void RegisterObjectTypes();

public:
    SnakesGameClient();
    ~SnakesGameClient();

public:
    void Open(ClientNetApi *network);
    void Close();

private:
    GameState GetGameState() const;

public:
    void RunStep(unsigned steptime);

    const game::ObjectPool& Pool() const { return pool; }

private:
    void OnUpdateObject(const void *data, size_t size) override;
};

#endif
