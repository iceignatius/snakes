#ifndef _GAME_SNAKE_DECOMPOSER_H_
#define _GAME_SNAKE_DECOMPOSER_H_

#include "GameFactory.h"

namespace game
{

class SnakeDecomposer : public Object
{
private:
    typedef Object Super;

private:
    ObjectPool *pool;
    ObjectFactory *factory;

public:
    SnakeDecomposer(int id, ObjectPool *pool, ObjectFactory *factory);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::FOODGEN; }

    void Update(unsigned steptime) final;
};

}   // namespace game

#endif
