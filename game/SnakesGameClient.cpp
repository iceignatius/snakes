#include <endian.h>
#include "GameState.h"
#include "GamePlayer.h"
#include "GameSnakeHead.h"
#include "GameSnakeBody.h"
#include "GameFood.h"
#include "GameWall.h"
#include "GameSlip.h"
#include "GameTunnel.h"
#include "SnakesGameClient.h"

void SnakesGameClient::RegisterObjectTypes()
{
    factory.RegisterType(
        game::ObjectType::PLAYSTATE,
        game::ObjectPool::Region::PLAYSTATE,
        game::PlayState::CreateInstance);
    factory.RegisterType(
        game::ObjectType::PLAYER,
        game::ObjectPool::Region::PLAYER,
        game::Player::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SNAKEHEAD,
        game::ObjectPool::Region::ENTITY,
        game::SnakeHead::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SNAKEBODY,
        game::ObjectPool::Region::ENTITY,
        game::SnakeBody::CreateInstance);
    factory.RegisterType(
        game::ObjectType::FOOD,
        game::ObjectPool::Region::ENTITY,
        game::Food::CreateInstance);
    factory.RegisterType(
        game::ObjectType::WALL,
        game::ObjectPool::Region::ENTITY,
        game::Wall::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SLIP,
        game::ObjectPool::Region::ENTITY,
        game::Slip::CreateInstance);
    factory.RegisterType(
        game::ObjectType::TUNNEL,
        game::ObjectPool::Region::ENTITY,
        game::Tunnel::CreateInstance);
}

SnakesGameClient::SnakesGameClient() :
    network(nullptr)
{
    RegisterObjectTypes();
}

SnakesGameClient::~SnakesGameClient()
{
    Close();
}

void SnakesGameClient::Open(ClientNetApi *network)
{
    Close();

    this->network = network;
}

void SnakesGameClient::Close()
{
    pool.EraseAll();

    network = nullptr;
}

GameState SnakesGameClient::GetGameState() const
{
    const game::PlayState *playstate = dynamic_cast<const game::PlayState*>(
        pool.Get(game::def::PLAYSTATE_ID));

    return playstate ? playstate->GetState() : GameState::PREPARING;
}

void SnakesGameClient::RunStep(unsigned steptime)
{
    GameState state = GetGameState();

    for(game::Object &obj : pool)
        obj.Update( state == GameState::PLAYING ? steptime : 0 );

    pool.EraseDeadObjects();
}

void SnakesGameClient::OnUpdateObject(const void *data, size_t size)
{
    const game::RawObjectHeader *hdr = (const game::RawObjectHeader*) data;
    if( size < sizeof(*hdr) ) return;

    int id = (int32_t) be32toh(hdr->id);
    game::Object *obj = pool.Get(id);

    if( obj && !hdr->is_alive )
    {
        obj->Kill();
        return;
    }

    if( obj )
    {
        game::Syncable *syncable = dynamic_cast<game::Syncable*>(obj);
        if( syncable && syncable->ImportRaw(data, size) )
            return;
    }

    factory.CreateImport(data, size, &pool);
}
