#ifndef _GAME_OBJECT_H_
#define _GAME_OBJECT_H_

#include "GameObjectType.h"

namespace game
{

class Object
{
private:
    int id;
    bool is_alive;

public:
    Object(int id) : id(id), is_alive(true) {}
    virtual ~Object() {}

public:
    int Id() const { return id; }
    virtual ObjectType Type() const = 0;

    virtual bool CanBeErased() const { return !IsAlive(); }
    virtual bool IsAlive() const { return is_alive; }
    virtual void Kill() { is_alive = false; }

    virtual void Update(unsigned steptime) {}

};

}   // namespace game

#endif
