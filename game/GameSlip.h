#ifndef _GAME_SLIP_H_
#define _GAME_SLIP_H_

#include "GameEntity.h"
#include "GameFactory.h"

namespace game
{

class Slip : public Entity
{
private:
    typedef Entity Super;

private:
    ObjectPool *pool;

public:
    Slip(int id, int owner, ObjectPool *pool);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::SLIP; }

    char Graph() const final;
    int Colour() const final;

    void OnCollision(Entity *another) final;
};

}   // namespace game

#endif
