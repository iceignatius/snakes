#ifndef _GAME_DEF_H_
#define _GAME_DEF_H_

namespace game
{
namespace def
{

static const unsigned STEPTIME = 50;

static const int PLAYSTATE_ID = 0;
static const int MIN_PLAYER_ID = 1;
static const int MAX_PLAYER_ID = 0x7F;
static const int MIN_ENTITY_ID = MAX_PLAYER_ID + 1;
static const int MAX_ENTITY_ID = 0x7FFFF;
static const int MIN_EFFECTS_ID = MAX_ENTITY_ID + 1;
static const int MAX_EFFECTS_ID = 0x7FFFFFFF;

static const int MAX_PLAYER_NUM = 8;

static const int MAP_WIDTH  = 48;
static const int MAP_HEIGHT = 22;

static const unsigned STARTING_COUNTDOWN = 5 * 1000;

static const unsigned SNAKE_INIT_BODY_NUM = 3;
static const unsigned SNAKE_INIT_MOVE_DURATION = 380;   // Initial snake speed in unit of milli-second / grid
static const unsigned SNAKE_DOUBLESPEED_LENGTH = 60;

static const unsigned FOODGEN_DURATION = 500;
static const unsigned FOODGEN_TRYCOUNT = 8;
static const unsigned MAX_FOOD_NUM = 32;

}   // namespace def
}   // namespace game

#endif
