#ifndef _GAME_FACTORY_H_
#define _GAME_FACTORY_H_

#include "GamePool.h"

namespace game
{

class ObjectFactory
{
private:
    struct TypeInfo
    {
        ObjectPool::Region region;
        Object*(*creator)(int, int, ObjectPool*, ObjectFactory*);
    };

    std::map<ObjectType, TypeInfo> typelist;

public:
    void RegisterType(
        ObjectType type,
        ObjectPool::Region region,
        Object*(*creator)(int, int, ObjectPool*, ObjectFactory*));

private:
    const TypeInfo* FindTypeInfo(ObjectType type) const;

public:
    Object* Create(ObjectType type, ObjectPool *pool, int owner = 0);
    Object* CreateImport(const void *raw, size_t size, ObjectPool *pool);
};

}   // namespace game

#endif
