#include "ui/ConsoleColour.h"
#include "GameDef.h"
#include "GameState.h"
#include "GameTunnel.h"

using namespace game;

#define INIT_SUSPEND_COUNT 32

Tunnel::Tunnel(int id, int owner, ObjectPool *pool) :
    Super(id, owner), pool(pool), dest(-1), suspend_count(0)
{
}

Object* Tunnel::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new Tunnel(id, owner, pool);
}

void Tunnel::Update(unsigned steptime)
{
    Super::Update(steptime);

    if( IsOnServer() )
    {
        if( suspend_count )
            --suspend_count;

        if( dest < 0 )
            dest = FindAnotherTunnel();
    }
}

char Tunnel::Graph() const
{
    return '&';
}

int Tunnel::Colour() const
{
    return ui::colour.TUNNEL;
}

void Tunnel::OnCollision(Entity *another)
{
    if( !IsOnServer() ) return;
    if( IsSuspending() ) return;
    if( another->Type() != ObjectType::SNAKEHEAD ) return;

    Tunnel *destobj = dynamic_cast<Tunnel*>(pool->GetAlive(dest));
    if( !destobj ) return;

    another->SetPos(destobj->PosX(), destobj->PosY());
    another->MarkSyncAsap();

    destobj->SuspendAwhile();
}

bool Tunnel::IsOnServer() const
{
    PlayState *state = dynamic_cast<PlayState*>(pool->Get(def::PLAYSTATE_ID));
    return state && state->IsOnServer();
}

bool Tunnel::IsSuspending() const
{
    return suspend_count > 0;
}

void Tunnel::SuspendAwhile()
{
    suspend_count = INIT_SUSPEND_COUNT;
}

int Tunnel::FindAnotherTunnel() const
{
    for(const Object &obj : *pool)
    {
        if( obj.IsAlive() &&
            obj.Type() == ObjectType::TUNNEL &&
            obj.Id() != Id() )
        {
            return obj.Id();
        }
    }

    return -1;
}
