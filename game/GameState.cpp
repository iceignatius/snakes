#include <assert.h>
#include <endian.h>
#include "GameDef.h"
#include "GameState.h"

using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    uint8_t state;
    uint32_t starting_remain;
};
#pragma pack(pop)

PlayState::PlayState(int id, ObjectPool *pool, bool server_mode) :
    Super(id),
    pool(pool),
    server_mode(server_mode),
    state(GameState::PREPARING),
    starting_remain(0),
    starting_timer(def::STARTING_COUNTDOWN)
{
}

Object* PlayState::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new PlayState(id, pool, false);
}

void PlayState::Update(unsigned steptime)
{
    if( state != GameState::STARTING ) return;

    if( server_mode )
    {
        unsigned dura = starting_timer.GetDuration();
        unsigned pass = starting_timer.Passed();
        starting_remain = dura > pass ? dura - pass : 0;
    }
    else
    {
        starting_remain -=
            steptime < starting_remain ? steptime : starting_remain;
    }

    if( server_mode )
        MarkSyncAsap();
}

size_t PlayState::ExportRaw(void *buf, size_t size) const
{
    size_t super_encsize = Super::ExportRaw(nullptr, 0);
    size_t total_encsize = super_encsize + sizeof(RawInfo);
    assert(super_encsize);

    if( !buf )
        return total_encsize;
    if( size < total_encsize )
        return 0;
    if( Super::ExportRaw(buf, super_encsize) != super_encsize )
        return 0;

    RawInfo *pkg = (RawInfo*)( (uint8_t*)buf + super_encsize );
    pkg->state = (unsigned) state;
    pkg->starting_remain = htobe32(starting_remain);

    return total_encsize;
}

size_t PlayState::ImportRaw(const void *raw, size_t size)
{
    size_t super_decsize = Super::ImportRaw(nullptr, 0);
    size_t total_decsize = super_decsize + sizeof(RawInfo);
    assert(super_decsize);

    if( !raw )
        return total_decsize;
    if( size < total_decsize )
        return 0;
    if( Super::ImportRaw(raw, super_decsize) != super_decsize )
        return 0;

    const RawInfo *pkg = (const RawInfo*)( (const uint8_t*)raw + super_decsize );
    state = (GameState) pkg->state;
    starting_remain = be32toh(pkg->starting_remain);

    return total_decsize;
}

bool PlayState::IsOnServer() const
{
    return server_mode;
}

void PlayState::SetState(GameState state)
{
    this->state = state;

    if( state == GameState::STARTING )
        starting_timer.Reset();

    MarkSyncAsap();
}

GameState PlayState::GetState() const
{
    return state;
}

unsigned PlayState::GetStartRemainTime() const
{
    return state == GameState::STARTING ? starting_remain : 0;
}
