#ifndef _GAME_TIMER_H_
#define _GAME_TIMER_H_

#include <stddef.h>

namespace game
{

class Timer
{
private:
    unsigned duration;
    int remain;
    bool paused;

public:
    Timer(unsigned duration = 0, bool paused = false) :
        duration(duration), remain(duration), paused(paused)
    {
    }

public:
    void SetDuration(unsigned duration) { this->duration = duration; }
    unsigned GetDuration() const { return duration; }
    unsigned GetRemainTime() const { return !paused && remain > 0 ? remain : 0; }

    bool Expired() const { return !paused && remain <= 0; }
    void Reset() { remain = duration; }
    void Next() { remain += duration; }

    void Pause() { paused = true; }
    void Resume() { paused = false; }
    bool Paused() const { return paused; }

    void Update(unsigned steptime) { remain -= !paused ? steptime : 0; }

    size_t ExportRaw(void *buf, size_t size) const;
    size_t ImportRaw(const void *raw, size_t size);
};

}   // namespace game

#endif
