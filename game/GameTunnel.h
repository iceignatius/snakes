#ifndef _GAME_TUNNEL_H_
#define _GAME_TUNNEL_H_

#include "GameEntity.h"
#include "GameFactory.h"

namespace game
{

class Tunnel : public Entity
{
private:
    typedef Entity Super;

private:
    ObjectPool *pool;

    int dest;
    int suspend_count;

public:
    Tunnel(int id, int owner, ObjectPool *pool);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::TUNNEL; }

    void Update(unsigned steptime) final;

    char Graph() const final;
    int Colour() const final;

    void OnCollision(Entity *another) final;

private:
    bool IsOnServer() const;
    bool IsSuspending() const;
    void SuspendAwhile();
    int FindAnotherTunnel() const;
};

}   // namespace game

#endif
