#ifndef _GAME_SNAKE_HEAD_H_
#define _GAME_SNAKE_HEAD_H_

#include "GameMovable.h"
#include "GameFactory.h"

namespace game
{

class SnakeHead : public Movable
{
private:
    typedef Movable Super;

private:
    ObjectPool *pool;
    ObjectFactory *factory;

    int followed;
    int tail;

    unsigned len;
    double speedratio;

public:
    SnakeHead(int id, int owner, ObjectPool *pool, ObjectFactory *factory);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::SNAKEHEAD; }

    void Update(unsigned steptime) final;

    size_t ExportRaw(void *buf, size_t size) const final;
    size_t ImportRaw(const void *raw, size_t size) final;

    char Graph() const final { return '@'; }
    int Colour() const final { return -1; }

    void OnCollision(Entity *another) final;

public:
    void AppendBody(unsigned num);
    void SetTempSpeedRatio(double ratio);

private:
    void MoveBodies();
    void UpdateSpeed();
    void NotifyPlayerLength();
};

}   // namespace game

#endif
