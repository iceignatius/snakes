#include <stdint.h>
#include <endian.h>
#include "GameTimer.h"

using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    uint32_t duration;
    int32_t remain;
    uint8_t paused;
};
#pragma pack(pop)

size_t Timer::ExportRaw(void *buf, size_t size) const
{
    RawInfo *pkg = (RawInfo*) buf;

    if( !buf )
        return sizeof(*pkg);
    if( size < sizeof(*pkg) )
        return 0;

    pkg->duration = htobe32(duration);
    pkg->remain = htobe32(remain);
    pkg->paused = paused;

    return sizeof(*pkg);
}

size_t Timer::ImportRaw(const void *raw, size_t size)
{
    const RawInfo *pkg = (const RawInfo*) raw;

    if( !raw )
        return sizeof(*pkg);
    if( size < sizeof(*pkg) )
        return 0;

    duration = be32toh(pkg->duration);
    remain = (int32_t) be32toh(pkg->remain);
    paused = pkg->paused;

    return sizeof(*pkg);
}
