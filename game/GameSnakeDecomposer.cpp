#include <list>
#include "GameSnakeBody.h"
#include "GameFood.h"
#include "GameSnakeDecomposer.h"

using namespace std;
using namespace game;

struct BodyInfo
{
    int id;
    int x;
    int y;
};

SnakeDecomposer::SnakeDecomposer(int id, ObjectPool *pool, ObjectFactory *factory) :
    Super(id), pool(pool), factory(factory)
{
}

Object* SnakeDecomposer::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new SnakeDecomposer(id, pool, factory);
}

void SnakeDecomposer::Update(unsigned steptime)
{
    if( !steptime ) return;

    // Collect bodies with the dead head.
    list<BodyInfo> bodylist;
    for(Object &obj : *pool)
    {
        SnakeBody *body = dynamic_cast<SnakeBody*>(&obj);
        if( !body || !body->IsAlive() ) continue;

        if( !pool->GetAlive(body->Head()) )
        {
            BodyInfo info;
            info.id = body->Id();
            info.x = body->PosX();
            info.y = body->PosY();

            bodylist.push_back(info);
        }
    }

    // Kill bodies with the dead head.
    for(const BodyInfo &info : bodylist)
    {
        SnakeBody *body = dynamic_cast<SnakeBody*>(pool->GetAlive(info.id));
        if( body )
            body->Kill();
    }

    // Generate foods on the old bodies position.
    for(const BodyInfo &info : bodylist)
    {
        Food *food = dynamic_cast<Food*>(factory->Create(ObjectType::FOOD, pool));
        if( food )
            food->SetPos(info.x, info.y);
    }
}
