#include <assert.h>
#include <vector>
#include <endian.h>
#include "net/cmddef.h"
#include "net/netdef.h"
#include "GameRankCalculator.h"
#include "GameSnakeHead.h"
#include "GameSnakeBody.h"
#include "GameFood.h"
#include "GameFoodGenerator.h"
#include "GameSnakeDecomposer.h"
#include "GameWall.h"
#include "GameSlip.h"
#include "GameTunnel.h"
#include "SnakesGameServer.h"

using namespace std;

#define LOGLABEL "SnakesServer"
#define SYSNAME "system"

void SnakesGameServer::RegisterObjectTypes()
{
    factory.RegisterType(
        game::ObjectType::PLAYSTATE,
        game::ObjectPool::Region::PLAYSTATE,
        game::PlayState::CreateInstance);
    factory.RegisterType(
        game::ObjectType::PLAYER,
        game::ObjectPool::Region::PLAYER,
        game::Player::CreateInstance);
    factory.RegisterType(
        game::ObjectType::RANKCALC,
        game::ObjectPool::Region::EFFECTS,
        game::RankCalculator::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SNAKEHEAD,
        game::ObjectPool::Region::ENTITY,
        game::SnakeHead::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SNAKEBODY,
        game::ObjectPool::Region::ENTITY,
        game::SnakeBody::CreateInstance);
    factory.RegisterType(
        game::ObjectType::FOOD,
        game::ObjectPool::Region::ENTITY,
        game::Food::CreateInstance);
    factory.RegisterType(
        game::ObjectType::FOODGEN,
        game::ObjectPool::Region::EFFECTS,
        game::FoodGenerator::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SNAKEDECOMP,
        game::ObjectPool::Region::EFFECTS,
        game::SnakeDecomposer::CreateInstance);
    factory.RegisterType(
        game::ObjectType::WALL,
        game::ObjectPool::Region::ENTITY,
        game::Wall::CreateInstance);
    factory.RegisterType(
        game::ObjectType::SLIP,
        game::ObjectPool::Region::ENTITY,
        game::Slip::CreateInstance);
    factory.RegisterType(
        game::ObjectType::TUNNEL,
        game::ObjectPool::Region::ENTITY,
        game::Tunnel::CreateInstance);
}

SnakesGameServer::SnakesGameServer() :
    network(nullptr),
    sync_timer(NETDEF_SYNC_DURATION),
    max_players_num(0),
    curr_players_num(0),
    playstate(nullptr)
{
    RegisterObjectTypes();
}

SnakesGameServer::~SnakesGameServer()
{
    Close();
}

void SnakesGameServer::Open(ServerNetApi *network, const string &scenefile)
{
    Close();

    try
    {
        this->network = network;

        if( scenefile.empty() )
            scene.LoadDefaultBlankMap();
        else if( !scene.LoadFile(scenefile) )
            throw runtime_error("Failed to load scene file \"" + scenefile + "\"!");
        printf(
            "%s: Server started with scene \"%s\".\n",
            LOGLABEL,
            scene.Name().c_str());

        max_players_num = min(scene.MaxPlayers(), game::def::MAX_PLAYER_NUM);
        curr_players_num = 0;

        playstate = new game::PlayState(game::def::PLAYSTATE_ID, &pool, true);
        pool.Insert(playstate);
    }
    catch(exception &e)
    {
        Close();
        throw;
    }
}

void SnakesGameServer::Close()
{
    playstate = nullptr;
    curr_players_num = 0;
    max_players_num = 0;

    pool.EraseAll();

    network = nullptr;
}

void SnakesGameServer::UpdateObjects(unsigned steptime)
{
    GameState state = playstate ? playstate->GetState() : GameState::PREPARING;

    for(game::Object &obj : pool)
        obj.Update( state == GameState::PLAYING ? steptime : 0 );
}

vector<uint8_t> SnakesGameServer::ExportObject(const game::Syncable *syncable)
{
    if( syncable->IsAlive() )
    {
        size_t bufsize = syncable->ExportRaw(nullptr, 0);
        assert(bufsize);

        vector<uint8_t> raw(bufsize);
        size_t encsize = syncable->ExportRaw(raw.data(), raw.size());
        assert( encsize == bufsize );
        (void)encsize; // Avoid the unused warning!

        return raw;
    }
    else
    {
        /*
         * It is no need to encode the full package for a dead object
         * which is going to be removed.
         */

        vector<uint8_t> raw(sizeof(game::RawObjectHeader));

        game::RawObjectHeader *pkg = (game::RawObjectHeader*) raw.data();
        pkg->id = htobe32(syncable->Id());
        pkg->type = htobe16((unsigned)syncable->Type());
        pkg->is_alive = htobe16(false);

        return raw;
    }
}

void SnakesGameServer::DoSyncObjects()
{
    unsigned objnum = 0;
    for(game::Object &obj : pool)
    {
        game::Syncable *syncable = dynamic_cast<game::Syncable*>(&obj);
        if( !syncable || !syncable->CheckIfShouldSync() ) continue;

        vector<uint8_t> raw = ExportObject(syncable);
        if( network && !network->PushObject(raw.data(), raw.size()) )
            break;

        ++objnum;
        syncable->LowerSyncPriority();
    }

    if( network && objnum )
        network->SendThenClearObjects();
}

void SnakesGameServer::CheckSyncObjects()
{
    bool sync_now = false;
    for(game::Object &obj : pool)
    {
        game::Syncable *syncable = dynamic_cast<game::Syncable*>(&obj);
        if( syncable && syncable->GetSyncPriority() == 0 )
        {
            sync_now = true;
            break;
        }
    }

    if( sync_now || sync_timer.Expired() )
    {
        DoSyncObjects();
        sync_timer.Reset();
    }
}

void SnakesGameServer::MarkSyncAllObjects()
{
    for(game::Object &obj : pool)
    {
        game::Syncable *syncable = dynamic_cast<game::Syncable*>(&obj);
        if( syncable )
            syncable->MarkSyncAsap();
    }
}

int SnakesGameServer::CountReadyPlayers()
{
    int count = 0;
    for(game::Object &obj : pool)
    {
        if( obj.Id() > game::def::MAX_PLAYER_ID ) break;

        game::Player *player = dynamic_cast<game::Player*>(&obj);
        if( !player || !player->IsAlive() ) continue;

        if( player->IsReady() )
            ++count;
    }

    return count;
}

int SnakesGameServer::CountSnakeAlivePlayers()
{
    int count = 0;
    for(game::Object &obj : pool)
    {
        if( obj.Id() > game::def::MAX_PLAYER_ID ) break;

        game::Player *player = dynamic_cast<game::Player*>(&obj);
        if( !player || !player->IsAlive() ) continue;

        if( player->IsSnakeAlive() )
            ++count;
    }

    return count;
}

void SnakesGameServer::UnreadyPlayers()
{
    for(game::Object &obj : pool)
    {
        if( obj.Id() > game::def::MAX_PLAYER_ID ) break;

        game::Player *player = dynamic_cast<game::Player*>(&obj);
        if( !player || !player->IsAlive() ) continue;

        player->SetReady(false);
    }
}

void SnakesGameServer::ProcStateWorks()
{
    if( !playstate ) return;

    switch( playstate->GetState() )
    {
    case GameState::PREPARING:
        if( curr_players_num && CountReadyPlayers() >= curr_players_num )
        {
            RebuildGameObjects();

            playstate->SetState(GameState::STARTING);
            printf("%s: Game starting.\n", LOGLABEL);
        }
        break;

    case GameState::STARTING:
        if( playstate->GetStartRemainTime() == 0 )
        {
            playstate->SetState(GameState::PLAYING);
            printf("%s: Game playing.\n", LOGLABEL);

            assert(network);
            network->SendTalkToAllUsers(0, SYSNAME, "Game started.");
        }
        break;

    case GameState::PAUSED:
    case GameState::PLAYING:
        if( curr_players_num <= 0 )
        {
            KillGameplayObjects();

            playstate->SetState(GameState::PREPARING);
            printf("%s: Game reset.\n", LOGLABEL);
        }
        else if( CountSnakeAlivePlayers() <= 0 ||
            ( curr_players_num > 1 && CountSnakeAlivePlayers() <= 1 ) )
        {
            UnreadyPlayers();

            playstate->SetState(GameState::FINISHED);
            printf("%s: Game finished.\n", LOGLABEL);

            assert(network);
            network->SendTalkToAllUsers(0, SYSNAME, "Game finished.");
        }
        break;

    case GameState::FINISHED:
        if( curr_players_num <= 0 )
        {
            KillGameplayObjects();

            playstate->SetState(GameState::PREPARING);
            printf("%s: Game reset.\n", LOGLABEL);
        }
        else if( CountReadyPlayers() >= curr_players_num )
        {
            RebuildGameObjects();

            playstate->SetState(GameState::STARTING);
            printf("%s: Game starting.\n", LOGLABEL);
        }
        break;
    }
}

void SnakesGameServer::KillGameplayObjects()
{
    // Kill all game objects except players and the game state.
    for(game::Object &obj : pool)
    {
        if( obj.Id() >= game::def::MIN_ENTITY_ID &&
            obj.Id() <= game::def::MAX_EFFECTS_ID )
        {
            obj.Kill();
        }
    }
}

void SnakesGameServer::BuildPlayerInitialSnake(game::Player *player)
{
    player->SetLength(0);
    player->ClearKillCount();
    player->SetRank(0);

    int x, y;
    if( !scene.GetPlayerStartPos(player->PlayerIndex(), x, y) )
    {
        char errmsg[64];
        snprintf(
            errmsg,
            sizeof(errmsg),
            "Failed to get player (%d) starting position!",
            player->PlayerIndex());
        fprintf(stderr, "%s: %s\n", LOGLABEL, errmsg);
        throw runtime_error(errmsg);
    }

    game::SnakeHead *head = dynamic_cast<game::SnakeHead*>(
        factory.Create(game::ObjectType::SNAKEHEAD, &pool, player->Id()));
    assert(head);

    head->SetPos(x, y);
    head->AppendBody(game::def::SNAKE_INIT_BODY_NUM);
    player->AssignSnake(head->Id());
}

void SnakesGameServer::RebuildGameObjects()
{
    KillGameplayObjects();

    for(const auto &objinfo : scene.GetObjectInfoList())
    {
        game::Entity *entity = dynamic_cast<game::Entity*>(
            factory.Create(objinfo.type, &pool));

        if( !entity )
        {
            fprintf(
                stderr,
                "%s: Unsupported object type (type id=%d)!\n",
                LOGLABEL,
                (int) objinfo.type);
            continue;
        }

        entity->SetPos(objinfo.x, objinfo.y);
    }

    for(game::Object &obj : pool)
    {
        game::Player *player = dynamic_cast<game::Player*>(&obj);
        if( player && player->IsAlive() )
            BuildPlayerInitialSnake(player);
    }

    factory.Create(game::ObjectType::RANKCALC, &pool);
    factory.Create(game::ObjectType::FOODGEN, &pool);
    factory.Create(game::ObjectType::SNAKEDECOMP, &pool);

    MarkSyncAllObjects();
}

void SnakesGameServer::KillOutOfMapEntities()
{
    for(game::Object &obj : pool)
    {
        game::Entity *entity = dynamic_cast<game::Entity*>(&obj);
        if( !entity || !entity->IsAlive() ) continue;

        int x = entity->PosX();
        int y = entity->PosY();
        if( x < 0 || game::def::MAP_WIDTH <= x ||
            y < 0 || game::def::MAP_HEIGHT <= y )
        {
            entity->Kill();
        }
    }
}

void SnakesGameServer::ProcCollisionReaction()
{
    for(game::Object &obj : pool)
    {
        game::Entity *entity = dynamic_cast<game::Entity*>(&obj);
        if( !entity || !entity->IsAlive() ) continue;

        for(game::Object &another_obj : pool)
        {
            game::Entity *another = dynamic_cast<game::Entity*>(&another_obj);
            if( !another || !another->IsAlive() ) continue;
            if( entity->Id() == another->Id() ) continue;

            if( entity->PosX() == another->PosX() &&
                entity->PosY() == another->PosY() )
            {
                entity->OnCollision(another);
            }
        }
    }
}

void SnakesGameServer::RunStep(unsigned steptime)
{
    UpdateObjects(steptime);
    ProcStateWorks();
    KillOutOfMapEntities();
    ProcCollisionReaction();
    CheckSyncObjects();
    pool.EraseDeadObjects();
}

QueryInfo SnakesGameServer::OnQuery()
{
    QueryInfo info;
    info.playstate = playstate ? playstate->GetState() : GameState::PREPARING;
    info.max_joined_num = max_players_num;
    info.curr_joined_num = curr_players_num;

    return info;
}

int SnakesGameServer::OnJoin(int &userid, const std::string &name)
{
    if( curr_players_num >= max_players_num )
        return CMDRC_ERR_JOIN_FULL;

    if( !playstate || playstate->GetState() != GameState::PREPARING )
        return CMDRC_ERR_JOIN_PLAYING;

    userid = pool.GenerateNewId(game::ObjectPool::Region::PLAYER);
    if( userid < 0 )
        return CMDRC_ERR_JOIN_FULL;

    game::Player *player = new game::Player(userid, &pool);
    player->SetName(name);
    player->AcquirePlayerIndex();

    pool.Insert(player);
    ++curr_players_num;

    printf(
        "%s: A player join, id=%d, name=%s\n",
        LOGLABEL,
        player->Id(),
        player->Name().c_str());

    assert(network);
    network->SendTalkToAllUsers(0, SYSNAME, player->Name() + " join game.");

    MarkSyncAllObjects();
    return 0;
}

void SnakesGameServer::OnQuit(int userid)
{
    game::Player *player = dynamic_cast<game::Player*>(pool.GetAlive(userid));
    if( !player ) return;

    printf(
        "%s: A player quit, id=%d, name=%s\n",
        LOGLABEL,
        player->Id(),
        player->Name().c_str());

    assert(network);
    network->SendTalkToAllUsers(0, SYSNAME, player->Name() + " quit game.");

    player->ReleasePlayerIndex();
    player->Kill();
    --curr_players_num;
}

void SnakesGameServer::OnConnError(int userid, int errcode)
{
    game::Player *player = dynamic_cast<game::Player*>(pool.GetAlive(userid));
    if( !player ) return;

    printf(
        "%s: A player disconnected, id=%d, name=%s\n",
        LOGLABEL,
        player->Id(),
        player->Name().c_str());

    assert(network);
    network->SendTalkToAllUsers(0, SYSNAME, player->Name() + " disconnected!");

    player->ReleasePlayerIndex();
    player->Kill();
    --curr_players_num;
}

void SnakesGameServer::OnTalk(int userid, const string &text)
{
    game::Player *player = dynamic_cast<game::Player*>(pool.GetAlive(userid));
    if( !player ) return;

    if( network )
        network->SendTalkToAllUsers(player->Id(), player->Name(), text);
}

void SnakesGameServer::OnSetState(int userid, GameState state)
{
    game::Player *player = dynamic_cast<game::Player*>(pool.GetAlive(userid));
    if( !player ) return;

    player->SetReady( state > GameState::PREPARING );

    if( state > GameState::PREPARING )
    {
        assert(network);
        network->SendTalkToAllUsers(0, SYSNAME, player->Name() + " ready play.");
    }
}

void SnakesGameServer::OnSetDir(int userid, char dir)
{
    game::Player *player = dynamic_cast<game::Player*>(pool.GetAlive(userid));
    if( !player ) return;

    GameState state = playstate ? playstate->GetState() : GameState::PREPARING;
    if( state == GameState::PLAYING || state == GameState::PAUSED )
        player->SetSnakeDir(dir);
}
