#include <fstream>
#include "common/pathutl.h"
#include "GameDef.h"
#include "GameScene.h"

using namespace std;
using namespace game;

#define LOGLABEL "SnakesScene"

void Scene::PushPlayerInfo(
    int x,
    int y,
    int index,
    map<int, PlayerStartInfo> &start_map)
{
    PlayerStartInfo info;
    info.x = x;
    info.y = y;

    if( index == 0 || index > def::MAX_PLAYER_NUM )
        throw out_of_range("Invalid player index: " + to_string(index) + "!");
    if( start_map.find(index) != start_map.end() )
        throw invalid_argument("Duplicated player index: " + to_string(index) + "!");

    start_map[index] = info;
}

void Scene::PushObjectInfo(
    int x,
    int y,
    int ch,
    vector<ObjectInfo> &objinfo_list)
{
    ObjectInfo info;
    info.x = x;
    info.y = y;

    switch( ch )
    {
    case '$':
        info.type = ObjectType::FOOD;
        break;

    case '#':
        info.type = ObjectType::WALL;
        break;

    case '%':
        info.type = ObjectType::SLIP;
        break;

    case '&':
        info.type = ObjectType::TUNNEL;
        break;

    default:
        return;
    }

    objinfo_list.push_back(info);
}

void Scene::ParseLineSymbols(
    const string &line,
    int row,
    map<int, PlayerStartInfo> &start_map,
    vector<ObjectInfo> &objinfo_list)
{
    if( line.length() < def::MAP_WIDTH )
        throw invalid_argument("Skewed line at (" + to_string(row) + ",:)!");

    int y = def::MAP_HEIGHT - 1 - row;
    for(int col = 0; col < def::MAP_WIDTH; ++col)
    {
        int x = col;
        char ch = line[col];

        if( isdigit(ch) )
            PushPlayerInfo(x, y, ch - '0', start_map);
        else if( ch != ' ' )
            PushObjectInfo(x, y, ch, objinfo_list);
    }
}

void Scene::ParseFileSymbols(
    const string &filename,
    map<int, PlayerStartInfo> &start_map,
    vector<ObjectInfo> &objinfo_list)
{
    fstream file(filename, ios::in);
    if( !file.is_open() )
        throw runtime_error("Failed to open file \"" + filename + "\"!");

    for(int row = 0; row < def::MAP_HEIGHT; ++row)
    {
        string line;
        getline(file, line);
        ParseLineSymbols(line, row, start_map, objinfo_list);
    }
}

void Scene::VerifyObjectList(const vector<ObjectInfo> &objinfo_list)
{
    // Tunnels count must be 0 or 2

    unsigned tunnels_count = 0;
    for(const auto &info : objinfo_list)
    {
        if( info.type == ObjectType::TUNNEL )
            ++ tunnels_count;
    }

    if( tunnels_count != 0 && tunnels_count != 2 )
        throw out_of_range("Invalid tunnels number: " + to_string(tunnels_count) + "!");
}

vector<Scene::PlayerStartInfo> Scene::ConvertPlayerStartInfoList(
    const map<int, PlayerStartInfo> &start_map)
{
    vector<PlayerStartInfo> list;

    // The natural player with index 0
    static const PlayerStartInfo player0 = { 0, 0 };
    list.push_back(player0);

    for(int i = 1; i <= def::MAX_PLAYER_NUM; ++i)
    {
        auto iter = start_map.find(i);
        if( iter == start_map.end() )
            break;

        list.push_back(iter->second);
    }

    return list;
}

void Scene::Clear()
{
    scene_name.clear();
    start_list.clear();
    objinfo_list.clear();
}

void Scene::LoadDefaultBlankMap()
{
    Clear();

    scene_name = "<blank>";

    static const PlayerStartInfo player0 = { 0, 0 };
    static const PlayerStartInfo player1 = { def::MAP_WIDTH / 2, def::MAP_HEIGHT * 3 / 4 };
    static const PlayerStartInfo player2 = { def::MAP_WIDTH / 2, def::MAP_HEIGHT / 4 };
    static const PlayerStartInfo player3 = { def::MAP_WIDTH / 4, def::MAP_HEIGHT * 3 / 4 };
    static const PlayerStartInfo player4 = { def::MAP_WIDTH * 3 / 4, def::MAP_HEIGHT / 4 };
    static const PlayerStartInfo player5 = { def::MAP_WIDTH * 3 / 4, def::MAP_HEIGHT * 3 / 4 };
    static const PlayerStartInfo player6 = { def::MAP_WIDTH / 4, def::MAP_HEIGHT / 4 };
    static const PlayerStartInfo player7 = { def::MAP_WIDTH / 4, def::MAP_HEIGHT / 2 };
    static const PlayerStartInfo player8 = { def::MAP_WIDTH * 3 / 4, def::MAP_HEIGHT / 2 };

    start_list.push_back(player0);  // The natural player with index 0
    start_list.push_back(player1);
    start_list.push_back(player2);
    start_list.push_back(player3);
    start_list.push_back(player4);
    start_list.push_back(player5);
    start_list.push_back(player6);
    start_list.push_back(player7);
    start_list.push_back(player8);
}

bool Scene::LoadFile(const string &filename)
{
    Clear();

    try
    {
        scene_name = pathutl::RemoveFileExt(pathutl::ExtractFileName(filename));
        if( scene_name.empty() )
            throw invalid_argument("Invalid scene name!");

        map<int, PlayerStartInfo> start_map;
        ParseFileSymbols(filename, start_map, objinfo_list);

        VerifyObjectList(objinfo_list);

        start_list = ConvertPlayerStartInfoList(start_map);
        if( start_list.size() <= 1 )
            throw invalid_argument("No players start position be found!");
    }
    catch(exception &e)
    {
        fprintf(stderr, "%s: Failed to load scene: %s\n", LOGLABEL, e.what());

        Clear();
        return false;
    }

    return true;
}

int Scene::MaxPlayers() const
{
    return start_list.size() > 1 ? start_list.size() - 1 : 0;
}

bool Scene::GetPlayerStartPos(int index, int &x, int &y)
{
    if( index < 1 || (int) start_list.size() <= index )
        return false;

    x = start_list[index].x;
    y = start_list[index].y;
    return true;
}
