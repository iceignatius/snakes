#include <assert.h>
#include <endian.h>
#include "GamePlayer.h"
#include "GameSnakeBody.h"

using namespace std;
using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    int32_t followed;
    int32_t head;
    uint8_t moved;
};
#pragma pack(pop)

SnakeBody::SnakeBody(int id, int owner, ObjectPool *pool) :
    Super(id, owner), pool(pool), moved(false), followed(-1), head(-1)
{
}

Object* SnakeBody::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new SnakeBody(id, owner, pool);
}

void SnakeBody::Update(unsigned steptime)
{
    Super::Update(steptime);
}

size_t SnakeBody::ExportRaw(void *buf, size_t size) const
{
    size_t super_encsize = Super::ExportRaw(nullptr, 0);
    size_t total_encsize = super_encsize + sizeof(RawInfo);
    assert(super_encsize);

    if( !buf )
        return total_encsize;
    if( size < total_encsize )
        return 0;
    if( Super::ExportRaw(buf, super_encsize) != super_encsize )
        return 0;

    RawInfo *pkg = (RawInfo*)( (uint8_t*)buf + super_encsize );
    pkg->followed = htobe32(followed);
    pkg->head = htobe32(head);
    pkg->moved = moved;

    return total_encsize;
}

size_t SnakeBody::ImportRaw(const void *raw, size_t size)
{
    size_t super_decsize = Super::ImportRaw(nullptr, 0);
    size_t total_decsize = super_decsize + sizeof(RawInfo);
    assert(super_decsize);

    if( !raw )
        return total_decsize;
    if( size < total_decsize )
        return 0;
    if( Super::ImportRaw(raw, super_decsize) != super_decsize )
        return 0;

    const RawInfo *pkg = (const RawInfo*)( (const uint8_t*)raw + super_decsize );
    followed = (int32_t) be32toh(pkg->followed);
    head = (int32_t) be32toh(pkg->head);
    moved = pkg->moved;

    return total_decsize;
}

void SnakeBody::SetPos(int x, int y)
{
    Super::SetPos(x, y);

    if( head >= 0 )
    {
        moved = true;
        MarkSyncLater();
    }
}

void SnakeBody::OnCollision(Entity *another)
{
    if( !moved ) return;
    if( another->Type() != ObjectType::SNAKEHEAD ) return;

    another->Kill();

    Player *selfplayer = dynamic_cast<Player*>(pool->GetAlive(Owner()));
    if( selfplayer )
        selfplayer->IncreaseKillCount();
}

void SnakeBody::SetFollowed(int id)
{
    followed = id;
    MarkSyncAsap();
}

void SnakeBody::SetHead(int id)
{
    head = id;
    MarkSyncAsap();
}
