#include "GameDef.h"
#include "GamePool.h"

using namespace std;
using namespace game;

ObjectPool::ObjectPool() :
    last_player_id(0), last_entity_id(0), last_effects_id(0)
{
}

int ObjectPool::GenerateNewId(int minval, int maxval, int &lastval)
{
    lastval = min(max(minval - 1, lastval), maxval);

    for(int id = lastval + 1; id <= maxval; ++id)
    {
        if( !Existed(id) )
        {
            lastval = id;
            return id;
        }
    }

    for(int id = minval; id < lastval; ++id)
    {
        if( !Existed(id) )
        {
            lastval = id;
            return id;
        }
    }

    return -1;
}

int ObjectPool::GenerateNewId(Region region)
{
    switch( region )
    {
    case Region::PLAYSTATE:
        return !Existed(def::PLAYSTATE_ID) ? def::PLAYSTATE_ID : -1;

    case Region::PLAYER:
        return GenerateNewId(def::MIN_PLAYER_ID, def::MAX_PLAYER_ID, last_player_id);

    case Region::ENTITY:
        return GenerateNewId(def::MIN_ENTITY_ID, def::MAX_ENTITY_ID, last_entity_id);

    case Region::EFFECTS:
        return GenerateNewId(def::MIN_EFFECTS_ID, def::MAX_EFFECTS_ID, last_effects_id);
    }

    return -1;
}

bool ObjectPool::Existed(int id) const
{
    return pool.find(id) != pool.end();
}

void ObjectPool::Insert(Object *obj)
{
    if( obj )
        pool[obj->Id()] = shared_ptr<Object>(obj);
}

Object* ObjectPool::Get(int id)
{
    auto iter = pool.find(id);
    return iter != pool.end() ? iter->second.get() : nullptr;
}

const Object* ObjectPool::Get(int id) const
{
    auto iter = pool.find(id);
    return iter != pool.end() ? iter->second.get() : nullptr;
}

Object* ObjectPool::GetAlive(int id)
{
    auto iter = pool.find(id);
    if( iter == pool.end() ) return nullptr;

    Object *obj = iter->second.get();
    return obj->IsAlive() ? obj : nullptr;
}

const Object* ObjectPool::GetAlive(int id) const
{
    auto iter = pool.find(id);
    if( iter == pool.end() ) return nullptr;

    const Object *obj = iter->second.get();
    return obj->IsAlive() ? obj : nullptr;
}

void ObjectPool::EraseAll()
{
    pool.clear();
}

void ObjectPool::EraseDeadObjects()
{
    for(auto iter = pool.begin(); iter != pool.end();)
    {
        auto obj = (iter++)->second;

        if( obj->CanBeErased() )
            pool.erase(obj->Id());
    }
}
