#ifndef _GAME_RANK_CALCULATOR_H_
#define _GAME_RANK_CALCULATOR_H_

#include "GameFactory.h"

namespace game
{

class RankCalculator : public Object
{
private:
    typedef Object Super;

private:
    ObjectPool *pool;

public:
    RankCalculator(int id, ObjectPool *pool);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::RANKCALC; }

    void Update(unsigned steptime) final;
};

}   // namespace game

#endif
