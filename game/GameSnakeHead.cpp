#include <assert.h>
#include <math.h>
#include <endian.h>
#include "GameDef.h"
#include "GamePlayer.h"
#include "GameSnakeBody.h"
#include "GameSnakeHead.h"

using namespace std;
using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    int32_t followed;
    int32_t tail;
    uint16_t len;
    uint64_t speedratio;    // Integer of the speed ratio (floating) * 1000
};
#pragma pack(pop)

SnakeHead::SnakeHead(int id, int owner, ObjectPool *pool, ObjectFactory *factory) :
    Super(id, owner, def::SNAKE_INIT_MOVE_DURATION),
    pool(pool),
    factory(factory),
    followed(-1),
    tail(-1),
    len(1),
    speedratio(1.0)
{
}

Object* SnakeHead::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new SnakeHead(id, owner, pool, factory);
}

void SnakeHead::Update(unsigned steptime)
{
    Super::Update(steptime);

    if( JustMoved() )
    {
        MoveBodies();
        UpdateSpeed();
        MarkSyncAsap();
    }

    Player *player = dynamic_cast<Player*>(pool->GetAlive(Owner()));
    if( player && player->Length() != len )
        NotifyPlayerLength();
}

size_t SnakeHead::ExportRaw(void *buf, size_t size) const
{
    size_t super_encsize = Super::ExportRaw(nullptr, 0);
    size_t total_encsize = super_encsize + sizeof(RawInfo);
    assert(super_encsize);

    if( !buf )
        return total_encsize;
    if( size < total_encsize )
        return 0;
    if( Super::ExportRaw(buf, super_encsize) != super_encsize )
        return 0;

    RawInfo *pkg = (RawInfo*)( (uint8_t*)buf + super_encsize );
    pkg->followed = htobe32(followed);
    pkg->tail = htobe32(tail);
    pkg->len = htobe16(len);
    pkg->speedratio = htobe64(speedratio * 1000);

    return total_encsize;
}

size_t SnakeHead::ImportRaw(const void *raw, size_t size)
{
    size_t super_decsize = Super::ImportRaw(nullptr, 0);
    size_t total_decsize = super_decsize + sizeof(RawInfo);
    assert(super_decsize);

    if( !raw )
        return total_decsize;
    if( size < total_decsize )
        return 0;
    if( Super::ImportRaw(raw, super_decsize) != super_decsize )
        return 0;

    const RawInfo *pkg = (const RawInfo*)( (const uint8_t*)raw + super_decsize );
    followed = (int32_t) be32toh(pkg->followed);
    tail = (int32_t) be32toh(pkg->tail);
    len = be16toh(pkg->len);
    speedratio = be64toh(pkg->speedratio) / 1000.0;

    return total_decsize;
}

void SnakeHead::OnCollision(Entity *another)
{
    if( another->Type() != ObjectType::SNAKEHEAD ) return;

    Player *anotherplayer = dynamic_cast<Player*>(pool->GetAlive(another->Owner()));
    if( anotherplayer )
        anotherplayer->IncreaseKillCount();
    Player *selfplayer = dynamic_cast<Player*>(pool->GetAlive(Owner()));
    if( selfplayer )
        selfplayer->IncreaseKillCount();

    another->Kill();
    this->Kill();
}

void SnakeHead::AppendBody(unsigned int num)
{
    while( num-- )
    {
        SnakeBody *newbody = dynamic_cast<SnakeBody*>(
            factory->Create(ObjectType::SNAKEBODY, pool, Owner()));
        assert(newbody);

        SnakeBody *tailbody = dynamic_cast<SnakeBody*>(pool->GetAlive(tail));
        if( tailbody )
        {
            newbody->SetPos(tailbody->PosX(), tailbody->PosY());
            newbody->SetHead(Id());
            tailbody->SetFollowed(newbody->Id());
            tail = newbody->Id();
        }
        else
        {
            newbody->SetPos(PosX(), PosY());
            newbody->SetHead(Id());
            followed = newbody->Id();
            tail = newbody->Id();
        }

        ++len;
    }

    NotifyPlayerLength();
    MarkSyncAsap();
}

void SnakeHead::SetTempSpeedRatio(double ratio)
{
    if( ratio > 0 )
        speedratio = ratio;
}

void SnakeHead::MoveBodies()
{
    int new_x = LastPosX();
    int new_y = LastPosY();
    int old_x = new_x;
    int old_y = new_y;

    int body_id = followed;
    while(true)
    {
        SnakeBody *body = dynamic_cast<SnakeBody*>(pool->GetAlive(body_id));
        if( !body ) break;

        old_x = body->PosX();
        old_y = body->PosY();
        body->SetPos(new_x, new_y);
        new_x = old_x;
        new_y = old_y;

        body_id = body->Followed();
    }
}

void SnakeHead::UpdateSpeed()
{
    unsigned movedura =
        def::SNAKE_INIT_MOVE_DURATION /
        pow(2, (double) len / def::SNAKE_DOUBLESPEED_LENGTH);

    movedura /= speedratio;
    speedratio = 1; // Restore speed ratio!

    movedura = max(movedura, 40U);
    SetMoveDuration(movedura);

    MarkSyncLater();
}

void SnakeHead::NotifyPlayerLength()
{
    Player *player = dynamic_cast<Player*>(pool->GetAlive(Owner()));
    if( player )
        player->SetLength(len);
}
