#include "ui/ConsoleColour.h"
#include "GameSnakeHead.h"
#include "GameSnakeBody.h"
#include "GameSlip.h"

using namespace game;

Slip::Slip(int id, int owner, ObjectPool *pool) :
    Super(id, owner), pool(pool)
{
}

Object* Slip::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new Slip(id, owner, pool);
}

char Slip::Graph() const
{
    return '%';
}

int Slip::Colour() const
{
    return ui::colour.SLIP;
}

void Slip::OnCollision(Entity *another)
{
    SnakeHead *snakehead =
        another->Type() == ObjectType::SNAKEHEAD ?
        dynamic_cast<SnakeHead*>(another) :
        nullptr;

    if( !snakehead )
    {
        // Try to get snake head by its body.
        SnakeBody *snakebody =
            another->Type() == ObjectType::SNAKEBODY ?
            dynamic_cast<SnakeBody*>(another) :
            nullptr;
        snakehead =
            snakebody ?
            dynamic_cast<SnakeHead*>(pool->GetAlive(snakebody->Head())) :
            nullptr;
    }

    if( snakehead )
        snakehead->SetTempSpeedRatio(2);
}
