#include <assert.h>
#include <endian.h>
#include "GameMovable.h"

using namespace std;
using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    uint8_t currdir;
    uint8_t lastdir;
    int16_t last_x;
    int16_t last_y;
};
#pragma pack(pop)

Movable::Movable(int id, int owner, unsigned move_dura) :
    Super(id, owner),
    currdir(0),
    lastdir(0),
    movetimer(move_dura),
    just_moved(false),
    last_x(0),
    last_y(0)
{
}

void Movable::Update(unsigned steptime)
{
    Super::Update(steptime);

    just_moved = false;
    if( !IsAlive() ) return;

    movetimer.Update(steptime);
    if( !movetimer.Expired() ) return;

    movetimer.Next();
    if(( just_moved = MoveOneGrid() ))
        MarkSyncLater();

    if( lastdir != currdir )
    {
        lastdir = currdir;
        MarkSyncAsap();
    }
}

size_t Movable::ExportRaw(void *buf, size_t size) const
{
    size_t super_encsize = Super::ExportRaw(nullptr, 0);
    size_t timer_encsize = movetimer.ExportRaw(nullptr, 0);
    size_t total_encsize = super_encsize + sizeof(RawInfo) + timer_encsize;
    assert(super_encsize);
    assert(timer_encsize);

    if( !buf )
        return total_encsize;
    if( size < total_encsize )
        return 0;
    if( Super::ExportRaw(buf, super_encsize) != super_encsize )
        return 0;

    RawInfo *pkg = (RawInfo*)( (uint8_t*)buf + super_encsize );
    pkg->currdir = currdir;
    pkg->lastdir = lastdir;
    pkg->last_x = htobe16(last_x);
    pkg->last_y = htobe16(last_y);

    uint8_t *timer_buf = (uint8_t*)buf + super_encsize + sizeof(RawInfo);
    if( movetimer.ExportRaw(timer_buf, timer_encsize) != timer_encsize )
        return 0;

    return total_encsize;
}

size_t Movable::ImportRaw(const void *raw, size_t size)
{
    size_t super_decsize = Super::ImportRaw(nullptr, 0);
    size_t timer_decsize = movetimer.ImportRaw(nullptr, 0);
    size_t total_decsize = super_decsize + sizeof(RawInfo) + timer_decsize;
    assert(super_decsize);
    assert(timer_decsize);

    if( !raw )
        return total_decsize;
    if( size < total_decsize )
        return 0;
    if( Super::ImportRaw(raw, super_decsize) != super_decsize )
        return 0;

    const RawInfo *pkg = (const RawInfo*)( (const uint8_t*)raw + super_decsize );
    currdir = pkg->currdir;
    lastdir = pkg->lastdir;
    last_x = (int16_t) be16toh(pkg->last_x);
    last_y = (int16_t) be16toh(pkg->last_y);

    const uint8_t *timer_raw = (const uint8_t*)raw + super_decsize + sizeof(RawInfo);
    if( movetimer.ImportRaw(timer_raw, timer_decsize) != timer_decsize )
        return 0;

    return total_decsize;
}

bool Movable::MoveOneGrid()
{
    switch(currdir)
    {
    case 'N':
    case 'n':
    case 'U':
    case 'u':
        last_x = PosX();
        last_y = PosY();
        SetPos(last_x, last_y + 1);
        return true;

    case 'S':
    case 's':
    case 'D':
    case 'd':
        last_x = PosX();
        last_y = PosY();
        SetPos(last_x, last_y - 1);
        return true;

    case 'W':
    case 'w':
    case 'L':
    case 'l':
        last_x = PosX();
        last_y = PosY();
        SetPos(last_x - 1, last_y);
        return true;

    case 'E':
    case 'e':
    case 'R':
    case 'r':
        last_x = PosX();
        last_y = PosY();
        SetPos(last_x + 1, last_y);
        return true;

    default:
        return false;
    }
}

bool Movable::SetDir(int dir)
{
    bool dir_changed = false;
    switch(dir)
    {
    case 'N':
    case 'n':
    case 'U':
    case 'u':
        if(( dir_changed = ( lastdir != 'N' && lastdir != 'S' ) ))
            currdir = 'N';
        break;

    case 'S':
    case 's':
    case 'D':
    case 'd':
        if(( dir_changed = ( lastdir != 'N' && lastdir != 'S' ) ))
            currdir = 'S';
        break;

    case 'W':
    case 'w':
    case 'L':
    case 'l':
        if(( dir_changed = ( lastdir != 'W' && lastdir != 'E' ) ))
            currdir = 'W';
        break;

    case 'E':
    case 'e':
    case 'R':
    case 'r':
        if(( dir_changed = ( lastdir != 'W' && lastdir != 'E' ) ))
            currdir = 'E';
        break;

    default:
        break;
    }

    if( dir_changed )
        MarkSyncAsap();

    return dir_changed;
}

void Movable::SetMoveDuration(unsigned dura)
{
    movetimer.SetDuration(dura);
    MarkSyncLater();
}
