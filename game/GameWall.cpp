#include "ui/ConsoleColour.h"
#include "GameWall.h"

using namespace game;

Wall::Wall(int id, int owner, ObjectPool *pool) :
    Super(id, owner), pool(pool)
{
}

Object* Wall::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new Wall(id, owner, pool);
}

char Wall::Graph() const
{
    return '#';
}

int Wall::Colour() const
{
    return ui::colour.WALL;
}

void Wall::OnCollision(Entity *another)
{
    if( another->Type() == ObjectType::SNAKEHEAD )
        another->Kill();
}
