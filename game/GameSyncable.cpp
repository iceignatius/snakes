#include <algorithm>
#include <endian.h>
#include "net/netdef.h"
#include "GameSyncable.h"

using namespace std;
using namespace game;

Syncable::Syncable(int id) :
    Super(id),
    killing(false),
    killed(false),
    kill_timer(NETDEF_SYNC_KILL_TIMEOUT),
    sync_priority(0)
{
}

bool Syncable::CanBeErased() const
{
    return killed;
}

bool Syncable::IsAlive() const
{
    return !killed && !killing;
}

void Syncable::Kill()
{
    if( killed || killing ) return;

    killing = true;
    kill_timer.Reset();

    MarkSyncAsap();
}

void Syncable::Update(unsigned steptime)
{
    Super::Update(steptime);

    if( !killed && killing && kill_timer.Expired() )
    {
        Super::Kill();
        killing = false;
        killed = true;
    }
}

unsigned Syncable::GetSyncPriority() const
{
    return sync_priority;
}

bool Syncable::CheckIfShouldSync() const
{
    return rand() <= ( RAND_MAX >> sync_priority );
}

void Syncable::LowerSyncPriority()
{
    ++sync_priority;
}

void Syncable::MarkSyncAsap()
{
    sync_priority = 0;
}

void Syncable::MarkSyncLater()
{
    sync_priority = min(sync_priority, 4U);
}

size_t Syncable::ExportRaw(void *buf, size_t size) const
{
    RawObjectHeader *pkg = (RawObjectHeader*) buf;
    if( !buf ) return sizeof(*pkg);
    if( size < sizeof(*pkg) ) return 0;

    bool is_alive = IsAlive() && !killing && !killed;
    pkg->id = htobe32(Id());
    pkg->type = htobe16((unsigned)Type());
    pkg->is_alive = htobe16(is_alive);

    return sizeof(*pkg);
}

size_t Syncable::ImportRaw(const void *raw, size_t size)
{
    const RawObjectHeader *pkg = (const RawObjectHeader*) raw;
    if( !raw ) return sizeof(*pkg);
    if( size < sizeof(*pkg) ) return 0;

    if( be32toh(pkg->id) != (unsigned) Id() ) return 0;
    if( be16toh(pkg->type) != (unsigned) Type() ) return 0;
    if( !be16toh(pkg->is_alive) ) return 0;

    return sizeof(*pkg);
}
