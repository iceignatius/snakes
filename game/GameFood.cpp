#include <assert.h>
#include "ui/ConsoleColour.h"
#include "GameSnakeHead.h"
#include "GameFood.h"

using namespace std;
using namespace game;

Food::Food(int id, int owner) :
    Super(id, owner)
{
}

Object* Food::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new Food(id, owner);
}

char Food::Graph() const
{
    return '$';
}

int Food::Colour() const
{
    return ui::colour.FOOD;
}

void Food::OnCollision(Entity *another)
{
    if( another->Type() != ObjectType::SNAKEHEAD ) return;

    SnakeHead *head = dynamic_cast<SnakeHead*>(another);
    assert(head);

    head->AppendBody(1);
    Kill();
}
