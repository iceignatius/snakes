#include <endian.h>
#include "GameSyncable.h"
#include "GameFactory.h"

using namespace std;
using namespace game;

void ObjectFactory::RegisterType(
    ObjectType type,
    ObjectPool::Region region,
    Object*(*creator)(int, int, ObjectPool*, ObjectFactory*))
{
    TypeInfo info;
    info.region = region;
    info.creator = creator;

    typelist[type] = info;
}

const ObjectFactory::TypeInfo* ObjectFactory::FindTypeInfo(ObjectType type) const
{
    auto iter = typelist.find(type);
    return iter != typelist.end() ? &iter->second : nullptr;
}

Object* ObjectFactory::Create(ObjectType type, ObjectPool *pool, int owner)
{
    const TypeInfo *info = FindTypeInfo(type);
    if( !info ) return nullptr;

    int id = pool->GenerateNewId(info->region);
    if( id < 0 ) return nullptr;

    Object *obj = info->creator(id, owner, pool, this);
    if( !obj ) return nullptr;

    pool->Insert(obj);
    return obj;
}

Object* ObjectFactory::CreateImport(const void *raw, size_t size, ObjectPool *pool)
{
    const RawObjectHeader *hdr = (const RawObjectHeader*) raw;
    if( size < sizeof(*hdr) ) return nullptr;

    if( !hdr->is_alive ) return nullptr;
    int id = (int32_t) be32toh(hdr->id);
    ObjectType type = (ObjectType) be16toh(hdr->type);

    const TypeInfo *info = FindTypeInfo(type);
    if( !info ) return nullptr;

    Object *obj = info->creator(id, 0, pool, this);
    if( !obj ) return nullptr;

    Syncable *syncable = dynamic_cast<Syncable*>(obj);
    if( syncable && !syncable->ImportRaw(raw, size) )
    {
        delete obj;
        return nullptr;
    }

    pool->Insert(obj);
    return obj;
}
