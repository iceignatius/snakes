#ifndef _GAME_POOL_H_
#define _GAME_POOL_H_

#include <memory>
#include <map>
#include "GameObject.h"

namespace game
{

class ObjectPool
{
public:
    class Iterator
    {
    private:
        std::map<int, std::shared_ptr<Object>>::iterator iter;
    public:
        Iterator(const std::map<int, std::shared_ptr<Object>>::iterator &iter) :
            iter(iter)
        {
        }
    public:
        bool operator==(const Iterator &another) const { return iter == another.iter; }
        bool operator!=(const Iterator &another) const { return iter != another.iter; }
        Object& operator*() { return *(iter->second.get()); }
        Object* operator->() { return iter->second.get(); }
        Iterator& operator++() { ++iter; return *this; }
        Iterator operator++(int) { Iterator bak(iter); ++iter; return bak; }
        Iterator& operator--() { --iter; return *this; }
        Iterator operator--(int) { Iterator bak(iter); --iter; return bak; }
    };

    class ConstIterator
    {
    private:
        std::map<int, std::shared_ptr<Object>>::const_iterator iter;
    public:
        ConstIterator(const std::map<int, std::shared_ptr<Object>>::const_iterator &iter) :
            iter(iter)
        {
        }
    public:
        bool operator==(const ConstIterator &another) const { return iter == another.iter; }
        bool operator!=(const ConstIterator &another) const { return iter != another.iter; }
        const Object& operator*() { return *(iter->second.get()); }
        const Object* operator->() { return iter->second.get(); }
        ConstIterator& operator++() { ++iter; return *this; }
        ConstIterator operator++(int) { ConstIterator bak(iter); ++iter; return bak; }
        ConstIterator& operator--() { --iter; return *this; }
        ConstIterator operator--(int) { ConstIterator bak(iter); --iter; return bak; }
    };

    enum class Region
    {
        PLAYSTATE,
        PLAYER,
        ENTITY,
        EFFECTS,
    };

private:
    std::map<int, std::shared_ptr<Object>> pool;

    int last_player_id;
    int last_entity_id;
    int last_effects_id;

public:
    ObjectPool();

private:
    int GenerateNewId(int minval, int maxval, int &lastval);

public:
    int GenerateNewId(Region region);

    bool Existed(int id) const;
    void Insert(Object *obj);
    Object* Get(int id);
    const Object* Get(int id) const;
    Object* GetAlive(int id);
    const Object* GetAlive(int id) const;

    void EraseAll();
    void EraseDeadObjects();

    Iterator begin() { return Iterator(pool.begin()); }
    Iterator end() { return Iterator(pool.end()); }
    ConstIterator begin() const { return ConstIterator(pool.cbegin()); }
    ConstIterator end() const { return ConstIterator(pool.cend()); }
};

}   // namespace game

#endif
