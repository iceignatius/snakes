#ifndef _GAME_SYNCABLE_H_
#define _GAME_SYNCABLE_H_

#include <stddef.h>
#include <stdint.h>
#include "common/tmutl.h"
#include "GameObject.h"

namespace game
{

#pragma pack(push,1)
struct RawObjectHeader
{
    uint32_t id;
    uint16_t type;
    uint16_t is_alive;
};
#pragma pack(pop)

class Syncable : public Object
{
private:
    typedef Object Super;

private:
    bool killing;
    bool killed;
    tmutl::Timer kill_timer;

    unsigned sync_priority;

public:
    Syncable(int id);

public:
    bool CanBeErased() const override;
    bool IsAlive() const override;
    void Kill() override;
    void Update(unsigned steptime) override;

public:
    unsigned GetSyncPriority() const;
    bool CheckIfShouldSync() const;

    void LowerSyncPriority();
    void MarkSyncAsap();
    void MarkSyncLater();

public:
    /*
     * The export and import function returns
     * the size of data be write to or read from the buffer; or
     * returns NULL if error occurred.
     *
     * If the input buffer is NULL, then
     * it returns the minimum buffer size be required.
     */
    virtual size_t ExportRaw(void *buf, size_t size) const;
    virtual size_t ImportRaw(const void *raw, size_t size);
};

}   // namespace game

#endif
