#ifndef _GAME_SNAKE_BODY_H_
#define _GAME_SNAKE_BODY_H_

#include "GameEntity.h"
#include "GameFactory.h"

namespace game
{

class SnakeBody : public Entity
{
private:
    typedef Entity Super;

private:
    ObjectPool *pool;

    bool moved; // Check if body moved since the head be set.
    int followed;
    int head;

public:
    SnakeBody(int id, int owner, ObjectPool *pool);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::SNAKEBODY; }

    void Update(unsigned steptime) final;

    size_t ExportRaw(void *buf, size_t size) const final;
    size_t ImportRaw(const void *raw, size_t size) final;

    void SetPos(int x, int y) final;
    char Graph() const final { return '0'; }
    int Colour() const final { return -1; }

    void OnCollision(Entity *another) final;

public:
    int Followed() const { return followed; }
    void SetFollowed(int id);

    int Head() const { return head; }
    void SetHead(int id);
};

}   // namespace game

#endif
