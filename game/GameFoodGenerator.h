#ifndef _GAME_FOOD_GENERATOR_H_
#define _GAME_FOOD_GENERATOR_H_

#include "GameFactory.h"
#include "GameTimer.h"

namespace game
{

class FoodGenerator : public Object
{
private:
    typedef Object Super;

private:
    ObjectPool *pool;
    ObjectFactory *factory;

    Timer timer;

public:
    FoodGenerator(int id, ObjectPool *pool, ObjectFactory *factory);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::FOODGEN; }

    void Update(unsigned steptime) final;

private:
    unsigned CountFood() const;
    bool IsAnyEntityOnPos(int x, int y) const;
    bool SelectNewFoodPos(int &x, int &y, unsigned trycount) const;
    bool GenOneFood(unsigned trycount);
};

}   // namespace game

#endif
