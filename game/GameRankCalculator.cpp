#include <assert.h>
#include "GamePlayer.h"
#include "GameRankCalculator.h"

using namespace std;
using namespace game;

struct ScoreInfo
{
    Player *player;
    unsigned score;
};

static
unsigned CalcPlayerScore(Player *player)
{
    unsigned score = player->IsAlive() ? 1 : 0;

    score <<= 8;
    score += player->KillCount();

    score <<= 16;
    score += player->Length();

    return score;
}

static
int CompareScoreItems(const ScoreInfo *a, const ScoreInfo *b)
{
    return (int) b->score - (int) a->score;
}

RankCalculator::RankCalculator(int id, ObjectPool *pool) :
    Super(id), pool(pool)
{
}

Object* RankCalculator::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new RankCalculator(id, pool);
}

void RankCalculator::Update(unsigned steptime)
{
    Super::Update(steptime);

    ScoreInfo scorelist[def::MAX_PLAYER_NUM] = {0};
    unsigned listnum = 0;

    // Collect player instances.
    for(Object &obj : *pool)
    {
        if( obj.Id() > def::MAX_PLAYER_ID ) break;

        Player *player = dynamic_cast<Player*>(&obj);
        if( !player || !player->IsAlive() ) continue;

        assert( listnum < def::MAX_PLAYER_NUM );
        scorelist[listnum++].player = player;
    }

    if( !listnum ) return;

    for(unsigned i = 0; i < listnum; ++i)
        scorelist[i].score = CalcPlayerScore(scorelist[i].player);

    qsort(
        scorelist,
        listnum,
        sizeof(scorelist[0]),
        (int(*)(const void*, const void*)) CompareScoreItems);

    // Set rank values by sorting sequence.
    unsigned prev_score = 0, rank = 0;
    for(unsigned i = 0; i < listnum; ++i)
    {
        if( prev_score && prev_score != scorelist[i].score )
            ++rank;

        scorelist[i].player->SetRank(rank);
        prev_score = scorelist[i].score;
    }
}
