#include <assert.h>
#include <endian.h>
#include "GameEntity.h"

using namespace std;
using namespace game;

#pragma pack(push,1)
struct RawInfo
{
    int32_t owner;
    int16_t x;
    int16_t y;
};
#pragma pack(pop)

Entity::Entity(int id, int owner) :
    Super(id), owner(owner), x(0), y(0)
{
}

size_t Entity::ExportRaw(void *buf, size_t size) const
{
    size_t super_encsize = Super::ExportRaw(nullptr, 0);
    size_t total_encsize = super_encsize + sizeof(RawInfo);
    assert(super_encsize);

    if( !buf )
        return total_encsize;
    if( size < total_encsize )
        return 0;
    if( Super::ExportRaw(buf, super_encsize) != super_encsize )
        return 0;

    RawInfo *pkg = (RawInfo*)( (uint8_t*)buf + super_encsize );
    pkg->owner = htobe32(owner);
    pkg->x = htobe16(x);
    pkg->y = htobe16(y);

    return total_encsize;
}

size_t Entity::ImportRaw(const void *raw, size_t size)
{
    size_t super_decsize = Super::ImportRaw(nullptr, 0);
    size_t total_decsize = super_decsize + sizeof(RawInfo);
    assert(super_decsize);

    if( !raw )
        return total_decsize;
    if( size < total_decsize )
        return 0;
    if( Super::ImportRaw(raw, super_decsize) != super_decsize )
        return 0;

    const RawInfo *pkg = (const RawInfo*)( (const uint8_t*)raw + super_decsize );
    owner = be32toh(pkg->owner);
    x = (int16_t) be16toh(pkg->x);
    y = (int16_t) be16toh(pkg->y);

    return total_decsize;
}

void Entity::SetPos(int x, int y)
{
    this->x = x;
    this->y = y;
    MarkSyncLater();
}
