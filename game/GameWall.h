#ifndef _GAME_WALL_H_
#define _GAME_WALL_H_

#include "GameEntity.h"
#include "GameFactory.h"

namespace game
{

class Wall : public Entity
{
private:
    typedef Entity Super;

private:
    ObjectPool *pool;

public:
    Wall(int id, int owner, ObjectPool *pool);

    static Object* CreateInstance(
        int id,
        int owner,
        ObjectPool *pool,
        ObjectFactory *factory);

public:
    ObjectType Type() const final { return ObjectType::WALL; }

    char Graph() const final;
    int Colour() const final;

    void OnCollision(Entity *another) final;
};

}   // namespace game

#endif
