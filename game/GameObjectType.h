#ifndef _GAME_OBJECT_TYPE_H_
#define _GAME_OBJECT_TYPE_H_

namespace game
{

enum class ObjectType
{
    INVALID = 0,
    PLAYSTATE,
    PLAYER,
    RANKCALC,
    SNAKEBODY,
    SNAKEHEAD,
    FOOD,
    FOODGEN,
    SNAKEDECOMP,
    WALL,
    SLIP,
    TUNNEL,
};

}   // namespace game

#endif
