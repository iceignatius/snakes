#ifndef _GAME_NET_INTERFACE_H_
#define _GAME_NET_INTERFACE_H_

#include <string>

enum class GameState
{
    PREPARING,
    STARTING,
    PAUSED,
    PLAYING,
    FINISHED,
};

struct QueryInfo
{
    GameState playstate;
    unsigned curr_joined_num;
    unsigned max_joined_num;
};

class ServerNetApi
{
public:
    virtual void SendTalkToAllUsers(
        int sender, const std::string &name, const std::string &text) = 0;

    virtual bool PushObject(const void *data, size_t size) = 0;
    virtual bool SendThenClearObjects() = 0;
};

class ServerNetEvents
{
public:
    virtual QueryInfo OnQuery() = 0;
    virtual int OnJoin(int &userid, const std::string &name) = 0;
    virtual void OnQuit(int userid) = 0;
    virtual void OnConnError(int userid, int errcode) = 0;
    virtual void OnTalk(int userid, const std::string &text) = 0;
    virtual void OnSetState(int userid, GameState state) = 0;
    virtual void OnSetDir(int userid, char dir) = 0;
};

enum class ClientState
{
    Disconnected,
    ConnectFailed,
    ConnectionError,
    Querying,
    Joining,
    Connected,
};

class ClientNetApi
{
};

class ClientNetEvents
{
public:
    virtual void OnUpdateObject(const void *data, size_t size) = 0;
};

#endif
