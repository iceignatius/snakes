#include "GameDef.h"
#include "GameFood.h"
#include "GameFoodGenerator.h"

using namespace std;
using namespace game;

FoodGenerator::FoodGenerator(int id, ObjectPool *pool, ObjectFactory *factory) :
    Super(id), pool(pool), factory(factory), timer(def::FOODGEN_DURATION)
{
}

Object* FoodGenerator::CreateInstance(
    int id,
    int owner,
    ObjectPool *pool,
    ObjectFactory *factory)
{
    return new FoodGenerator(id, pool, factory);
}

void FoodGenerator::Update(unsigned steptime)
{
    Super::Update(steptime);

    timer.Update(steptime);
    if( !timer.Expired() ) return;
    timer.Next();

    if( CountFood() < def::MAX_FOOD_NUM )
        GenOneFood(def::FOODGEN_TRYCOUNT);
}

unsigned FoodGenerator::CountFood() const
{
    unsigned count = 0;
    for(Object &obj : *pool)
        count += ( obj.Type() == ObjectType::FOOD && obj.IsAlive() )?( 1 ):( 0 );

    return count;
}

bool FoodGenerator::IsAnyEntityOnPos(int x, int y) const
{
    for(Object &obj : *pool)
    {
        Entity *entity = dynamic_cast<Entity*>(&obj);
        if( !entity || !entity->IsAlive() ) continue;

        if( x == entity->PosX() && y == entity->PosY() )
            return true;
    }

    return false;
}

bool FoodGenerator::SelectNewFoodPos(int &x, int &y, unsigned trycount) const
{
    while( trycount-- )
    {
        x = rand() % def::MAP_WIDTH;
        y = rand() % def::MAP_HEIGHT;

        if( !IsAnyEntityOnPos(x, y) )
            return true;
    }

    return false;
}

bool FoodGenerator::GenOneFood(unsigned trycount)
{
    int x, y;
    if( !SelectNewFoodPos(x, y, trycount) )
        return false;

    Food *food = dynamic_cast<Food*>(factory->Create(ObjectType::FOOD, pool));
    if( !food ) return false;

    food->SetPos(x, y);
    return true;
}
