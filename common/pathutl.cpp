#include <string.h>

#ifdef _WIN32
#   include <windows.h>
#   include <shlobj.h>
#else
#   include <unistd.h>
#   include <pwd.h>
#   include <dirent.h>
#   include <linux/limits.h>
#endif

#include "pathutl.h"

using namespace std;
using namespace pathutl;

#ifdef _WIN32
static
char* convert_to_unix_path(char *pathname)
{
    for(char *pos = pathname; *pos; ++pos)
    {
        if( *pos == '\\' )
            *pos = '/';
    }

    return pathname;
}
#endif

string pathutl::GetExecutableFileName()
{
#ifdef _WIN32
    static char pathname[MAX_PATH] = {0};
    if( pathname[0] ) return pathname;

    DWORD len = GetModuleFileName(NULL, pathname, sizeof(pathname)-1);
    if( !len || len > sizeof(pathname) - 1 )
        return "";

    return convert_to_unix_path(pathname);
#else
    static char pathname[PATH_MAX] = {0};
    if( pathname[0] ) return pathname;

    if( 0 > readlink("/proc/self/exe", pathname, sizeof(pathname)-1) )
        pathname[0] = 0;

    return pathname;
#endif
}

string pathutl::GetHomeDir()
{
#ifdef _WIN32
    static char pathname[MAX_PATH + 1] = {0};
    if( pathname[0] ) return pathname;

    if( FAILED(SHGetFolderPath(NULL, CSIDL_PROFILE, NULL, 0, pathname)) )
        pathname[0] = 0;

    return convert_to_unix_path(pathname);
#else
    static char pathname[PATH_MAX] = {0};
    if( pathname[0] ) return pathname;

    struct passwd *pwd = getpwuid(getuid());
    strncpy(pathname, pwd ? pwd->pw_dir : "", sizeof(pathname)-1);

    return pathname;
#endif
}

string pathutl::ExtractFileDir(const string &filename)
{
    size_t pos = filename.rfind('/');
    return pos != string::npos ? filename.substr(0, pos) : "";
}

string pathutl::ExtractFileName(const string &filename)
{
    size_t pos = filename.rfind('/');
    return pos != string::npos ? filename.substr(pos + 1) : filename;
}

string pathutl::ExtractFileExt(const string &filename)
{
    size_t pos = filename.rfind('.');
    return pos != string::npos ? filename.substr(pos) : filename;
}

string pathutl::RemoveFileExt(const string &filename)
{
    size_t pos = filename.rfind('.');
    return pos != string::npos ? filename.substr(0, pos) : filename;
}

list<string> pathutl::ListDirFiles(const string &dirname)
{
    list<string> filelist;

#ifdef _WIN32
    HANDLE hfind = INVALID_HANDLE_VALUE;

    do
    {
        if( dirname.empty() )
            break;

        string prefix = dirname;
        if( prefix[prefix.length()-1] != '/' )
            prefix += "/";

        string findstr = prefix + "*";

        WIN32_FIND_DATAA item;
        hfind = FindFirstFileA(findstr.c_str(), &item);
        if( hfind == INVALID_HANDLE_VALUE )
            break;

        do
        {
            if( !strcmp(item.cFileName, ".") || !strcmp(item.cFileName, "..") )
                continue;

            filelist.push_back(prefix + item.cFileName);
        } while(FindNextFileA(hfind, &item));
    } while(false);

    if( hfind != INVALID_HANDLE_VALUE )
        FindClose(hfind);
#else
    DIR *dir = nullptr;

    do
    {
        if( !( dir = opendir(dirname.c_str()) ) )
            break;

        string prefix = dirname;
        if( prefix[prefix.length()-1] != '/' )
            prefix += "/";

        for(struct dirent *item = readdir(dir); item; item = readdir(dir))
        {
            if( !strcmp(item->d_name, ".") || !strcmp(item->d_name, "..") )
                continue;

            filelist.push_back(prefix + item->d_name);
        }
    } while(false);

    if( dir )
        closedir(dir);
#endif

    return filelist;
}
