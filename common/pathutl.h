#ifndef _PATH_UTILITY_H_
#define _PATH_UTILITY_H_

#include <string>
#include <list>

namespace pathutl
{

std::string GetExecutableFileName();
std::string GetHomeDir();
std::string ExtractFileDir(const std::string &filename);
std::string ExtractFileName(const std::string &filename);
std::string ExtractFileExt(const std::string &filename);
std::string RemoveFileExt(const std::string &filename);
std::list<std::string> ListDirFiles(const std::string &dirname);

}   // namespace pathutl

#endif
