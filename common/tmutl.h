#ifndef _TIME_UTILITY_H_
#define _TIME_UTILITY_H_

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

void tmutl_sleep(unsigned ms);
unsigned tmutl_get_mstime(void);

typedef struct tmutl_timer
{
    unsigned start;
    unsigned dura;
} tmutl_timer_t;

static inline
void tmutl_timer_init(tmutl_timer_t *self, unsigned dura)
{
    self->start = tmutl_get_mstime();
    self->dura = dura;
}

static inline
void tmutl_timer_set_dura(tmutl_timer_t *self, unsigned dura)
{
    self->dura = dura;
}

static inline
unsigned tmutl_timer_get_dura(const tmutl_timer_t *self)
{
    return self->dura;
}

static inline
void tmutl_timer_reset(tmutl_timer_t *self)
{
    self->start = tmutl_get_mstime();
}

static inline
void tmutl_timer_next(tmutl_timer_t *self)
{
    self->start += self->dura;
}

static inline
bool tmutl_timer_expired(const tmutl_timer_t *self)
{
    return tmutl_get_mstime() - self->start >= self->dura;
}

static inline
unsigned tmutl_timer_passed(const tmutl_timer_t *self)
{
    return tmutl_get_mstime() - self->start;
}

#ifdef __cplusplus
}  // extern "C"
#endif

#ifdef __cplusplus

namespace tmutl
{

inline void Sleep(unsigned ms) { tmutl_sleep(ms); }
inline unsigned GetMsTime() { return tmutl_get_mstime(); }

class Timer : private tmutl_timer_t
{
public:
    Timer(unsigned dura) { tmutl_timer_init(this, dura); }

public:
    inline void SetDuration(unsigned dura) { tmutl_timer_set_dura(this, dura); }
    inline unsigned GetDuration() const { return tmutl_timer_get_dura(this); }
    inline void Reset() { tmutl_timer_reset(this); }
    inline void Next() { tmutl_timer_next(this); }
    inline bool Expired() const { return tmutl_timer_expired(this); }
    inline unsigned Passed() const { return tmutl_timer_passed(this); }
};

}   // namespace tmutl

#endif  // __cplusplus

#endif
